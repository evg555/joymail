<?php

use App\Models\Offer;
use App\Models\Order;
use Illuminate\Support\Facades\DB;

class SendPageCest
{
    public function _before(FunctionalTester $I)
    {
        $order = Order::where('message', 'functional test')->first();

        if ($order) {
            $order->offers()->delete();
            $order->delete();
        }
    }

    public function index(FunctionalTester $I)
    {
        $I->amOnPage('/send');
        $I->see('CREAT YOUR');
        $I->see('ADDITIONAL ITEMS');
        $I->see('ENTER YOUR DATA');
        $I->see('Pay');
        $I->see('support@joymail.biz');
    }

    public function createOrder(FunctionalTester $I)
    {
        $I->amOnPage('/send');

        $fields = [
            'letter_type' => 'love-letter',
            'message' => 'functional test',
            'city_id' => '2',
            'name_sender' => 'test',
            'name_reciever' => 'test',
            'email_sender' => 'test@test.ru',
            'tel_sender' => 'test',
            'tel_reciever'=> 'test',
            'price' => '8',
            'currency' => 'GEL',
            'status' => 'N',
        ];

        $offers = Offer::select(
            'offers.id',
            'offer_langs.name'
        )->join('offer_langs', 'offer_langs.offer_id', 'offers.id')
            ->where('offer_langs.lang', 'en')
            ->get();

        foreach ($offers as $offer) {
            $offerFields['offers'][$offer->id] = '1';
        }

        $I->submitForm('#data-form', array_merge($fields, $offerFields, [
            'promocode' => 'test',
            'agree' => '1'
        ]));

        $I->seeResponseCodeIs(200);

        $I->seeInDatabase('orders', $fields);

        $order = Order::where('message', 'functional test')->first();

        $I->seeInDatabase('order_offers', ['order_id' => $order->id]);
    }
}
