<?php

use Illuminate\Support\Facades\DB;

class TransactionCest
{
    public function _before(FunctionalTester $I)
    {
        DB::table('jobs')->truncate();
        DB::table('payments')->where('invoice_id',  'functional_test')->delete();
    }

    /**
     * @param FunctionalTester $I
     */
    public function payment(FunctionalTester $I)
    {
        $I->seeInDatabase('orders', ['message' => 'functional test']);

        $order = DB::table('orders')->where('message', 'functional test')->first();

        $I->sendPost('/payment', [
            'order_id' => 'functional_test',
            'shop_order_id' => $order->id,
            'ipay_payment_id' => 'functional_test',
            'status' => 'success',
            'status_description' => 'functional_test'
        ]);
        $I->seeResponseContainsJson(['status' => 'success']);
        $I->seeInDatabase('jobs',['id' => 1]);

//        Artisan::call('queue:work', ['--once']);
//
//        $I->seeInDatabase('payments', ['invoice_id' => 'functional_test']);
    }

}
