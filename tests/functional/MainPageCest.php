<?php

class MainPageCest
{
    public function index(FunctionalTester $I)
    {
        $I->amOnPage('/');
        $I->see('Send JoyKa');
        $I->see('How it works?');
        $I->see('Reasons for sending');
        $I->see('Why us?');
        $I->see('FAQ');
        $I->see('Reviews');
        $I->see('support@joymail.biz');
    }
}
