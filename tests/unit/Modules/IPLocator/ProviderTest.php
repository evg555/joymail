<?php

namespace Tests\unit\Modules\IPLocator;

use App\Modules\IPLocator\Provider;
use App\Modules\IPLocator\Readers\DefaultReader;
use App\Modules\IPLocator\Readers\GeoIPReader;
use PHPUnit\Framework\TestCase;

class ProviderTest extends TestCase
{
    public function testGetCountryISO()
    {
        //Default reader
        $provider = new Provider(new DefaultReader());

        static::assertEquals('en', $provider->getCountryISO('0.0.0.0'));

        $provider = new Provider(new GeoIPReader());

        //TODO: undefined database_path
        static::assertEquals('ru', $provider->getCountryISO('77.88.55.88'));
        static::assertEquals('us', $provider->getCountryISO('8.8.8.8'));
    }
}
