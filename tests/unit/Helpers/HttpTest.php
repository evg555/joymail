<?php

namespace Tests\Unit\Helpers;

use App\Helpers\Http;
use PHPUnit\Framework\TestCase;

class HttpTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testIsHttps()
    {
        $_SERVER['HTTPS'] = 'on';
        static::assertTrue( Http::isHttps());

        unset($_SERVER['HTTPS']);
        static::assertFalse(Http::isHttps());

        $_SERVER['HTTPS'] = 'off';
        static::assertFalse(Http::isHttps());
    }

    public function testGetSchemaProtocol()
    {
        $_SERVER['HTTPS'] = 'on';
        static::assertEquals( 'https://', Http::getSchemaProtocol());

        unset($_SERVER['HTTPS']);
        static::assertEquals( 'http://', Http::getSchemaProtocol());

        $_SERVER['HTTPS'] = 'off';
        static::assertEquals( 'http://', Http::getSchemaProtocol());
    }
}
