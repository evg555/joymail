## Технологии:

* Laravel 6.2
* PHP 7.4
* MysSQL 8
* NPM
* Docker

## Разворачивание проекта
1. Копируем .env.example в .env

```
cp .env.example .env
```

2. Создаем докер контейнеры

``` 
make docker_up
```
3. Устанавливеам зависимости

```
make composer_install
```

4. Устанавливаем миграции

```
make migrate
```

5. Гененрируем ключ приложения

```
make generate_key
```

6. Заходим на http://localhost:8091

## Запуск тестов

```
make test
```


