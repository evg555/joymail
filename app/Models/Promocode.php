<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Promocode
 * @package App\Models
 */
class Promocode extends Model
{
    protected $guarded = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo(Company::class);
    }
}
