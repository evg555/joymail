<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Country
 * @package App\Models
 */
class Country extends Model
{
    protected $fillable = ['language_id', 'code', 'name'];

    /**
     * @return HasMany
     */
    public function langs()
    {
        return $this->hasMany(CountryLang::class);
    }

    /**
     * @return HasOne
     */
    public function currency()
    {
        return $this->hasOne(Currency::class);
    }
}
