<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Filesystem\Filesystem;

/**
 * Class Offer
 * @package App\Models
 */
class Offer extends Model
{
    /**
     * @return HasMany
     */
    public function langs()
    {
        return $this->hasMany(OfferLang::class);
    }

    /**
     * @return BelongsTo
     */
    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    /**
     * @return HasMany
     */
    public function basketOffers()
    {
        return $this->hasMany(BasketOffer::class);
    }

    /**
     * Delete the model from the database.
     *
     * @return bool|null
     *
     * @throws Exception
     */
    public function delete()
    {
        app(Filesystem::class)->delete(public_path($this->image));
        return parent::delete();
    }
}
