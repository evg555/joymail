<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class OrderOffer
 * @package App
 */
class OrderOffer extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
