<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class OfferLang
 * @package App\Models
 */
class OfferLang extends Model
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function offers()
    {
        return $this->belongsTo(Offer::class);
    }
}
