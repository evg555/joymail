<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CityLang
 * @package App\Models
 */
class CityLang extends Model
{
    protected $guarded = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function lang()
    {
        return $this->hasOne(Lang::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo(City::class);
    }
}
