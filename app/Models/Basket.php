<?php

namespace App\Models;

use App\Helpers\Site;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Basket
 * @package App\Models
 */
class Basket extends Model
{
    protected $guarded = [];

    /**
     * @return mixed
     */
    public function city()
    {
        return $this->$this->belongsTo(City::class);
    }

    /**
     * @return HasMany
     */
    public function offers()
    {
        return $this->hasMany(BasketOffer::class);
    }

    /**
     * @return mixed
     */
    public static function getBasketByToken()
    {
        return static::where('token', Site::getBasketCookie())->first();
    }

    public static function saveBasketOrFail(?Basket $basket, array $data)
    {
        if ($basket) {
            $basket->fill($data);
            $basket->save();
        } else {
            throw new Exception('Basket not found with token: ' . Site::getBasketCookie());
        }
    }
}
