<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Filesystem\Filesystem;

/**
 * Class Card
 * @package App\Models
 */
class Card extends Model
{
    protected $guarded = [];

    /**
     * @return BelongsTo
     */
    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    /**
     * Delete the model from the database.
     *
     * @return bool|null
     *
     * @throws Exception
     */
    public function delete()
    {
        app(Filesystem::class)->delete(public_path($this->picture));
        return parent::delete();
    }
}
