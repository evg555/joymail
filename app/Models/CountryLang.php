<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CountryLang
 * @package App\Model
 */
class CountryLang extends Model
{
    protected $guarded = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(Country::class);
    }
}
