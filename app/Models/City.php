<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class City
 * @package App\Models
 */
class City extends Model
{
    /**
     * @return array
     */
    public static function getCities()
    {
        $cities = self::select('cities.id', 'city_langs.name')
            ->join('city_langs', 'city_langs.city_id', 'cities.id')
            ->join('langs', 'langs.id', 'city_langs.lang_id')
            ->where('langs.name', 'en')
            ->orderBy('cities.sort', 'asc')
            ->get()
            ->toArray();

        foreach ($cities as $city) {
            $formatedCities[$city['id']] = $city['name'];
        }

        return $formatedCities ?? [];
    }

    /**
     * @return BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    /**
     * @return HasMany
     */
    public function langs()
    {
        return $this->hasMany(CityLang::class);
    }

    public function default()
    {
        return $this->langs()->where('lang_id', 2);
    }

    /**
     * @return HasMany
     */
    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    /**
     * @return HasMany
     */
    public function baskets()
    {
        return $this->hasMany(Basket::class);
    }

    /**
     * @return HasOne
     */
    public function company()
    {
        return $this->hasOne(Company::class);
    }
}
