<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Filesystem\Filesystem;

/**
 * Class Order
 * @package App\Models
 */
class Order extends Model
{
    const ORDER_STATUS_NEW = 'N';
    const ORDER_STATUS_PAYED = 'P';

    protected $guarded = [];

    /**
     * @return HasOne
     */
    public function payment()
    {
        return $this->hasOne(Payment::class);
    }

    /**
     * @return BelongsTo
     */
    public function city()
    {
        return $this->belongsTo(City::class);
    }

    /**
     * @return HasMany
     */
    public function offers()
    {
        return $this->hasMany(OrderOffer::class);
    }
}
