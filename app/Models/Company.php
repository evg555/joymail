<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Company
 * @package App\Models
 */
class Company extends Model
{
    protected $guarded = [];

    /**
     * @return BelongsTo
     */
    public function city()
    {
        return $this->belongsTo(City::class);
    }

    /**
     * @return HasMany
     */
    public function users()
    {
        return $this->hasMany(User::class);
    }

    /**
     * @return HasMany
     */
    public function offers()
    {
        return $this->hasMany(Offer::class);
    }

    /**
     * @return HasMany
     */
    public function promocodes()
    {
        return $this->hasMany(Promocode::class);
    }

    /**
     * @return HasMany
     */
    public function cards()
    {
        return $this->hasMany(Card::class);
    }

    /**
     * @return BelongsToMany
     */
    public function systems()
    {
        return $this->belongsToMany(PaymentSystem::class)->withPivot('active');
    }

    /**
     * @return HasMany
     */
    public function templates()
    {
        return $this->hasMany(Postcard::class);
    }
}
