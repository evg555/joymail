<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Lang
 * @package App\Models
 */
class Lang extends Model
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cityLang()
    {
        return $this->belongsTo(CityLang::class);
    }
}
