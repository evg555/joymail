<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Filesystem\Filesystem;

class Postcard extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    /**
     * @return BelongsTo
     */
    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    /**
     * Delete the model from the database.
     *
     * @return bool|null
     *
     * @throws Exception
     */
    public function delete()
    {
        app(Filesystem::class)->delete(public_path($this->image));
        return parent::delete();
    }
}
