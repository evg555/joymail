<?php

namespace App\Repositories;

use App\Models\Country;
use App\Models\CountryLang;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;

class CountryRepository implements IRepository
{
    /**
     * @return mixed
     */
    public static function getAll(): ?Collection
    {
        return CountryLang::select('country_langs.country_id', 'country_langs.name', 'countries.image')
            ->join('langs', 'langs.id', 'country_langs.lang_id')
            ->join('countries', 'country_langs.country_id', 'countries.id')
            ->where('langs.name', App::getLocale())
            ->get();
    }

    /**
     * @param int $cityId
     *
     * @return Country|null
     */
    public static function getCountryByCityId(int $cityId): ?Country
    {
        return Country::select('country_langs.name')
            ->join('cities', 'cities.country_id', 'countries.id')
            ->join('country_langs', 'country_langs.country_id', 'countries.id')
            ->join('langs', 'langs.id', 'country_langs.lang_id')
            ->where('langs.name', App::getLocale())
            ->where('cities.id', $cityId)
            ->first();
    }
}
