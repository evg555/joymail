<?php

namespace App\Repositories;

use Illuminate\Support\Collection;

interface IRepository
{
    public static function getAll(): ?Collection;
}
