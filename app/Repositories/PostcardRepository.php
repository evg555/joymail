<?php

namespace App\Repositories;

use App\Models\Postcard;
use Illuminate\Support\Collection;

class PostcardRepository
{
    public static function getAllByCompanyId(int $companyId): ?Collection
    {
        return Postcard::where('company_id', $companyId)->orderBy('sort', 'asc')->get();
    }
}
