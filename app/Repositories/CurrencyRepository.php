<?php

namespace App\Repositories;

use App\Models\Currency;

class CurrencyRepository
{
    /**
     * @param int $countryId
     *
     * @return mixed
     */
    public static function getByCountryId(int $countryId): ?Currency
    {
        return Currency::where('country_id', $countryId)->first();
    }
}
