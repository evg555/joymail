<?php

namespace App\Repositories;

use App\Models\City;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;

class CityRepository
{
    /**
     * @param int $countryId
     *
     * @return Collection|null
     */
    public static function getAllByCountryId(int $countryId) : ?Collection
    {
        return City::select('cities.id', 'city_langs.name', 'cities.active')
            ->join('city_langs', 'city_langs.city_id', 'cities.id')
            ->join('langs', 'langs.id', 'city_langs.lang_id')
            ->where('cities.country_id', $countryId)
            ->where('langs.name', App::getLocale())
            ->orderBy('cities.active', 'desc')
            ->orderBy('cities.sort', 'asc')
            ->get();
    }

    /**
     * @param int $id
     *
     * @return City|null
     */
    public static function getById(int $id): ?City
    {
        return City::select('city_langs.name')
            ->join('city_langs', 'city_langs.city_id', 'cities.id')
            ->join('langs', 'langs.id', 'city_langs.lang_id')
            ->where('langs.name', App::getLocale())
            ->where('cities.id', $id)
            ->first();
    }
}
