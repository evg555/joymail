<?php

namespace App\Repositories;

use App\Models\Company;

class CompanyRepository
{
    /**
     * @param int $cityId
     *
     * @return mixed
     */
    public static function getByCityId(int $cityId): ?Company
    {
        return Company::where('city_id', $cityId)->first();
    }
}
