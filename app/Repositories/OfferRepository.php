<?php

namespace App\Repositories;

use App\Models\Offer;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;

class OfferRepository
{
    /**
     * @param int $companyId
     *
     * @return Collection|null
     */
    public static function getAllByCompanyId(int $companyId): ?Collection
    {
        return Offer::with(['langs' => function($query) {
                return $query->where('lang', App::getLocale());
            }])
            ->where('company_id', $companyId)
            ->orderBy('sort')
            ->get();
    }
}
