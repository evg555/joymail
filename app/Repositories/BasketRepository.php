<?php

namespace App\Repositories;

use App\Helpers\Site;
use App\Models\Basket;

class BasketRepository
{
    /**
     * @return mixed
     */
    public static function get(): ?Basket
    {
        $basketToken = Site::setBasketCookie();
        return Basket::where('token', $basketToken)->first();
    }
}
