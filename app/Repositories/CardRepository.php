<?php

namespace App\Repositories;

use App\Models\Card;
use Illuminate\Support\Collection;

class CardRepository
{
    /**
     * @param int $companyId
     *
     * @return Collection|null
     */
    public static function getAllByCompanyId(int $companyId) : ?Collection
    {
        return Card::where('company_id', $companyId)->get();
    }

    /**
     * @param int $id
     *
     * @return Card|null
     */
    public static function getById(int $id): ?Card
    {
        return Card::find($id);
    }
}
