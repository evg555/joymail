<?php

namespace App\Providers;

use App\Http\View\Composer\MainComposer;
use App\Modules\Currency\Converter;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Converter::class, function() {
            return new Converter();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(['main.partials.*'], MainComposer::class);
        Schema::defaultStringLength(191);
    }
}
