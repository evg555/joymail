<?php

namespace App\Providers;

use App\Http\Admin\Company;
use App\Http\Admin\Feedback;
use App\Http\Admin\PaymentSystem;
use App\Http\Admin\User;
use App\Policies\CompanyPolicy;
use App\Policies\FeedbackPolicy;
use App\Policies\PaymentSystemPolicy;
use App\Policies\UserPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

/**
 * Class AuthServiceProvider
 * @package App\Providers
 */
class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Feedback::class => FeedbackPolicy::class,
        Company::class => CompanyPolicy::class,
        User::class => UserPolicy::class,
        PaymentSystem::class => PaymentSystemPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('isAdmin', function($user) {
            return $user->isAdmin();
        });
    }
}
