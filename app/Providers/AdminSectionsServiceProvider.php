<?php

namespace App\Providers;

use App\Models\Card;
use App\Models\Company;
use App\Models\Offer;
use App\Models\Order;
use App\Models\PaymentSystem;
use App\Models\Postcard;
use App\Models\Promocode;
use App\Models\User;
use App\Modules\Feedbacks\Model\Feedback;
use KodiCMS\Assets\Facades\PackageManager;
use SleepingOwl\Admin\Admin;
use SleepingOwl\Admin\Providers\AdminSectionsServiceProvider as ServiceProvider;

/**
 * Class AdminSectionsServiceProvider
 * @package App\Providers
 */
class AdminSectionsServiceProvider extends ServiceProvider
{

    /**
     * @var array
     */
    protected $sections = [
        Order::class => \App\Http\Admin\Order::class,
        Card::class => \App\Http\Admin\Card::class,
        Postcard::class => \App\Http\Admin\Postcard::class,
        Offer::class => \App\Http\Admin\Offer::class,
        //Payment::class => \App\Http\Admin\Payment::class,
        Promocode::class => \App\Http\Admin\Promocode::class,
        Feedback::class => \App\Http\Admin\Feedback::class,
        Company::class => \App\Http\Admin\Company::class,
        User::class => \App\Http\Admin\User::class,
        PaymentSystem::class => \App\Http\Admin\PaymentSystem::class,
    ];

    /**
     * Register sections.
     *
     * @param Admin $admin
     * @return void
     */
    public function boot(Admin $admin)
    {
        PackageManager::add('jquery')
            ->js('jquery.js', 'https://code.jquery.com/jquery-3.1.0.min.js');

        parent::boot($admin);
    }
}
