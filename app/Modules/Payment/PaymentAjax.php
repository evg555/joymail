<?php

namespace App\Modules\Payment;

use App\Helpers\Site;
use App\Models\Order;
use App\Modules\AjaxInterface;
use App\Modules\AjaxResult;

/**
 * Class System
 * @package App\Modules\Payment
 */
class PaymentAjax implements AjaxInterface
{
    use AjaxResult;

    /**
     * @var
     */
    private $params;

    /**
     * @param array $params
     *
     * @return AjaxInterface
     */
    public function setParams(array $params) : AjaxInterface
    {
        $data = request()->validate([
            'orderId' => 'int|required',
            'systemId' => 'int|required',
        ]);

        $this->params = $data;

        return $this;
    }

    /**
     * @return array
     */
    public function dispatch() : array
    {
        Site::setCountryAndLocale();

        $system = PaymentFactory::getSystem($this->params['systemId']);
        $order = Order::find($this->params['orderId']);
        $html = $system->getPaymentButton($order);

        return $this->sendSuccess('Get Payment Button', [
            'view' => $html
        ]);
    }
}
