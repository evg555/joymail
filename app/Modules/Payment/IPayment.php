<?php

namespace App\Modules\Payment;

use App\Models\Order;

/**
 * Interface IPayment
 * @package App\Modules\Payment
 */
interface IPayment
{
    public function getPaymentButton(Order $order): string;

    public function savePayment(array $data);
}
