<?php

namespace App\Modules\Payment\Systems;

use App\Models\Order;
use App\Modules\Payment\IPayment;

class OnDelivery implements IPayment
{
    public function getPaymentButton(Order $order): string
    {
        $url = '/savePayment/' . $order->id;
        return view('payment.ipay', compact('url'))->render();
    }

    public function savePayment(array $data)
    {
        // TODO: Implement savePayment() method.
    }
}
