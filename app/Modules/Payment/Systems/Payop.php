<?php

namespace App\Modules\Payment\Systems;

use App\Helpers\Http;
use App\Models\Order;
use App\Models\Payment;
use GuzzleHttp\Client;
use Psy\Util\Json;

/**
 * Class Payop
 * @package App\Models\Payment
 */
class Payop
{
    const PAIMENT_METOD = 381;

    const ALLOWED_IP = [
        '52.49.204.201',
        '54.229.170.212'
    ];

    /**
     * @param array $order
     *
     * @return false|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function createInvoice(array $order)
    {
        $locale = app()->getLocale() == 'ru' ? 'ru' : 'en';
        $http = Http::getSchemaProtocol();

        $data = [
            "publicKey" => \env('PAYOP_PUBLIC_KEY'),
            "order" => [
                "id" => $order['order_id'],
                "amount" => $order['price'],
                "currency" => $order['currency'],
                "items" => []
            ],
            "signature" => self::getSignature($order),
            "payer" => [
                "email" => $order['email_sender'],
                "phone" => $order['tel_sender'],
                "name" => $order['name_sender']
            ],
            "language" => $locale,
            "paymentMethod" => self::PAIMENT_METOD,
            "resultUrl" => $http . $_SERVER['HTTP_HOST'] . "/{$locale}/send?status=success",
            "failPath" => $http . $_SERVER['HTTP_HOST'] . "/{$locale}/send?status=fail"
        ];

        $client = new Client();
        $response = $client->request('POST', \env('PAYOP_URL'), ['body' => Json::encode($data)]);
        $result = json_decode($response->getBody(),true);

        if (!empty($result['status']) && $result['status'] == 1) {
            $invoiceId = $result['data'];
        }

        return $invoiceId ?? false;
    }

    /**
     * @param array $order
     *
     * @return string
     */
    private static function getSignature(array $order)
    {
        $fields = [
            "id" => $order['order_id'],
            "amount" => $order['price'],
            "currency" => $order['currency']
        ];
        ksort($fields, SORT_STRING);
        $dataSet = array_values($fields);
        $dataSet[] = \env('PAYOP_SECRET_KEY');
        $hash = hash('sha256', implode(':', $dataSet));

        return $hash;
    }

    /**
     * @return bool
     */
    public static function isAllowedIp()
    {
        return in_array($_SERVER['REMOTE_ADDR'], self::ALLOWED_IP);
    }

    /**
     * @param array $data
     *
     * @return int|bool
     */
    public static function savePayment(array $data)
    {
        try {
            $transaction = $data['transaction'];

            $payment = new Payment();
            $payment->invoice_id = $data['invoice']['id'];
            $payment->order_id = (int) $transaction['order']['id'];
            $payment->transaction_id = $transaction['id'];
            $payment->transaction_state = $transaction['state'];
            $payment->error = $transaction['error']['message'] ?: '';
            $payment->save();

            if (empty($payment->error)) {
                self::changeOrderStatus($payment->order_id);
                return $payment->order_id;
            }

            return false;
        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    }

    /**
     * @param int $orderId
     */
    private static function changeOrderStatus(int $orderId)
    {
        $order = Order::find($orderId);
        $order->status = Order::ORDER_STATUS_PAYED;
        $order->save();
    }
}
