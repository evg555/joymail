<?php

namespace App\Modules\Payment\Systems;

use App;
use App\Helpers\OrderHelper;
use App\Models\Order;
use App\Models\Payment;
use App\Modules\Currency\Converter;
use App\Modules\Payment\IPayment;
use Exception;

/**
 * Class LiqPay
 * @package App\Helpers
 */
class Liqpay implements IPayment
{
    const CURRENCY_EUR = 'EUR';
    const CURRENCY_USD = 'USD';
    const CURRENCY_UAH = 'UAH';
    const CURRENCY_RUB = 'RUB';
    const CURRENCY_RUR = 'RUR';

    private $checkoutUrl = 'https://www.liqpay.ua/api/3/checkout';
    protected $supportedCurrencies = [
        self::CURRENCY_EUR,
        self::CURRENCY_USD,
        self::CURRENCY_UAH,
        self::CURRENCY_RUB,
        self::CURRENCY_RUR,
    ];
    private $publicKey;
    private $privateKey;

    public function __construct()
    {
        $this->publicKey = env('LIQPAY_PUBLIC_KEY');
        $this->privateKey = env('LIQPAY_PRIVATE_KEY');
    }

    /**
     * cnb_form
     *
     * @param array $params
     *
     * @return string
     *
     * @throws Exception
     */
    public function getForm($params)
    {
        $params = $this->prepareParams($params);
        $data = static::encodeParams($params);
        $signature = $this->getSignature($params);
        $url = $this->checkoutUrl;

        return view('payment.liqpay', compact('url', 'data', 'signature'));
    }

    /**
     * cnb_form raw data for custom form
     *
     * @param $params
     *
     * @return array
     * @throws Exception
     */
    public function getFormRaw($params)
    {
        $params = $this->prepareParams($params);

        return [
            'url' => $this->checkoutUrl,
            'data' => static::encodeParams($params),
            'signature' => $this->getSignature($params)
        ];
    }

    /**
     * getSignature
     *
     * @param array $params
     *
     * @return string
     * @throws Exception
     */
    public function getSignature($params)
    {
        $params = $this->prepareParams($params);
        $private_key = $this->privateKey;

        $json = static::encodeParams($params);
        $signature = static::strToSign($private_key . $json . $private_key);

        return $signature;
    }

    /**
     * prepareParams
     *
     * @param array $params
     *
     * @return array $params
     * @throws Exception
     */
    private function prepareParams($params)
    {
        $params['public_key'] = $this->publicKey;

        if (!isset($params['version'])) {
            throw new Exception('version is null');
        }

        if (!isset($params['amount'])) {
            throw new Exception('amount is null');
        }

        if (!isset($params['currency'])) {
            throw new Exception('currency is null');
        }

//        if (!in_array($params['currency'], $this->supportedCurrencies)) {
//            throw new Exception('currency is not supported');
//        }
//
//        if ($params['currency'] == self::CURRENCY_RUR) {
//            $params['currency'] = self::CURRENCY_RUB;
//        }

        if (!isset($params['description'])) {
            throw new Exception('description is null');
        }

        return $params;
    }

    /**
     * encodeParams
     *
     * @param array $params
     *
     * @return string
     */
    public static function encodeParams($params)
    {
        return base64_encode(json_encode($params));
    }

    /**
     * @param $params
     *
     * @return false|string
     */
    private static function decodeParams($params)
    {
        return json_decode(base64_decode($params), true);
    }

    /**
     * strToSign
     *
     * @param string $str
     *
     * @return string
     */
    public static function strToSign($str)
    {
        $signature = base64_encode(sha1($str, 1));

        return $signature;
    }

    /**
     * @param array $data
     *
     * @return int|bool
     */
    public function savePayment(array $data)
    {
        try {
            $decodeData = static::decodeParams($data['data']);

            $payment = new Payment();
            $payment->invoice_id = $decodeData['payment_id'];
            $payment->order_id = (int) $decodeData['order_id'];
            $payment->transaction_id = $decodeData['transaction_id'] ?: '';
            $payment->transaction_state = ($decodeData['status'] == 'success' ? 1 : 0);
            $payment->error = '';
            $payment->save();

            if ($decodeData['status'] == 'success') {
                OrderHelper::changeOrderStatus($payment->order_id);
                return $payment->order_id;
            }
        } catch (Exception $e) {
            info('Payment is not saved: ' . $e->getMessage());
        }

        return false;
    }

    public function getPaymentButton(Order $order) : string
    {
        $locale = App::getLocale();

        if ($order->currency !== 'uah') {
            $rate = app(Converter::class)->convert($order->currency, 'uah');
            $order->price = ceil($rate * $order->price) + Converter::TAX;
            $order->currency = 'uah';
        }

        $params = [
            'version' => 3,
            'action' => 'pay',
            'amount' => $order->price,
            'currency' => strtoupper($order->currency),
            'description' => '',
            'order_id' => $order->id,
            'language' => $locale == 'ua' ? 'uk' : $locale,
            'result_url' => request()->getSchemeAndHttpHost() . '/thanks',
            'server_url' => request()->getSchemeAndHttpHost() . '/liqpay/'
        ];

        return $this->getForm($params);
    }
}
