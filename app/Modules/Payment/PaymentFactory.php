<?php

namespace App\Modules\Payment;

use App\Models\PaymentSystem;

/**
 * Class PaymentFactory
 * @package App\Modules\Payment
 */
class PaymentFactory
{
    /**
     * @param int $systemId
     *
     * @return IPayment
     */
    public static function getSystem(int $systemId): IPayment
    {
        $system = PaymentSystem::find($systemId);
        $className = __NAMESPACE__ . '\\Systems\\' . (ucfirst($system->code));

        return new $className;
    }
}
