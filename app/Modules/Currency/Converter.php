<?php

namespace App\Modules\Currency;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Currency converter from https://free.currencyconverterapi.com/
 */
class Converter
{
    private $apiKey;

    const TAX = 10;

    const API_URL = 'https://free.currconv.com/api/v7/convert';

    public function __construct()
    {
        $this->apiKey = env('FREE_CURSCONV_KEY');
    }

    /**
     * @param string $from
     * @param string $to
     *
     * @return float
     * @throws GuzzleException
     */
    public function convert(string $from, string $to): float
    {
        $client = new Client();

        $response = $client->request('GET', static::API_URL, [
            'query' => [
                'q' => sprintf('%s_%s', strtoupper($from), strtoupper($to)),
                'compact' => 'ultra',
                'apiKey' => $this->apiKey
            ]
        ]);

        $result = json_decode($response->getBody(),true);

        return reset($result);
    }
}
