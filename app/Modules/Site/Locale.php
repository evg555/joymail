<?php

namespace App\Modules\Site;

use Illuminate\Support\Facades\App;

class Locale
{
    public static function set(): void
    {
        //TODO: Add checking existing cookies in array
        if (!Cookie::hasCountry()) {
            Cookie::setCountry();
        }

        if (!Cookie::hasLang()) {
            Cookie::setLang();
        }

        App::setLocale(Cookie::getLang());
    }

}
