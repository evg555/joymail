<?php

namespace App\Modules\Site;

use App\Models\Country;
use App\Models\Lang;
use App\Modules\IPLocator\Provider;
use App\Modules\IPLocator\Readers\DefaultReader;
use App\Modules\IPLocator\Readers\GeoIPReader;
use Illuminate\Support\Facades\Cookie as CookieFacade;

class Cookie
{
    private const COOKIE_LANG = 'lang';
    private const COOKIE_COUNTRY_ID = 'country_id';
    private const COOKIE_TIME = 60 * 24 * 30; // 30 days

    /**
     * @return bool
     */
    public static function hasLang(): bool
    {
        return request()->hasCookie(self::COOKIE_LANG);
    }

    /**
     * @return string
     */
    public static function getLang(): string
    {
        return (string) request()->cookie(self::COOKIE_LANG);
    }

    /**
     * @return void
     */
    public static function setLang(): void
    {
        $lang = substr(request()->getPreferredLanguage(), 0, 2) ?: 'en';
        $isExistLang = Lang::firstWhere('name', $lang);

        CookieFacade::queue('lang', $isExistLang ? $lang : config('app.locale'), self::COOKIE_TIME,
            null, null, false, false);
    }

    /**
     * @return bool
     */
    public static function hasCountry(): bool
    {
        return request()->hasCookie(self::COOKIE_COUNTRY_ID);
    }

    /**
     * @return void
     */
    public static function setCountry(): void
    {
        //TODO: вынести в DI контейнер или в настройки модуля
        $geoProvider = new Provider(new DefaultReader());
        $countryISO = $geoProvider->getCountryISO(request()->ip());

        $country = Country::firstWhere('code', $countryISO);
        $countryId = $country ? $country->id : 1;

        CookieFacade::queue(self::COOKIE_COUNTRY_ID, $countryId, self::COOKIE_TIME,
            null, null, false, false);
    }
}
