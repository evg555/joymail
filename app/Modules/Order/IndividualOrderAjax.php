<?php

namespace App\Modules\Order;

use App\Mail\Providers\IndividualOrderProvider;
use App\Modules\AjaxInterface;
use App\Modules\AjaxResult;
use Exception;

class IndividualOrderAjax implements AjaxInterface
{
    use AjaxResult;

    /**
     * @var
     */
    private $params;

    /**
     * @param array $params
     *
     * @return $this|AjaxInterface
     */
    public function setParams(array $params) : AjaxInterface
    {
        $this->params = $params;

        return $this;
    }

    /**
     * @return array
     */
    public function dispatch() : array
    {
        $data = request()->validate([
            'name' => 'required',
            'phone' => 'required',
            'type' => 'required'
        ]);

        try {
            IndividualOrderProvider::handle($data);
        } catch (Exception $e) {
            return $this->sendError($e->getMessage());
        }

        return $this->sendSuccess('Your order has sent!');
    }
}
