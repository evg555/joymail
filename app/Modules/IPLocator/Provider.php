<?php

namespace App\Modules\IPLocator;

use App\Modules\IPLocator\Readers\ReaderInterface;

class Provider
{
    private ReaderInterface $reader;

    public function __construct(ReaderInterface $reader)
    {
        $this->reader = $reader;
    }

    public function getCountryISO(string $ip): string
    {
        return $this->reader->getCountryIso($ip);
    }
}
