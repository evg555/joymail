<?php

namespace App\Modules\IPLocator\Readers;

class DefaultReader implements ReaderInterface
{
    private const DEFAULT_COUNTRY_ISO = 'en';

    public function getCountryIso(string $ip): string
    {
        return self::DEFAULT_COUNTRY_ISO;
    }
}
