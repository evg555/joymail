<?php

namespace App\Modules\IPLocator\Readers;

use GeoIp2\Database\Reader;
use GeoIp2\Exception\AddressNotFoundException;
use MaxMind\Db\Reader\InvalidDatabaseException;

/**
 * Class GeoIP
 * @package App\Helpers
 */
class GeoIPReader implements ReaderInterface
{
    private Reader $reader;

    /**
     * @throws InvalidDatabaseException
     */
    public function __construct()
    {
        //TODO: устарела база ip-адресов
        $this->reader = new Reader( __DIR__ . '/geoIP/GeoLite2-Country.mmdb');
    }

    /**
     * @throws AddressNotFoundException
     * @throws InvalidDatabaseException
     */
    public function getCountryISO(string $ip): string
    {
        $record = $this->reader->country($ip);

        return strtolower($record->country->isoCode);
    }
}
