<?php

namespace App\Modules\IPLocator\Readers;

interface ReaderInterface
{
    public function getCountryIso(string $ip): string;
}
