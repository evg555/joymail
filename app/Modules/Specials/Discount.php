<?php

namespace App\Modules\Specials;

use App\Models\Basket;
use App\Models\Company;
use App\Modules\AjaxInterface;
use App\Modules\AjaxResult;
use App\Models\Promocode;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class Discount
 * @package App\Modules\Specials
 */
class Discount implements AjaxInterface
{
    use AjaxResult;

    /**
     * @var
     */
    private $params;

    /**
     * @param array $params
     *
     * @return $this|AjaxInterface
     */
    public function setParams(array $params) : AjaxInterface
    {
        $this->params = [
            'code' => $params['code'] ?: null,
        ];

        return $this;
    }

    /**
     * @return array
     */
    public function dispatch() : array
    {
        foreach ($this->params as $name => $value) {
            if (is_null($value)) {
                return $this->sendError("Param {$name} is empty");
            }
        }

        return $this->activateCode($this->params['code']);
    }

    /**
     * @param string $code
     *
     * @return string[]
     */
    private function activateCode(string $code)
    {
        $basket = Basket::firstWhere('token', request()->cookie('basket_token'));
        $company = Company::firstWhere('city_id', $basket->city_id);
        $promocode = Promocode::where('name', $code)->where('company_id', $company->id)->first();

        $message = static::validateCode($promocode);

        if ($message) {
            return $this->sendError($message);
        }

        return $this->sendSuccess('Promocode is aplied', [
            'type' => $promocode->type,
            'amount' => $promocode->amount,
        ]);
    }

    public static function appliedCode(string $code)
    {
        $promocode = Promocode::firstWhere('name', $code);

        $promocode->count_apply--;
        $promocode->save();
    }

    /**
     * @param Collection $promocode
     *
     * @return string
     */
    private static function validateCode($promocode): string
    {
        $message = '';

        if (empty($promocode)) {
            $message = 'Promocode is not found or for another city';
        } else if (!$promocode->active) {
            $message = 'Promocode is not active';
        } else if (Carbon::now() > $promocode->date_expired) {
            $message = 'Promocode is expired';
        } else if ($promocode->count_apply <= 0) {
            $message = 'Promocode has already been applied';
        }

        return $message;
    }
}
