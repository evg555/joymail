<?php

namespace App\Modules;

/**
 * Interface AjaxInterface
 * @package App\Modules
 */
interface AjaxInterface
{
    /**
     * @param array $params
     *
     * @return AjaxInterface
     */
    public function setParams(array $params): AjaxInterface;

    public function dispatch(): array;
}
