<?php

namespace App\Modules\Feedbacks;

use App\Modules\Feedbacks\Repository\FeedbackRepository;
use Illuminate\Support\Collection;

class Provider
{
    /**
     * @return Collection|null
     */
    static public function getFeedbacks(): ?Collection
    {
        return FeedbackRepository::getAll();
    }
}
