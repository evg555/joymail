<?php

namespace App\Modules\Feedbacks\Repository;

use App\Modules\Feedbacks\Model\Feedback;
use App\Repositories\IRepository;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;

class FeedbackRepository implements IRepository
{
    /**
     * @return mixed
     */
    public static function getAll(): ?Collection
    {
        return Feedback::select(
            'feedbacks.id',
            'feedbacks.picture',
            'feedback_langs.body',
        )
            ->join('feedback_langs', 'feedback_langs.feedback_id', 'feedbacks.id')
            ->where('feedback_langs.lang', App::getLocale())
            ->get();
    }
}
