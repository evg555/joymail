<?php

namespace App\Modules\Feedbacks\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class FeedbackLang
 * @package App\Models
 */
class FeedbackLang extends Model
{
    /**
     * @return BelongsTo
     */
    public function feedback()
    {
        return $this->belongsTo(Feedback::class);
    }
}
