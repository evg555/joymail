<?php

namespace App\Modules\Feedbacks\Model;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Filesystem\Filesystem;

/**
 * Class Feedback
 * @package App\Models
 */
class Feedback extends Model
{
    protected $table = 'feedbacks';

    /**
     * @return HasMany
     */
    public function lang()
    {
        return $this->hasMany(FeedbackLang::class);
    }

    /**
     * Delete the model from the database.
     *
     * @return bool|null
     *
     * @throws Exception
     */
    public function delete()
    {
        app(Filesystem::class)->delete(public_path($this->picture));
        return parent::delete();
    }
}
