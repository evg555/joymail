<?php

namespace App\Modules\Template;

use App\Models\Basket;
use App\Repositories\CardRepository;
use App\Repositories\CityRepository;
use App\Repositories\CompanyRepository;
use App\Repositories\CountryRepository;
use App\Repositories\CurrencyRepository;
use App\Repositories\OfferRepository;
use App\Repositories\PostcardRepository;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

class Builder
{
    private static string $parentTemplate;
    private static string $template;
    private static ?Basket $basket;

    /**
     * @return Application|Factory|View
     * @throws Exception
     */
    public static function getTemplate(?Basket $basket, $isParentTemplate = false)
    {
        static::$template = $basket ? $basket->state : 'countries';
        static::$parentTemplate = $isParentTemplate ? 'main.partials.quiz.' . static::$template : 'main.send';
        static::$basket = $basket;

        $method = 'get' . ucfirst(static::$template);

        return static::$method();
    }

    private static function getCountries()
    {
        $countries = CountryRepository::getAll();

        return view(static::$parentTemplate, static::compactTemplateAndBasket() + compact('countries'));
    }

    private static function getCities()
    {
        $cities = CityRepository::getAllByCountryId(static::$basket->country_id);
        $currency = CurrencyRepository::getByCountryId(static::$basket->country_id);

        return view(
            static::$parentTemplate,
            static::compactTemplateAndBasket() + compact('cities', 'currency')
        );
    }

    private static function getCards()
    {
        $company = CompanyRepository::getByCityId(static::$basket->city_id);
        $cards = CardRepository::getAllByCompanyId($company->id);

        return view(static::$parentTemplate, static::compactTemplateAndBasket() + compact('cards', ));
    }

    private static function getMessage()
    {
        $card = CardRepository::getById(static::$basket->card_id);
        $company = CompanyRepository::getByCityId(static::$basket->city_id);
        $postcards = PostcardRepository::getAllByCompanyId($company->id);

        return view(static::$parentTemplate, static::compactTemplateAndBasket() + compact('card', 'postcards'));
    }

    private static function getOffers()
    {
        $company = CompanyRepository::getByCityId(static::$basket->city_id);
        $offers = OfferRepository::getAllByCompanyId($company->id);

        return view(static::$parentTemplate, static::compactTemplateAndBasket() + compact('offers'));
    }

    private static function getDetail()
    {
        static::$basket->load('offers');

        $country = CountryRepository::getCountryByCityId(static::$basket->city_id);
        $city = CityRepository::getById(static::$basket->city_id);
        $card = CardRepository::getById(static::$basket->card_id);

        return view(static::$parentTemplate, static::compactTemplateAndBasket() + compact( 'country', 'city', 'card'));
    }

    private static function compactTemplateAndBasket(): array
    {
        return [
            'template' => static::$template,
            'basket' => static::$basket
        ];
    }
}
