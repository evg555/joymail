<?php

namespace App\Modules\Basket;

use App\Helpers\Site;
use App\Models\Basket;
use App\Modules\AjaxInterface;
use App\Modules\AjaxResult;
use App\Modules\Template\Builder;
use Exception;
use Throwable;

/**
 * Get previous form
 */
class GetForm implements AjaxInterface
{
    use AjaxResult;

    /**
     * @var
     */
    private $params;

    private $actions = [
        'country',
        'city',
        'card',
        'message',
        'detail',
        'offers'
    ];

    const ALLOWED_STATES = [
        'countries',
        'cities',
        'offers',
        'cards',
        'message',
        'detail',
        'payment'
    ];

    /**
     * @param array $params
     *
     * @return $this|AjaxInterface
     */
    public function setParams(array $params) : AjaxInterface
    {
        $this->params = $params;

        return $this;
    }

    /**
     * @return string[]
     * @throws Throwable
     */
    public function dispatch() : array
    {
        $data = request()->validate([
            'state' => 'required|in:' . implode(',', static::ALLOWED_STATES)
        ]);

        Site::setCountryAndLocale();

        $prevIndex = array_search($data['state'], static::ALLOWED_STATES) - 2;
        $data['state'] = static::ALLOWED_STATES[$prevIndex];

        return $this->getForm($data);
    }

    /**
     * @param array $data
     *
     * @return string[]
     * @throws Throwable
     */
    private function getForm(array $data)
    {
        $basket = Basket::getBasketByToken();

        try {
            Basket::saveBasketOrFail($basket, $data);

            $view = Builder::getTemplate($basket, true);
        } catch (Exception $exception) {
            return $this->sendError($exception->getMessage());
        }

        return $this->sendSuccess(ucfirst($data['state']) . ' is got', [
            'view' => $view->render()
        ]);
    }
}
