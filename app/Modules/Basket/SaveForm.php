<?php

namespace App\Modules\Basket;

use App\Helpers\OrderHelper;
use App\Helpers\Site;
use App\Http\Validators\CardValidator;
use App\Http\Validators\CityValidator;
use App\Http\Validators\CountryValidator;
use App\Http\Validators\DetailValidator;
use App\Http\Validators\MessageValidator;
use App\Http\Validators\OffersValidator;
use App\Http\Validators\Validator;
use App\Models\Basket;
use App\Models\Card;
use App\Models\Company;
use App\Models\Postcard;
use App\Modules\AjaxInterface;
use App\Modules\AjaxResult;
use App\Modules\Currency\Converter;
use App\Modules\Specials\Discount;
use App\Modules\Template\Builder;
use Exception;
use Throwable;

/**
 * Class SaveForm
 * @package App\Modules\Basket
 */
class SaveForm implements AjaxInterface
{
    use AjaxResult;

    private array $params;

    private array $actions = [
        'country',
        'city',
        'card',
        'message',
        'detail',
        'offers'
    ];

    /**
     * @param array $params
     *
     * @return $this|AjaxInterface
     */
    public function setParams(array $params) : AjaxInterface
    {
        $this->params = $params;

        return $this;
    }

    /**
     * @return array
     */
    public function dispatch() : array
    {
        $method = $this->params['action'] ?? null;

        if (!$method || !in_array($method, $this->actions)) {
            return $this->sendError('Empty or not-existing action');
        }

        Site::setCountryAndLocale();

        $method = 'save' . ucfirst($method);

        return $this->$method();
    }

    /**
     * @return array
     * @throws Throwable
     */
    private function saveCountry() : array
    {
        $data = $this->validate(new CountryValidator());
        $basket = Basket::getBasketByToken();

        try {
            if ($basket) {
                $basket->fill($data);
                $basket->save();
            } else {
                $basket = Basket::create([
                    'token' => Site::getBasketCookie(),
                    'country_id' => $data['country_id'],
                    'state' => $data['state'],
                ]);
            }

            $view = Builder::getTemplate($basket, true);
        } catch (Exception $exception) {
            return $this->sendError($exception->getMessage());
        }

        return $this->sendSuccess('Country is saved', [
            'view' => $view->render()
        ]);
    }

    /**
     * @return array
     * @throws Throwable
     */
    private function saveCity() : array
    {
        $data = $this->validate(new CityValidator());
        $data['price'] = 0;

        $basket = Basket::getBasketByToken();

        try {
            Basket::saveBasketOrFail($basket, $data);
            $basket->offers()->delete();

            $view = Builder::getTemplate($basket, true);
        } catch (Exception $exception) {
            return $this->sendError($exception->getMessage());
        }

        return $this->sendSuccess('City is saved', [
            'view' => $view->render()
        ]);
    }

    /**
     * @return array
     * @throws Throwable
     */
    private function saveCard() : array
    {
        $data = $this->validate(new CardValidator());
        $basket = Basket::getBasketByToken();

        if ($basket->card_id) {
            $cardPrice = Card::find($basket->card_id)->price;
            $basket->price -= $cardPrice;
        }

        $data['price'] += $basket->price;

        try {
            Basket::saveBasketOrFail($basket, $data);

            $view = Builder::getTemplate($basket, true);
        } catch (Exception $exception) {
            return $this->sendError($exception->getMessage());
        }

        return $this->sendSuccess('Card is saved', [
            'view' => $view->render()
        ]);
    }

    /**
     * @return array
     * @throws Throwable
     */
    private function saveMessage() : array
    {
        $data = $this->validate(new MessageValidator());
        $data['photo'] = null;

        if (request()->hasFile('photo')) {
            $destination = 'upload/';

            $file = request()->file('photo');
            $filePath = 'img' . time() . '.' . $file->clientExtension();
            $file->move($destination, $filePath);

            $data['photo'] = $destination . $filePath;
        }

        if (isset($data['postcard_id']) && empty($data['photo'])) {
            $postcard = Postcard::find($data['postcard_id']);

            $data['photo'] = $postcard->image;
        }

        unset($data['postcard_id']);

        $basket = Basket::getBasketByToken();

        try {
            Basket::saveBasketOrFail($basket, $data);

            $view = Builder::getTemplate( $basket, true);
        } catch (Exception $exception) {
            return $this->sendError($exception->getMessage());
        }

        return $this->sendSuccess('Message is saved', [
            'view' => $view->render()
        ]);
    }

    /**
     * @return array
     * @throws Throwable
     */
    private function saveOffers() : array
    {
        $data = $this->validate(new OffersValidator());
        $offers = [];

        if (!empty($data['offers'])) {
            foreach ($data['offers'] as $id => $amount) {
                if (!$amount) {
                    continue;
                }

                $offers[] = [
                    'offer_id' => $id,
                    'amount' => $amount
                ];
            }

            unset($data['offers']);
        }

        $data['card_id'] = null;

        $basket = Basket::getBasketByToken();

        try {
            Basket::saveBasketOrFail($basket, $data);

            if (!empty($offers)) {
                $basket->offers()->delete();
                $basket->offers()->createMany($offers);
            }

            $view = Builder::getTemplate($basket, true);
        } catch (Exception $exception) {
            return $this->sendError($exception->getMessage());
        }

        return $this->sendSuccess('Offers is saved', [
            'view' => $view->render()
        ]);
    }

    /**
     * @return array
     * @throws Throwable
     */
    private function saveDetail() : array
    {
        $data = $this->validate(new DetailValidator());
        $offers = [];

        if (!empty($data['offers'])) {
            foreach ($data['offers'] as $id => $amount) {
                if (!$amount) {
                    continue;
                }

                $offers[] = [
                    'offer_id' => $id,
                    'amount' => $amount
                ];
            }

            unset($data['offers']);
        }

        if ($data['promo'] > 0) {
            $data['price'] -=  $data['promo'];

            Discount::appliedCode($data['promocode']);
        }

        unset($data['promo']);

        if ($data['price'] < 0) {
            $data['price'] = 0;
        }

        $basket = Basket::getBasketByToken();

        try {
            Basket::saveBasketOrFail($basket, $data);

            if (!empty($offers)) {
                $basket->offers()->delete();
                $basket->offers()->createMany($offers);
            }

            $basket->load('offers');

            $company = Company::where('city_id', $basket->city_id)
                ->with(['systems' => function($query) {
                    return $query->where('active', 1);
                }])
                ->first();

            $systems = $company->systems()->get();

            $orderHelper = OrderHelper::instance();
            $order = $orderHelper->createOrder($basket);

            if ($order) {
                $basket->delete();

                $priceInUah = '';

                if ($order->currency !== 'uah') {
                    $rate = app(Converter::class)->convert($order->currency, 'uah');
                    $priceInUah = sprintf('(%d Uah)', ceil($rate * $order->price) + Converter::TAX);
                }
            }

            $html = view('main.partials.quiz.payment',
                compact('order', 'systems', 'priceInUah'))->render();
        } catch (Exception $exception) {
            return $this->sendError($exception->getMessage());
        }

        return $this->sendSuccess('Detail is saved', [
            'view' => $html,
            'redirect' => $data['price'] == 0
        ]);
    }

    /**
     * @param Validator $validator
     *
     * @return array
     */
    private function validate(Validator $validator): array
    {
        return request()->validate($validator->getRules());
    }
}
