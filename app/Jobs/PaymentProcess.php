<?php

namespace App\Jobs;

use App\Helpers\OrderHelper;
use App\Modules\Payment\PaymentFactory;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

/**
 * Class PaymentProcess
 * @package App\Jobs
 */
class PaymentProcess implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var array
     */
    private $data;

    /**
     * Create a new job instance.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (empty($this->data)) {
            info('Data for payment is not recieved');
            return;
        }

        try {
            $system = PaymentFactory::getSystem($this->data['systemId']);
            $orderId = $system->savePayment($this->data);

            if ($orderId) {
                static::process($orderId);
            }
        } catch (Exception $e) {
            info('Payment is not saved: ' . $e->getMessage());
        }
    }

    /**
     * @param int $orderId
     */
    public static function process(int $orderId)
    {
        $order = OrderHelper::instance();

        $order->setOrder($orderId);

        if (!$order->getOrder()) {
            return;
        }

        $order->saveUser();
        $order->send('payed_order');
    }
}
