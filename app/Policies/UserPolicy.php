<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * Class UserPolicy
 * @package App\Policies
 */
class UserPolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     * @param $ability
     * @param \App\Http\Admin\User $item
     *
     * @return false
     */
    public function before(User $user, $ability, \App\Http\Admin\User $item)
    {
        return $this->adminPolicy($user);
    }

    /**
     * @param User $user
     * @param \App\Http\Admin\User $item
     *
     * @return bool
     */
    public function display(User $user, \App\Http\Admin\User $item)
    {
        return $this->adminPolicy($user);
    }

    /**
     * @param User $user
     * @param \App\Http\Admin\User $item
     *
     * @return bool
     */
    public function create(User $user, \App\Http\Admin\User $item)
    {
        return $this->adminPolicy($user);
    }

    /**
     * @param User $user
     * @param \App\Http\Admin\User $item
     *
     * @return bool
     */
    public function edit(User $user, \App\Http\Admin\User $item)
    {
        return $this->adminPolicy($user);
    }

    /**
     * @param User $user
     *
     * @return bool
     */
    private function adminPolicy(User $user)
    {
        if (!$user->isAdmin()) {
            return false;
        }

        return true;
    }
}
