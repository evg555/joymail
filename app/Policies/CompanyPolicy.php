<?php

namespace App\Policies;

use App\Http\Admin\Company;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * Class CompanyPolicy
 * @package App\Policies
 */
class CompanyPolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     * @param $ability
     * @param Company $item
     *
     * @return false
     */
    public function before(User $user, $ability, Company $item)
    {
        return $this->adminPolicy($user);
    }

    /**
     * @param User $user
     * @param Company $item
     *
     * @return bool
     */
    public function display(User $user, Company $item)
    {
        return $this->adminPolicy($user);
    }

    /**
     * @param User $user
     * @param Company $item
     *
     * @return bool
     */
    public function create(User $user, Company $item)
    {
        return $this->adminPolicy($user);
    }

    /**
     * @param User $user
     * @param Company $item
     *
     * @return bool
     */
    public function edit(User $user, Company $item)
    {
        return $this->adminPolicy($user);
    }

    /**
     * @param User $user
     *
     * @return bool
     */
    private function adminPolicy(User $user)
    {
        if (!$user->isAdmin()) {
            return false;
        }

        return true;
    }
}
