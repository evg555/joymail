<?php

namespace App\Policies;


use App\Http\Admin\PaymentSystem;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * Class UserPolicy
 * @package App\Policies
 */
class PaymentSystemPolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     * @param $ability
     * @param PaymentSystem $item
     *
     * @return false
     */
    public function before(User $user, $ability, PaymentSystem $item)
    {
        return $this->adminPolicy($user);
    }

    /**
     * @param User $user
     * @param PaymentSystem $item
     *
     * @return bool
     */
    public function display(User $user, PaymentSystem $item)
    {
        return $this->adminPolicy($user);
    }

    /**
     * @param User $user
     * @param PaymentSystem $item
     *
     * @return bool
     */
    public function create(User $user, PaymentSystem $item)
    {
        return $this->adminPolicy($user);
    }

    /**
     * @param User $user
     * @param PaymentSystem $item
     *
     * @return bool
     */
    public function edit(User $user, PaymentSystem $item)
    {
        return $this->adminPolicy($user);
    }

    /**
     * @param User $user
     *
     * @return bool
     */
    private function adminPolicy(User $user)
    {
        if (!$user->isAdmin()) {
            return false;
        }

        return true;
    }
}
