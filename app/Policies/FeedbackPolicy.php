<?php

namespace App\Policies;

use App\Http\Admin\Feedback;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * Class FeedbackPolicy
 * @package App\Policies
 */
class FeedbackPolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     * @param $ability
     * @param Feedback $item
     *
     * @return false
     */
    public function before(User $user, $ability, Feedback $item)
    {
        return $this->adminPolicy($user);
    }

    /**
     * @param User $user
     * @param Feedback $item
     *
     * @return bool
     */
    public function display(User $user, Feedback $item)
    {
        return $this->adminPolicy($user);
    }

    /**
     * @param User $user
     * @param Feedback $item
     *
     * @return bool
     */
    public function create(User $user, Feedback $item)
    {
        return $this->adminPolicy($user);
    }

    /**
     * @param User $user
     * @param Feedback $item
     *
     * @return bool
     */
    public function edit(User $user, Feedback $item)
    {
        return $this->adminPolicy($user);
    }

    /**
     * @param User $user
     *
     * @return bool
     */
    private function adminPolicy(User $user)
    {
        if (!$user->isAdmin()) {
            return false;
        }

        return true;
    }
}
