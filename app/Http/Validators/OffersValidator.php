<?php

namespace App\Http\Validators;

class OffersValidator extends Validator
{
    protected array $price;
    protected array $offers;
    protected array $state;

    public function __construct()
    {
        $this->price = ['price' => 'required|integer'];
        $this->offers = ['offers.*' => 'integer'];
        $this->state = ['state' => 'required|in:' . implode(',', self::ALLOWED_STATES)];
    }
}
