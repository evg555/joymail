<?php

namespace App\Http\Validators;

class CityValidator extends Validator
{
    protected array $cityId;
    protected array $currency;
    protected array $state;

    public function __construct()
    {
        $this->cityId = ['city_id' => 'required|integer'];
        $this->currency = ['currency' => 'required|min:3|max:4'];
        $this->state = ['state' => 'required|in:' . implode(',', self::ALLOWED_STATES)];
    }
}
