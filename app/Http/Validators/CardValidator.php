<?php

namespace App\Http\Validators;

class CardValidator extends Validator
{
    protected array $price;
    protected array $cardId;
    protected array $state;

    public function __construct()
    {
        $this->price = ['price' => 'required|integer'];
        $this->cardId = ['card_id' => 'required|integer'];
        $this->state = ['state' => 'required|in:' . implode(',', self::ALLOWED_STATES)];
    }
}
