<?php

namespace App\Http\Validators;

class CountryValidator extends Validator
{
    protected array $countryId;
    protected array $state;

    public function __construct()
    {
        $this->countryId = ['country_id' => 'required|integer'];
        $this->state = ['state' => 'required|in:' . implode(',', self::ALLOWED_STATES)];
    }
}
