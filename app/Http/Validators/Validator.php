<?php

namespace App\Http\Validators;

abstract class Validator
{
    protected const ALLOWED_STATES = [
        'countries',
        'cities',
        'cards',
        'message',
        'offers',
        'detail',
    ];

    public function getRules(): array
    {
        $result = [];

        foreach ($this as $value) {
            if ($value) {
                $result += $value;
            }
        }

        return $result;
    }
}
