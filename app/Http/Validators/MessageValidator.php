<?php

namespace App\Http\Validators;

class MessageValidator extends Validator
{
    protected array $message;
    protected array $postcardId;
    protected array $state;

    public function __construct()
    {
        $this->message = ['message' => 'required'];
        $this->postcardId = ['postcard_id' => 'int'];
        $this->state = ['state' => 'required|in:' . implode(',', self::ALLOWED_STATES)];
    }
}
