<?php

namespace App\Http\Validators;

class DetailValidator extends Validator
{
    protected array $promocode;
    protected array $promo;
    protected array $nameSender;
    protected array $nameReciever;
    protected array $emailSender;
    protected array $telSender;
    protected array $telReciever;
    protected array $price;
    protected array $offers;

    public function __construct()
    {
        $this->promocode = ['promocode' => 'max:255|nullable'];
        $this->promo = ['promo' => 'integer|nullable'];
        $this->nameSender = ['name_sender' => 'required|max:255'];
        $this->nameReciever = ['name_reciever' => 'required|max:255'];
        $this->emailSender = ['email_sender' => 'required|email|max:255'];
        $this->telSender = ['tel_sender' => 'required|max:255'];
        $this->telReciever = ['tel_reciever'=> 'required|max:255'];
        $this->price = ['price' => 'required'];
        $this->offers = ['offers.*' => 'integer'];
    }
}
