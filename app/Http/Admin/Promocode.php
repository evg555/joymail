<?php

namespace App\Http\Admin;

use AdminDisplay;
use AdminColumn;
use AdminDisplayFilter;
use AdminForm;
use AdminFormElement;
use App\Models\Company;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\Cancel;

/**
 * Class Order
 *
 * @property \App\Models\Order $model
 *
 * @see https://sleepingowladmin.ru/#/ru/model_configuration_section
 */
class Promocode extends Section implements Initializable
{
    /**
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $alias;

    /**
     * Initialize class.
     */
    public function initialize()
    {
        $this->addToNavigation()->setPriority(100)->setIcon('fas fa-percent');
    }

    /**
     * @param array $payload
     *
     * @return DisplayInterface
     */
    public function onDisplay($payload = [])
    {
        /* @var \App\Models\User $user */
        $user = auth()->user();

        $columns = [
            AdminColumn::text('id', '#')->setWidth('50px')
                ->setHtmlAttribute('class', 'text-center')
                ->setSearchCallback(function($column, $query, $search){
                    return $query
                        ->orWhere('id', 'like', '%'.$search.'%');
                })
            ,
            AdminColumn::link('name', 'Name')
                ->setSearchCallback(function($column, $query, $search){
                    return $query
                        ->orWhere('name', 'like', '%'.$search.'%');
                })
                ->setOrderable(function($column, $query, $direction) {
                    $query->orderBy('name', $direction);
                }),
            AdminColumn::text('active', 'Is active')
                ->setHtmlAttribute('class', 'text-center')
                ->setSearchCallback(function($column, $query, $search){
                    return $query
                        ->orWhere('active', '=', $search);
                })
                ->setOrderable(function($query, $direction) {
                    $query->orderBy('active', $direction);
                }),
            AdminColumn::text('count_apply', 'Applyed count')
                ->setHtmlAttribute('class', 'text-center')
                ->setSearchCallback(function($column, $query, $search){
                    return $query
                        ->orWhere('count_apply', '=', $search);
                })
                ->setOrderable(function($query, $direction) {
                    $query->orderBy('count_apply', $direction);
                }),
            AdminColumn::text('type', 'Type')
                ->setHtmlAttribute('class', 'text-center')
                ->setSearchCallback(function($column, $query, $search){
                    return $query
                        ->orWhere('type', '=', $search);
                })
                ->setOrderable(function($query, $direction) {
                    $query->orderBy('type', $direction);
                }),
            AdminColumn::text('amount', 'Amount')
                ->setHtmlAttribute('class', 'text-center')
                ->setSearchCallback(function($column, $query, $search){
                    return $query
                        ->orWhere('amount', 'like', '%'.$search.'%');
                })
                ->setOrderable(function($query, $direction) {
                    $query->orderBy('amount', $direction);
                }),
            AdminColumn::text('date_expired', 'Expired')
                ->setHtmlAttribute('class', 'text-center')
                ->setOrderable(function($query, $direction) {
                    $query->orderBy('date_expired', $direction);
                })
                ->setSearchable(false),
            AdminColumn::text('updated_at', 'Created / updated')
                ->setWidth('160px')
                ->setOrderable(function($query, $direction) {
                    $query->orderBy('updated_at', $direction);
                })
                ->setSearchable(false),
        ];

        $display = AdminDisplay::datatables()
            ->setName('firstdatatables')
            ->setOrder([[0, 'desc']])
            ->setDisplaySearch(true)
            ->paginate(50)
            ->setColumns($columns)
            ->setHtmlAttribute('class', 'table-primary table-hover th-center')
        ;

        if ($user->isManager()) {
            $display->setFilters(
                AdminDisplayFilter::field('company_id')
                    ->setValue($user->company_id)
                    ->setAlias('Company')
            );
        }

        return $display;
    }

    /**
     * @param int|null $id
     * @param array $payload
     *
     * @return FormInterface
     */
    public function onEdit($id = null, $payload = [])
    {
        /* @var \App\Models\User $user */
        $user = auth()->user();

        $companyField = $user->isManager() ?
            AdminFormElement::hidden('company_id')->setDefaultValue($user->company_id) :
            AdminFormElement::select('company_id', 'Company', Company::class)
                ->setDisplay('name')
                ->required();

        $form = AdminForm::card()->addBody([
            AdminFormElement::columns()->addColumn([
                AdminFormElement::text('id', 'ID')
                    ->setReadonly(true),
                $companyField,
                AdminFormElement::html('<hr>'),
                AdminFormElement::text('name', 'Name')->required(),
                AdminFormElement::checkbox('active', 'Is active')->setDefaultValue(true),
                AdminFormElement::datetime('date_expired', 'Expired')
                    ->setDefaultValue(date('Y-m-d H:i:s', strtotime('+3 months'))),
            ], 'col-xs-12 col-sm-6 col-md-4 col-lg-4')->addColumn([
                AdminFormElement::number('count_apply', 'Applyed count')->setDefaultValue(1),
                AdminFormElement::select('type', 'Type', ['fix' => 'Fix price', '%' => '% from price'])
                    ->required()
                    ->setSortable(false),
                AdminFormElement::number('amount', 'Amount')->required(),
            ], 'col-xs-12 col-sm-6 col-md-8 col-lg-8'),
        ]);

        $form->getButtons()->setButtons([
            'save'  => new Save(),
            'save_and_close'  => new SaveAndClose(),
            'cancel'  => (new Cancel()),
        ]);

        return $form;
    }

    /**
     * @param null $id
     * @param array $payload
     *
     * @return FormInterface
     */
    public function onCreate($id = null, $payload = [])
    {
        return $this->onEdit();
    }

    /**
     * @param Model $model
     *
     * @return bool
     */
    public function isDeletable(Model $model)
    {
        return true;
    }
}
