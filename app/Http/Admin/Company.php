<?php

namespace App\Http\Admin;

use AdminDisplay;
use AdminColumn;
use AdminForm;
use AdminFormElement;
use App\Models\City;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use Illuminate\Http\UploadedFile;

/**
 * Class Feedback
 *
 * @property \App\Models\Company $model
 *
 * @see https://sleepingowladmin.ru/#/ru/model_configuration_section
 */
class Company extends Section implements Initializable
{
    /**
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $alias;

    /**
     * Initialize class.
     */
    public function initialize()
    {
        $this->addToNavigation()->setPriority(100)->setIcon('far fa-building');
    }

    /**
     * @param array $payload
     *
     * @return DisplayInterface
     */
    public function onDisplay($payload = [])
    {
        $columns = [
            AdminColumn::text('id', '#')->setWidth('50px')
                ->setHtmlAttribute('class', 'text-center'),
            AdminColumn::text('city.default.name', 'City')
                ->setWidth('160px')
                ->setOrderable(function($query, $direction) {
                    $query->orderBy('city_id', $direction);
                }),
            AdminColumn::text('name', 'Name')
                ->setWidth('160px')
                ->setOrderable(function($query, $direction) {
                    $query->orderBy('name', $direction);
                }),
            AdminColumn::text('email', 'Email')
                ->setWidth('160px')
                ->setOrderable(function($query, $direction) {
                    $query->orderBy('email', $direction);
                }),
            AdminColumn::text('phone', 'Phone')
                ->setWidth('160px')
                ->setOrderable(function($query, $direction) {
                    $query->orderBy('phone', $direction);
                }),
            AdminColumn::text('address', 'Address')
                ->setWidth('160px')
                ->setOrderable(function($query, $direction) {
                    $query->orderBy('address', $direction);
                }),
        ];

        $display = AdminDisplay::datatables()
            ->setName('firstdatatables')
            ->setOrder([[0, 'desc']])
            ->setDisplaySearch(true)
            ->paginate(50)
            ->setColumns($columns)
            ->setHtmlAttribute('class', 'table-primary table-hover th-center')
        ;

        return $display;
    }

    /**
     * @param int|null $id
     * @param array $payload
     *
     * @return FormInterface
     */
    public function onEdit($id = null, $payload = [])
    {
        $cities = City::getCities();

        $form = AdminForm::card()->addBody([
            AdminFormElement::columns()->addColumn([
                AdminFormElement::text('id', 'ID')
                    ->setReadonly(true),
                AdminFormElement::select('city_id', 'City', $cities)
                    ->required()
                    ->setSortable(false),
                AdminFormElement::datetime('created_at')
                    ->setVisible(true)
                    ->setReadonly(true)
            ], 'col-xs-12 col-sm-4 col-md-4 col-lg-4')->addColumn([
                AdminFormElement::text('name', 'Name'),
                AdminFormElement::text('email', 'Email'),
                AdminFormElement::text('phone', 'Phone'),
                AdminFormElement::text('address', 'Address'),
            ], 'col-xs-12 col-sm-4 col-md-4 col-lg-4'),
        ]);

        $form->getButtons()->setButtons([
            'save'  => new Save(),
            'save_and_close'  => new SaveAndClose(),
            'cancel'  => (new Cancel()),
        ]);

        return $form;
    }

    /**
     * @param null $id
     * @param array $payload
     *
     * @return FormInterface
     */
    public function onCreate($id = null, $payload = [])
    {
        return $this->onEdit();
    }

    /**
     * @param Model $model
     *
     * @return bool
     */
    public function isDeletable(Model $model)
    {
        return true;
    }
}
