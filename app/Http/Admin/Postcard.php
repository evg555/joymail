<?php

namespace App\Http\Admin;

use AdminDisplay;
use AdminColumn;
use AdminForm;
use AdminFormElement;
use AdminDisplayFilter;
use Illuminate\Filesystem\Filesystem;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use Illuminate\Http\UploadedFile;
use App\Models\Company;

/**
 * Class Order
 *
 * @property \App\Models\Postcard $model
 *
 * @see https://sleepingowladmin.ru/#/ru/model_configuration_section
 */
class Postcard extends Section implements Initializable
{
    /**
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $alias;

    /**
     * Initialize class.
     */
    public function initialize()
    {
        $this->addToNavigation()->setPriority(100)->setIcon('fas fa-gift');
    }

    /**
     * @param array $payload
     *
     * @return DisplayInterface
     */
    public function onDisplay($payload = [])
    {
        /* @var \App\Models\User $user */
        $user = auth()->user();

        $columns = [
            AdminColumn::text('id', '#')->setWidth('50px')
                ->setHtmlAttribute('class', 'text-center'),
            AdminColumn::text('company.name', 'Company')
                ->setSearchCallback(function($column, $query, $search){
                    return $query
                        ->orWhere('company.name', 'like', '%'.$search.'%');
                })
                ->setOrderable(function($query, $direction) {
                    $query->orderBy('company_id', $direction);
                }),
            AdminColumn::text('title', 'Title')
                ->setHtmlAttribute('class', 'text-center')
                ->setOrderable(function($query, $direction) {
                    $query->orderBy('count', $direction);
                }),
            AdminColumn::image('image', 'Image')
                ->setHtmlAttribute('class', 'text-center'),
            AdminColumn::text('price', 'Price')
                ->setHtmlAttribute('class', 'text-center')
                ->setSearchCallback(function($column, $query, $search){
                    return $query
                        ->orWhere('price', '=', $search);
                })
                ->setOrderable(function($query, $direction) {
                    $query->orderBy('price', $direction);
                }),
            AdminColumn::text('sort', 'Sort')
                ->setHtmlAttribute('class', 'text-center')
                ->setOrderable(function($query, $direction) {
                    $query->orderBy('sort', $direction);
                }),
        ];

        $display = AdminDisplay::datatables()
            ->setName('firstdatatables')
            ->setOrder([[0, 'desc']])
            ->setDisplaySearch(true)
            ->paginate(50)
            ->setColumns($columns)
            ->setHtmlAttribute('class', 'table-primary table-hover th-center')
        ;

        if ($user->isManager()) {
            $display->setFilters(
                AdminDisplayFilter::field('company_id')
                    ->setValue($user->company_id)
                    ->setAlias('Company')
            );
        }

        return $display;
    }

    /**
     * @param int|null $id
     * @param array $payload
     *
     * @return FormInterface
     */
    public function onEdit($id = null, $payload = [])
    {

        /* @var \App\Models\User $user */
        $user = auth()->user();

        $companyField = $user->isManager() ?
            AdminFormElement::text('company.name', 'Company')
                ->setDefaultValue($user->company_id)
                ->setReadonly(true) :
            AdminFormElement::select('company_id', 'Company', Company::class)
                ->setDisplay('name')
                ->required();

        $form = AdminForm::card()->addBody([
            AdminFormElement::columns()->addColumn([
                AdminFormElement::text('id', 'ID')
                    ->setReadonly(true),
                $companyField,
                AdminFormElement::html('<hr>'),
                AdminFormElement::text('code', 'Alt')->required(),
                AdminFormElement::text('title', 'Title')->required(),
                AdminFormElement::textarea('description', 'Description'),
                AdminFormElement::number('price', 'Price')->required(),
                AdminFormElement::number('sort', 'Sort')->setDefaultValue(100),
            ], 'col-xs-12 col-sm-4 col-md-4 col-lg-4')->addColumn([
                AdminFormElement::image('image', 'Image')
                    ->required()
                    ->setUploadPath(function(UploadedFile $file) {
                        return 'img/card_templates';
                }),
            ], 'col-xs-12 col-sm-4 col-md-4 col-lg-4'),
        ]);

        $form->getButtons()->setButtons([
            'save'  => new Save(),
            'save_and_close'  => new SaveAndClose(),
            'cancel'  => (new Cancel()),
        ]);

        return $form;
    }

    /**
     * @param null $id
     * @param array $payload
     *
     * @return FormInterface
     */
    public function onCreate($id = null, $payload = [])
    {
        return $this->onEdit();
    }

    /**
     * @param Model $model
     *
     * @return bool
     */
    public function isDeletable(Model $model)
    {
        return true;
    }
}
