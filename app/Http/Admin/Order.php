<?php

namespace App\Http\Admin;

use AdminDisplay;
use AdminColumn;
use AdminDisplayFilter;
use AdminForm;
use AdminFormElement;
use App\Models\City;
use Illuminate\Filesystem\Filesystem;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use App\Models\Order as OrderEntity;

/**
 * Class Order
 *
 * @property OrderEntity $model
 *
 * @see https://sleepingowladmin.ru/#/ru/model_configuration_section
 */
class Order extends Section implements Initializable
{
    /**
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $alias;

    /**
     * Initialize class.
     */
    public function initialize()
    {
        $this->addToNavigation()->setPriority(100)->setIcon('far fa-envelope');
    }

    /**
     * @param array $payload
     *
     * @return DisplayInterface
     */
    public function onDisplay($payload = [])
    {
        /* @var \App\Models\User $user */
        $user = auth()->user();

        $columns = [
            AdminColumn::text('id', '#')
                ->setWidth('50px')
                ->setHtmlAttribute('class', 'text-center')
                ->setSearchCallback(function($column, $query, $search){
                    return $query
                        ->orWhere('id', 'like', '%'.$search.'%');
                }),
            AdminColumn::text('city.default.name', 'City')
                ->setOrderable(function($query, $direction) {
                    $query->orderBy('city_id', $direction);
                }),
            AdminColumn::text('status', 'Status')
                ->setOrderable(function($column, $query, $direction) {
                    $query->orderBy('status', $direction);
                }),
            AdminColumn::link('name_sender', 'Sender\'s name')
                ->setSearchCallback(function($column, $query, $search){
                    return $query
                        ->orWhere('name_sender', 'like', '%'.$search.'%');
                })
                ->setOrderable(function($column, $query, $direction) {
                    $query->orderBy('name_sender', $direction);
                }),
            AdminColumn::text('email_sender', 'Sender\'s email')
                ->setHtmlAttribute('class', 'text-center')
                ->setSearchCallback(function($column, $query, $search){
                    return $query
                        ->orWhere('email_sender', 'like', '%'.$search.'%');
                })
                ->setOrderable(function($query, $direction) {
                    $query->orderBy('email_sender', $direction);
                }),
            AdminColumn::text('tel_sender', 'Sender\'s phone')
                ->setHtmlAttribute('class', 'text-center')
                ->setSearchCallback(function($column, $query, $search){
                    return $query
                        ->orWhere('tel_sender', 'like', '%'.$search.'%');
                })
                ->setOrderable(function($query, $direction) {
                    $query->orderBy('tel_sender', $direction);
                }),
            AdminColumn::text('price', 'Price')
                ->setHtmlAttribute('class', 'text-center')
                ->setSearchCallback(function($column, $query, $search){
                    return $query
                        ->orWhere('price', 'like', '%'.$search.'%');
                })
                ->setOrderable(function($query, $direction) {
                    $query->orderBy('price', $direction);
                }),
            AdminColumn::text('updated_at', 'Created / updated')
                ->setWidth('160px')
                ->setOrderable(function($query, $direction) {
                    $query->orderBy('updated_at', $direction);
                })
                ->setSearchable(false),
        ];

        $display = AdminDisplay::datatables()
            ->setName('firstdatatables')
            ->setOrder([[0, 'desc']])
            ->setDisplaySearch(true)
            ->paginate(50)
            ->setColumns($columns)
            ->setHtmlAttribute('class', 'table-primary table-hover th-center')
        ;

        if ($user->isManager()) {
            $display->setFilters(
                AdminDisplayFilter::field('city_id')
                    ->setValue($user->company()->first()->city_id)
                    ->setAlias('City')
            );
        }

        return $display;
    }

    /**
     * @param int|null $id
     * @param array $payload
     *
     * @return FormInterface
     */
    public function onEdit($id = null, $payload = [])
    {
        $form = AdminForm::card()->addBody([
            AdminFormElement::columns()->addColumn([
                AdminFormElement::text('id', 'ID')
                    ->setReadonly(true),
                AdminFormElement::html('<hr>'),
                AdminFormElement::select('city_id', 'City', City::class)
                    ->setLoadOptionsQueryPreparer(function($element, $query) {
                        return $query
                            ->select('cities.id', 'city_langs.name')
                            ->join('city_langs','city_langs.city_id', 'cities.id')
                            ->where('city_langs.lang_id', 2);
                    })
                    ->setDisplay('name')
                    ->setReadonly(true),
                AdminFormElement::select('status', 'Status', [
                    OrderEntity::ORDER_STATUS_NEW => 'New',
                    OrderEntity::ORDER_STATUS_PAYED => 'Payed'
                ])->required(),
                AdminFormElement::checkbox('payment.on_delivery', 'By cash or terminal')->setReadonly(true),
                AdminFormElement::text('name_sender', 'Sender\'s name')->required(),
                AdminFormElement::text('email_sender', 'Sender\'s email')->required(),
                AdminFormElement::text('tel_sender', 'Sender\'s phone')->required(),
                AdminFormElement::text('name_reciever', 'Reciever\'s name')->required(),
                AdminFormElement::text('tel_reciever', 'Reciever\'s phone')->required(),
                //AdminFormElement::text('address', 'Address')->required(),
                AdminFormElement::html('<hr>'),
                AdminFormElement::text('promocode', 'Applied promocode')->setReadonly(true),
                AdminFormElement::number('price', 'Price')->setReadonly(true),
                AdminFormElement::text('currency', 'Currency')->setReadonly(true),
                AdminFormElement::html('<hr>'),
                AdminFormElement::datetime('created_at')
                    ->setVisible(true)
                    ->setReadonly(true)
                ,
            ], 'col-xs-12 col-sm-6 col-md-4 col-lg-4')->addColumn([
                AdminFormElement::textarea('message', 'Message')->required(),
                AdminFormElement::image('photo'),
                AdminFormElement::html('<hr>'),
                AdminFormElement::text('letter_type', 'Letter type')->setReadonly(true),
                AdminFormElement::view('admin.offers', $data = [], function() {
                    //
                }),
                AdminFormElement::html('<hr>'),
                AdminFormElement::view('admin.payment', $data = [], function() {
                    //
                })
            ], 'col-xs-12 col-sm-6 col-md-8 col-lg-8'),
        ]);

        $form->getButtons()->setButtons([
            'save'  => new Save(),
            'save_and_close'  => new SaveAndClose(),
            'cancel'  => (new Cancel()),
        ]);

        return $form;
    }

    /**
     * @param null $id
     * @param array $payload
     *
     * @return FormInterface
     */
    public function onCreate($id = null, $payload = [])
    {
        /* @var \App\Models\User $user */
        $user = auth()->user();

        $cityField = $user->isAdmin() ? AdminFormElement::select('city_id', 'City', City::class)
                ->setLoadOptionsQueryPreparer(function($element, $query) {
                    return $query
                        ->select('cities.id', 'city_langs.name')
                        ->join('city_langs','city_langs.city_id', 'cities.id')
                        ->where('city_langs.lang_id', 2);
                })
                ->setDisplay('name')
                ->required()
                : AdminFormElement::hidden('city_id')->setDefaultValue($user->company()->first()->city_id);

        $form = AdminForm::card()->addBody([
            AdminFormElement::columns()->addColumn([
                AdminFormElement::text('id', 'ID')
                    ->setReadonly(true),
                AdminFormElement::html('<hr>'),
                $cityField,
                AdminFormElement::text('name_sender', 'Sender\'s name')->required(),
                AdminFormElement::text('email_sender', 'Sender\'s email')->required(),
                AdminFormElement::text('tel_sender', 'Sender\'s phone')->required(),
                AdminFormElement::text('name_reciever', 'Reciever\'s name')->required(),
                AdminFormElement::text('tel_reciever', 'Reciever\'s phone')->required(),
                //AdminFormElement::text('address', 'Address')->required(),
                AdminFormElement::html('<hr>'),
                AdminFormElement::number('price', 'Price')->required(),
                AdminFormElement::select('currency', 'Currency', [
                    'GEL' => 'GEL',
                ])->required(),
                AdminFormElement::html('<hr>'),
            ], 'col-xs-12 col-sm-6 col-md-4 col-lg-4')->addColumn([
                AdminFormElement::textarea('message', 'Message')->required(),
                AdminFormElement::image('photo'),
                AdminFormElement::html('<hr>'),
                AdminFormElement::select('letter_type', 'Letter type', [
                    'love-letter' => 'Love Letter',
                    'standart' => 'Standart',
                    'manuscript' => 'Manuscript',
                    'free'  => 'Free',
                ])->required(),
            ], 'col-xs-12 col-sm-6 col-md-8 col-lg-8'),
        ]);

        $form->getButtons()->setButtons([
            'save'  => new Save(),
            'save_and_close'  => new SaveAndClose(),
            'cancel'  => (new Cancel()),
        ]);

        return $form;
    }

    /**
     * @param Model $model
     *
     * @return bool
     */
    public function isDeletable(Model $model)
    {
        return false;
    }
}
