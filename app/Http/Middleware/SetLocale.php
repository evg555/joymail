<?php

namespace App\Http\Middleware;

use App\Modules\Site\Locale;
use Closure;

class SetLocale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Locale::set();

        return $next($request);
    }
}
