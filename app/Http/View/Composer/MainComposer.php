<?php

namespace App\Http\View\Composer;

use App\Models\Lang;
use Carbon\Carbon;
use Illuminate\View\View;

/**
 * Class MainComposer
 * @package App\Http\View\Composer
 */
class MainComposer
{
    public function compose(View $view)
    {
        $expired = now()->addMonth();

        $langs = cache()->remember('langs', $expired, function() {
            return Lang::orderBy('sort', 'asc')->get();
        });

        $view->with('langs', $langs);
    }
}
