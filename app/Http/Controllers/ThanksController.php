<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

class ThanksController extends Controller
{
    /**
     * @return Application|Factory|View
     * @throws Exception
     */
    public function index()
    {
        return view('thanks.index');
    }
}
