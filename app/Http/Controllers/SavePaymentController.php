<?php

namespace App\Http\Controllers;

use App\Helpers\OrderHelper;
use App\Models\Order;
use App\Models\Payment;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

/**
 *
 */
class SavePaymentController extends Controller
{
    /**
     * @return Application|Factory|View
     * @throws Exception
     */
    public function index(int $orderId)
    {
        Order::findOrFail($orderId);

        $params = [
            'order_id' => $orderId,
            'on_delivery' => true,
            'invoice_id' => '',
            'transaction_id' => '',
            'transaction_state' => 0,
            'error' => 'PERFORMED'
        ];

        $payment = new Payment();
        $payment->fill($params);
        $payment->save();

        $order = OrderHelper::instance();
        $order->setOrder($orderId);

        $order->saveUser();
        $order->send('pay_on_delivery_order');

        return redirect(route('thanks'));
    }
}
