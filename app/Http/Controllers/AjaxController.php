<?php

namespace App\Http\Controllers;

use App\Modules\AjaxResult;
use App\Modules\Basket\GetForm;
use App\Modules\Basket\SaveForm;
use App\Modules\Order\IndividualOrderAjax;
use App\Modules\Payment\PaymentAjax;
use App\Modules\Specials\Discount;
use Illuminate\Http\JsonResponse;

/**
 * Class OrderController
 * @package App\Http\Controllers
 */
class AjaxController extends Controller
{
    use AjaxResult;

    private $handlers = [
        'discount' => Discount::class,
        'individual' => IndividualOrderAjax::class,
        'prevStep' => GetForm::class,
        'nextStep' => SaveForm::class,
        'getPaymentButton' => PaymentAjax::class
    ];

    /**
     * @param $action
     *
     * @return JsonResponse
     */
    public function index($action)
    {
        $handler = $this->buildHandler($action);

        if (!$handler) {
            return response()->json($this->sendError('Invalid ajax action: ' . $action));
        }

        $result = $handler->setParams(request()->all())->dispatch();

        return response()->json($result);
    }

    /**
     * @param string $action
     *
     * @return false|mixed
     */
    private function buildHandler(string $action)
    {
        if (!array_key_exists($action, $this->handlers)) {
            return false;
        }

        return new $this->handlers[$action];
    }
}

