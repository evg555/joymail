<?php

namespace App\Http\Controllers;

use App\Models\Company;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

/**
 *
 */
class AgreementController extends Controller
{
    /**
     * @return Application|Factory|View
     * @throws Exception
     */
    public function index()
    {
        $companies = Company::all();

        return view('agreement.index', compact('companies'));
    }
}
