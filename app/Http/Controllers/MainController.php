<?php

namespace App\Http\Controllers;

use App\Modules\Feedbacks\Provider;
use App\Modules\Template\Builder;
use App\Repositories\BasketRepository;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

/**
 * Class MainController
 * @package App\Http\Controllers
 */
class MainController extends Controller
{
    /**
     * @return Application|Factory|View
     * @throws Exception
     */
    public function index()
    {
        $feedbacks = cache()->remember('feedbacks', now()->addMonth(), function() {
            return Provider::getFeedbacks();
        });

        return view('main.index', compact('feedbacks'));
    }

    /**
     * @return Application|Factory|View
     * @throws Exception
     */
    public function checkout()
    {
        $basket = BasketRepository::get();

        return Builder::getTemplate($basket);
    }
}
