<?php

namespace App\Http\Controllers;

use App\Jobs\PaymentProcess;

/**
 * Class PaymentController
 * @package App\Http\Controllers
 */
class PaymentController extends Controller
{
    public function index()
    {
        if (request()->isMethod('GET')) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Method GET is not supported. Use method POST'
            ]);
        }

        $data = request()->all();
        $data['site_url'] = request()->getSchemeAndHttpHost();
        $data['systemId'] = 1;

        info('ipay_payment_details: ' . serialize($data));

        PaymentProcess::dispatch($data);

        return response()->json([
            'status' => 'success'
        ]);
    }
}
