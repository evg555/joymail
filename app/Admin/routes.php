<?php

Route::get('', ['as' => 'admin.dashboard', function () {
    $content = view('admin.settings');
    return AdminSection::view($content, 'Settings');
}]);

Route::get('/logout', function () {
    auth()->logout();
    return redirect()->back();
})->name('admin.logout');
