<?php

namespace App\Helpers;

/**
 * Trait Singletone
 * @package App\Helpers
 */
trait Singletone
{
    private static $instance;

    private function __construct() {}

    private function __clone() {}

    /**
     * @return static
     */
    public static  function instance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }
}
