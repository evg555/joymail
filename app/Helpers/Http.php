<?php

namespace App\Helpers;

/**
 * Class Http
 * @package App\Helpers
 */
class Http
{
    /**
     * @return string
     */
    public static function getSchemaProtocol()
    {
        return self::isHttps() ? 'https://' : 'http://';
    }


    /**
     * @return bool
     */
    public static function isHttps(): bool
    {
        return !empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on';
    }
}
