<?php

namespace App\Helpers;

use App\Mail\Providers\MailProvider;
use App\Mail\Providers\NewOrderProvider;
use App\Mail\Providers\OnDeliveryOrderProvider;
use App\Mail\Providers\PayedOrderProvider;
use App\Models\Basket;
use App\Models\City;
use App\Models\Company;
use App\Models\Offer;
use App\Models\Order;
use App\Models\User;
use App\Models\Card;
use App\Repositories\CityRepository;

/**
 * Class OrderHelper
 * @package App\Helpers
 */
class OrderHelper
{
    use Singletone;

    const LETTER_TYPES = [
        'standart' => 'Standard',
        'love-letter' => 'Love Letter',
        'manuscript' => 'Manuscript'
    ];

    const MAIL_PROVIDERS = [
        'new_order' => NewOrderProvider::class,
        'payed_order' => PayedOrderProvider::class,
        'pay_on_delivery_order' => OnDeliveryOrderProvider::class
    ];
    /**
     * @var Order
     */
    private $order = null;

    /**
     * @var array
     */
    private $offers = [];

    /**
     * @param Basket $basket
     *
     * @return Order|null
     */
    public function createOrder(Basket $basket): ?Order
    {
        foreach ($basket->offers as $basketOffer) {
            $offer = Offer::where('id', $basketOffer->offer_id)
                ->with(['langs' => function($query) {
                    return $query->where('lang', 'en');
                }])
                ->first();

            $this->offers[] = [
                'name' => $offer->langs->first()->name,
                'price' => $offer->price,
                'amount' => $basketOffer->amount
            ];
        }

        $card = Card::find($basket->card_id);

        $data = [
            'message' => $basket->message,
            'photo' => $basket->photo,
            'city_id' => $basket->city_id,
            'name_sender' => $basket->name_sender,
            'email_sender' => $basket->email_sender,
            'tel_sender' => $basket->tel_sender,
            'name_reciever' => $basket->name_reciever,
            'tel_reciever' => $basket->tel_reciever,
            'promocode' => $basket->promocode,
            'price' => $basket->price,
            'currency' => $basket->currency,
            'letter_type' => $card->code,
        ];

        $order = Order::create($data);
        $order->offers()->createMany($this->offers);

        $this->order = $order;

        $this->send('new_order');

        return $this->order;
    }

    public function saveUser(): void
    {
        if (is_null($this->order)) {
            return;
        }

        $existUser = User::where('email', $this->order->email_sender)->count() > 0;

        if (!$existUser) {
            User::create([
                'name' => $this->order->name_sender,
                'email' => $this->order->email_sender,
                'password' => password_hash(time(), PASSWORD_BCRYPT, ['cost' => 10])
            ]);
        }
    }

    /**
     * @param $status
     *
     * @return void
     */
    public function send($status)
    {
        if (is_null($this->order)) {
            return;
        }

        $this->order->load('offers');
        $data = $this->order->toArray();

        if (array_key_exists($data['letter_type'], static::LETTER_TYPES)) {
            $data['letter_type'] = static::LETTER_TYPES[$data['letter_type']];
        } else {
            $data['letter_type'] = 'Default';
        }

        $data['city'] = CityRepository::getById($data['city_id'])->name;
        $data['site_url'] = request()->getSchemeAndHttpHost();

        $users = Company::where('city_id', $data['city_id'])
            ->with('users')
            ->first()
            ->users()
            ->get();

        $data['email_manager'] = [];

        foreach ($users as $user) {
            $data['email_manager'][] = $user->email;
        }

        /* @var MailProvider $mailProvider */
        $mailProvider = static::MAIL_PROVIDERS[$status];
        $mailProvider::handle($data);
    }

    /**
     * @param int|null $orderId
     *
     * @return mixed
     */
    public function getOrder(int $orderId = null): ?Order
    {
        if (!$orderId) {
            return $this->order;
        }

        $order = Order::find($orderId);

        if (empty($order)) {
            info('Order with id ' . $orderId . ' is not found');
            return null;
        }

        return $order;
    }

    /**
     * @return array
     */
    public function getOffers(): array
    {
        return $this->offers;
    }

    /**
     * @param int $orderId
     */
    public function setOrder(int $orderId)
    {
        $this->order = $this->getOrder($orderId);
    }

    /**
     * @param int $orderId
     */
    public static function changeOrderStatus(int $orderId)
    {
        $order = Order::find($orderId);
        $order->status = Order::ORDER_STATUS_PAYED;
        $order->save();
    }
}
