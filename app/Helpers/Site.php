<?php

namespace App\Helpers;

use App\Models\Country;
use App\Models\Lang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cookie;

/**
 * Class Site
 * @package App\Helpers
 */
class Site
{
    public static function setBasketCookie(): string
    {
        $basketCookie = request()->cookie('basket_token');

        if (!$basketCookie) {
            $basketCookie = csrf_token();
            Cookie::queue('basket_token', $basketCookie, 60 * 24, null, null, false, false);
        }

        return $basketCookie;
    }

    public static function getBasketCookie(): string
    {
        return request()->cookie('basket_token') ?? '';
    }
}
