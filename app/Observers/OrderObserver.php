<?php

namespace App\Observers;

use App\Models\Order;

/**
 * Class OrderObserver
 * @package App\Observers
 */
class OrderObserver
{
    /**
     * Handle the order "created" event.
     *
     * @param Order $order
     *
     * @return void
     */
    public function creating(Order $order)
    {
        if (!$order->city_id && auth()->user()->isManager()) {
            $order->city_id = auth()->user()->company()->first()->city_id;
        }
    }
}
