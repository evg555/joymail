<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Mailer extends Mailable
{
    use Queueable, SerializesModels;

    private $data;
    private $template;

    /**
     * Create a new message instance.
     *
     * @param string $template
     * @param string $subject
     * @param array $data
     */
    public function __construct(string $template, string $subject, array $data)
    {
        $this->template = $template;
        $this->subject = $subject;
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->subject)->view('emails.' . $this->template, ['data' => $this->data]);
    }
}
