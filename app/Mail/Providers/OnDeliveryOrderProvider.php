<?php

namespace App\Mail\Providers;

/**
 * Class OnDeliveryOrderProvider
 * @package App\Mail\Providers
 */
class OnDeliveryOrderProvider extends MailProvider
{
    protected static $subject = 'Pay On Delivery Order';

    /**
     * @param array $data
     */
    public static function handle(array $data)
    {
        static::$subject .= ' from ' . $data['city'];

        static::send([
            'orders.on_delivery.seller' => array_merge(static::getEmails(), $data['email_manager']),
            'orders.on_delivery.customer' => $data['email_sender'] ?? ''
        ], $data);
    }
}
