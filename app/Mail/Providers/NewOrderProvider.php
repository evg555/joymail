<?php

namespace App\Mail\Providers;

use Illuminate\Support\Facades\Mail;

/**
 * Class NewOrderProvider
 * @package App\Mail\Providers
 */
class NewOrderProvider extends MailProvider
{
    protected static $subject = 'New Order';

    /**
     * @param array $data
     */
    public static function handle(array $data)
    {
        static::$subject .= ' from ' . $data['city'];

        static::send([
            'orders.new.seller' => array_merge(static::getEmails(), $data['email_manager']),
            'orders.new.customer' => $data['email_sender'] ?? ''
        ], $data);
    }
}
