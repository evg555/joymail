<?php

namespace App\Mail\Providers;

use Illuminate\Support\Facades\Mail;

/**
 * Class IndividualOrderProvider
 * @package App\Mail\Providers
 */
class IndividualOrderProvider extends MailProvider
{
    protected static $subject = 'New Individual Order';

    /**
     * @param array $data
     */
    public static function handle(array $data)
    {
        static::send(['support' => static::getEmails()], $data);
    }
}
