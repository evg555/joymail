<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\CountryLang;

/**
 * Class CreateCountryLangsTable
 */
class CreateCountryLangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('country_langs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('country_id')->unsigned();
            $table->bigInteger('lang_id')->unsigned();
            $table->string('name');

            $table->foreign('country_id')->references('id')->on('countries');
            $table->foreign('lang_id')->references('id')->on('langs');
        });

        $langs = [
            [
                'country_id' => 1,
                'lang_id' => 1,
                'name' => 'საქართველო'
            ],
            [
                'country_id' => 1,
                'lang_id' => 2,
                'name' => 'Georgia'
            ],
            [
                'country_id' => 1,
                'lang_id' => 3,
                'name' => 'Грузия'
            ],
            [
                'country_id' => 1,
                'lang_id' => 4,
                'name' => 'Грузія'
            ],
            [
                'country_id' => 2,
                'lang_id' => 1,
                'name' => 'უკრაინა'
            ],
            [
                'country_id' => 2,
                'lang_id' => 2,
                'name' => 'Ukraine'
            ],
            [
                'country_id' => 2,
                'lang_id' => 3,
                'name' => 'Украина'
            ],
            [
                'country_id' => 2,
                'lang_id' => 4,
                'name' => 'Україна'
            ]
        ];

        foreach ($langs as $lang) {
            $model = new CountryLang();
            $model->timestamps = false;
            $model->fill($lang);
            $model->save();
        }


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('country_langs');
    }
}
