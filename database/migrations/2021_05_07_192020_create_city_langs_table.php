<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\CityLang;

/**
 * Class CreateCityLangsTable
 */
class CreateCityLangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('city_langs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('city_id')->unsigned();
            $table->bigInteger('lang_id')->unsigned();
            $table->string('name');

            $table->foreign('city_id')->references('id')->on('cities');
            $table->foreign('lang_id')->references('id')->on('langs');
        });

        $langs = [
            ['ბათუმი', 'თბილისი', 'ქუთაისი', 'რუსთავი'],
            ['Batumi', 'Tbilisi', 'Kutaisi', 'Rustavi'],
            ['Батуми', 'Тбилиси', 'Кутаиси', 'Рустави'],
            ['Батумі', 'Тбілісі', 'Кутаїсі', 'Руставі'],
        ];

        $langs2 = [
            ['დნეპრი', 'კიევი', 'ოდესა', 'პოლტავა'],
            ['Dnipro', 'Kiev', 'Odessa', 'Poltava'],
            ['Днепр', 'Киев', 'Одесса', 'Полтава'],
            ['Дніпро', 'Київ', 'Одеса', 'Полтава'],
        ];

        foreach ($langs as $langId => $lang) {
            foreach ($lang as $i => $name) {
                $model = new CityLang();
                $model->timestamps = false;
                $model->city_id = $i + 1;
                $model->lang_id = $langId + 1;
                $model->name = $name;
                $model->save();
            }
        }

        foreach ($langs2 as $langId => $lang) {
            foreach ($lang as $i => $name) {
                $model = new CityLang();
                $model->timestamps = false;
                $model->city_id = $i + 5;
                $model->lang_id = $langId + 1;
                $model->name = $name;
                $model->save();
            }
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('city_langs');
    }
}
