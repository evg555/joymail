<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\City;

/**
 * Class NewCitiesTable
 */
class NewCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Schema::dropIfExists('cities');
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        Schema::create('cities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('country_id')->unsigned();
            $table->integer('sort')->default(100);

            $table->foreign('country_id')
                ->references('id')->on('countries');
        });

        for ($country = 1; $country < 3; $country++) {
            for ($city = 1; $city < 5; $city++) {
                $model = new City();
                $model->timestamps = false;
                $model->country_id = $country;
                $model->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cities');
    }
}
