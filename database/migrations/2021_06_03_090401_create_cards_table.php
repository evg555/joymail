<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Card;

class CreateCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cards', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code');
            $table->string('title');
            $table->string('image');
            $table->integer('price');
            $table->integer('sort')->default(100);
            $table->unsignedBigInteger('company_id');
            $table->timestamps();

            $table->foreign('company_id')->references('id')->on('companies');
        });

        $cards = [
            [
                'code' => 'standart',
                'price' => 10,
                'title' => 'white',
                'image' => '/img/white_joyka@1x.jpg',
                'company_id' => 1,
                'sort' => 1
            ],
            [
                'code' => 'love-letter',
                'price' => 12,
                'title' => 'craft',
                'image' => '/img/craft_joyka@1x.jpg',
                'company_id' => 1,
                'sort' => 2
            ]
        ];

        Card::insert($cards);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cards');
    }
}
