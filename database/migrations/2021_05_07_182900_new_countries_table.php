<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Country;

/**
 * Class DropColumnsCountriesTable
 */
class NewCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Schema::dropIfExists('countries');
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        Schema::create('countries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code');
            $table->integer('sort')->default('100');
        });

        $countries = ['ge', 'ua'];

        foreach ($countries as $country) {
            $model = new Country();
            $model->timestamps = false;
            $model->code = $country;
            $model->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries');
    }
}
