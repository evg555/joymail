<?php

use App\Models\Order;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_offers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('order_id');
            $table->string('name')->nullable();
            $table->float('price')->nullable();
            $table->integer('amount')->nullable();
        });

        $count = Order::all()->count();
        $offset = 0;
        $limit = 100;

        do {
            $orders = Order::select('id', 'offers')->limit($limit)->offset($offset)->get();

            foreach ($orders as $order) {
                if (!empty($order->offers)) {
                    $offers = unserialize($order->offers);

                    foreach ($offers as $name => $amount) {
                        $order->offers()->create([
                            'name' => $name,
                            'amount' => $amount
                        ]);
                    }
                }
            }

            $offset += $limit;
        } while ($offset + $limit <= $count);

        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('offers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_offers');
    }
}
