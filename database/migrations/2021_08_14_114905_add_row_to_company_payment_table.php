<?php

use App\Models\Company;
use Illuminate\Database\Migrations\Migration;

class AddRowToCompanyPaymentTable extends Migration
{
    const COMPANY_ID = 1;
    const PAYMENT_ID = 2;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $company = Company::find(self::COMPANY_ID);
        $company->systems()->attach(self::PAYMENT_ID, ['active' => 1]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $company = Company::find(self::COMPANY_ID);
        $company->systems()->detach(self::PAYMENT_ID);
    }
}
