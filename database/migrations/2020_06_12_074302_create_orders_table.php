<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('message');
            $table->string('photo')->nullable();
            $table->bigInteger('city_id')->unsigned();
            $table->string('address');
            $table->string('name_sender');
            $table->string('tel_sender');
            $table->string('name_reciever');
            $table->string('tel_reciever');
            $table->string('delivery_details')->nullable();
            $table->integer('price')->default(0);
            $table->timestamps();

            $table->foreign('city_id')
                ->references('id')->on('cities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
