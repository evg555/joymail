<?php

use App\Models\Order;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToOrdersTable extends Migration
{
    const OFFERS_FIELDS = [
        'rose',
        'chocolate',
        'soft_toy',
        'balloon',
        'kinder'
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function() {
            Schema::table('orders', function (Blueprint $table) {
                $table->text('offers')->nullable();
            });

            $this->replaceOldFields();
            $this->dropOldFields();
        });
    }

    private function replaceOldFields()
    {
        $orders = Order::all();

        foreach ($orders as $order) {
            $fields = [];

            foreach (static::OFFERS_FIELDS as $field) {
                $fields[ucfirst($field)] = $order->$field;
            }

            $order->offers = serialize($fields);
            $order->save();
        }
    }

    private function replaceNewFields()
    {
        $orders = Order::all();

        foreach ($orders as $order) {
            $fields = unserialize($order->offers);

            foreach ($fields as $field => $value) {
                $field = lcfirst($field);

                if (in_array($field, static::OFFERS_FIELDS)) {
                    $order->$field = $value;
                }
            }

            $order->save();
        }
    }

    private function dropOldFields()
    {
        Schema::table('orders', function (Blueprint $table) {
            foreach (static::OFFERS_FIELDS as $field) {
                $table->dropColumn($field);
            }
        });
    }

    private function createOldFields()
    {
        Schema::table('orders', function (Blueprint $table) {
            foreach (static::OFFERS_FIELDS as $field) {
                $table->integer($field);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::transaction(function() {
            $this->createOldFields();
            $this->replaceNewFields();

            Schema::table('orders', function(Blueprint $table) {
                $table->dropColumn('offers');
            });
        });
    }
}
