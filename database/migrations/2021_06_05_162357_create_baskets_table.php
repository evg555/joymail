<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBasketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('baskets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('token');
            $table->string('letter_type')->nullable();
            $table->text('message')->nullable();
            $table->string('photo')->nullable();
            $table->unsignedBigInteger('city_id');
            $table->string('name_sender')->nullable();
            $table->string('email_sender')->nullable();
            $table->string('tel_sender')->nullable();
            $table->string('name_reciever')->nullable();
            $table->string('tel_reciever')->nullable();
            $table->string('promocode')->nullable();
            $table->integer('price')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('baskets');
    }
}
