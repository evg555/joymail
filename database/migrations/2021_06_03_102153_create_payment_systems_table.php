<?php

use App\Models\Company;
use App\Models\PaymentSystem;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentSystemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_systems', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code');
            $table->string('name');
        });

        Schema::create('company_payment_system', function (Blueprint $table) {
            $table->primary(['company_id', 'payment_system_id',]);
            $table->boolean('active')->default(1);
            $table->unsignedBigInteger('company_id');
            $table->unsignedBigInteger('payment_system_id');

            $table->foreign('company_id')
                ->references('id')
                ->on('companies')
                ->onDelete('cascade');
            $table->foreign('payment_system_id')
                ->references('id')
                ->on('payment_systems')
                ->onDelete('cascade');
        });

        $systems = [
            1 => [
                'code' => 'ipay',
                'name' => 'Ipay'
            ],
            2 => [
                'code' => 'liqpay',
                'name' => 'LiqPay'
            ],
        ];

        foreach ($systems as $companyId => $system) {
            $company = Company::find($companyId);

            $paymentSystem = PaymentSystem::create($system);
            $paymentSystem->companies()->save($company);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_payment_system');
        Schema::dropIfExists('payment_systems');
    }
}
