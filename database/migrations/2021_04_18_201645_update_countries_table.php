<?php

use Illuminate\Database\Migrations\Migration;
use App\Models\Country;
use App\Models\Language;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('countries', function (Blueprint $table) {
            $table->string('code');
        });

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Country::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $language = new Language();
        $language->name = 'ua';
        $language->save();

        $countries = [
            [
                'language_id' => 2,
                'code' => 'ge',
                'name' =>'Georgia',
            ],
            [
                'language_id' => $language->id,
                'code' => 'ua',
                'name' =>'Ukraine',
            ]

        ];

        foreach ($countries as $item) {
            $country = new Country();
            $country->fill($item);
            $country->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Language::select('id')->where('name', '=', 'ua')->delete();
        Country::truncate();

        Schema::table('countries', function(Blueprint $table) {
            $table->dropColumn('code');
        });
    }
}
