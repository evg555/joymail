FROM php:7.4-fpm as php-joymail

RUN apt-get update

# localities
RUN apt-get install -y locales locales-all
ENV LC_ALL en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8

# install linux packages
# perhaps not all packages are needed
RUN apt-get install --no-install-recommends -y \
    libfreetype6-dev \
    libjpeg62-turbo-dev \
    libmcrypt-dev \
    libpng-dev \
    zlib1g-dev \
    libxml2-dev \
    libicu-dev \
    libaio-dev \
    libssl-dev \
    libc-client-dev \
    libsodium-dev \
    openssl \
    libkrb5-dev \
    libpq-dev \
    supervisor \
    g++ \
    apache2-utils \
    libzip-dev \
    unzip \
    libcurl4-openssl-dev \
    libldap2-dev \
    wget \
    libonig-dev \
    graphviz

# install php extensions
RUN docker-php-ext-configure imap --with-kerberos --with-imap-ssl \
    && docker-php-ext-install pcntl pdo opcache mysqli pdo_mysql imap ldap xml curl mbstring zip sodium
# install xdebug
# RUN pecl uninstall xdebug
RUN pecl install xdebug-3.1.1
RUN docker-php-ext-enable xdebug

# install the xhprof extension to profile requests
RUN curl "http://pecl.php.net/get/xhprof-2.3.9.tgz" -fsL -o ./xhprof-2.3.9.tgz && \
    mkdir /var/xhprof && tar xf ./xhprof-2.3.9.tgz -C /var/xhprof && \
    cd /var/xhprof/xhprof-2.3.9/extension && \
    phpize && \
    ./configure && \
    make && \
    make install

# custom settings for xhprof
COPY config/php_xhprof.ini /usr/local/etc/php/conf.d/xhprof.ini

RUN docker-php-ext-enable xhprof

#folder for xhprof profiles (same as in file php_xhprof.ini)
RUN mkdir -m 777 /profiles


ARG NODE_VERSION
ENV NVM_DIR=/root/.nvm

# install nvm, nodejs (version depends on .env variable) with npm and yarn
RUN curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.7/install.sh | bash \
    && . $NVM_DIR/nvm.sh \
    && nvm install ${NODE_VERSION} \
    && nvm alias default ${NODE_VERSION} \
    && nvm use default

ENV PATH="/root/.nvm/versions/node/v${NODE_VERSION}/bin/:${PATH}"
RUN npm install --global yarn

# install composer
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
    && php composer-setup.php --install-dir=/usr/bin --filename=composer \
    && php -r "unlink('composer-setup.php');"

WORKDIR /src
