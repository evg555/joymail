<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['locale']], function () {
    Route::get('/', 'MainController@index');
    Route::get('/checkout', 'MainController@checkout')->name('checkout');
    Route::get('/agreement', 'AgreementController@index')->name('agreement');
    Route::get('/contacts', 'ContactsController@index')->name('contacts');
    Route::get('/partners', 'PartnersController@index')->name('partners');
    Route::get('/thanks', 'ThanksController@index')->name('thanks');
});

Route::get('/savePayment/{orderId}', 'SavePaymentController@index');

Route::post('/checkout', 'OrderController@index');
Route::post('/ajax/{action}/', 'AjaxController@index');

Auth::routes();
