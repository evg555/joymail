<?php

return [
    'slogan1' => 'Радувати людей',
    'slogan2' => 'це наша робота!',
    'country' => 'Оберіть країну доставки',
    'menu' => [
        'agreement' => 'Угода - Оферта',
        'contacts' => 'Контакти',
        'partners' => 'Партнерам'
    ]
];
