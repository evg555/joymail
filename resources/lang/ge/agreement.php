<?php

return [
    'txt' => '<div class="agreement-main">

                <p class="agreement-descr"><br><br>
					წინამდებარე სამომხმარებლო ხელშეკრულება, შემდგომ „შეთანხმება“, ფორმდება ინტერნეტ მაღაზია SP "Fedorenko N.O.", ინტერნეტ მისამართით <a href="https://joymail.biz/">joymail.biz</a>, შემდგომ „ინტერნეტ მაღაზია“ ან „საიტი“, და ინტერნეტ მაღაზიის მომხმარებლის შორის, შემდგომ „მყიდველი“, და განსაზღვრავს საიტზე საქონლის ყიდვის პირობებს. ინტერნეტ მაღაზიის საიტი <a href="https://joymail.biz/">joymail.biz</a>, არის JoyMail კომაპნიის საკუთრება
				</p>
				<ol class="agreement-lists">
					<li class="agreement-list">
						<p>ძირითადი დებულებები</p>
						<ol class="agreement-list--2">
							<li class="agreement-list__item">წინამდებარე ხელშეკრულება ფორმდება შემდეგ მხარეებს შორის მყიდველი და ინტერნეტ მაღაზია შეკვეთის გაფორმების მომენტში.მყიდველი შეკვეთის გაფორმებისას “ მე ვეთანხმები მაღაზიის წესებს“ გრაფის მონიშვნით ადასტურებს წინამდებარე ხელშეკრულებით დადგენილ პირობებზე თანხმობას .</li>
							<li class="agreement-list__item">მყიდველი შეიძლება იყოს ნებისმიერი ფიზიკური ან იურიდიული პირი საქართველოს ტერიტორიაზე რომელსაც შეუძლია მიიღოს და გადაიხადოს მის მიერ შეკვეთილი საქონლის საფასური წინამდებარე ხელშეკრულებაში დადგენილი პირობებით.</li>
							<li class="agreement-list__item">ინტერნეტ მაღაზია იტოვებს უფლებას შეიტანოს ცვლილებები წინამდებარე ხელშეკრულებაში.</li>
							<li class="agreement-list__item">წინამდებარე ხელშეკრულება უნდა განიხილებოდეს იმ სახით როგორც არის გამოქვეყნებული საიტზე ,ასევე უნდა გამოიყენებოდეს საქართველოს კანონმდებლობის თანახმად .</li>
						</ol>
					</li>
					<li class="agreement-list">
						<p>ინფორმაცია საქონლის შესახებ</p>
						<ol class="agreement-list--2">
							<li class="agreement-list__item">საქონელი საიტზე წარმოდგენილია ფოტო -მასალებით რომლებიც საიტის საკუთრებას წარმოადგენს .</li>
							<li class="agreement-list__item">ყოველი ფოტო წარმოდგენილია თავისი ტექსტური ინფორმაციით,ფასით და საქონლის აღწერით.</li>
							<li class="agreement-list__item">ინტერნეტ მაღაზია იტოვებს უფლებას გააფართოვოს ან შეამციროს შეთავაზებული საქონლის რაოდენობა საიტზე.</li>
							<li class="agreement-list__item">ინტერნეტ მაღაზიას ცალმხრივად შეუძლია საიტზე მითითებული ფასის ცვლილება. </li>
						</ol>
					</li>
					<li class="agreement-list">
						<p>საქონლის მიწოდება</p>
						<ol class="agreement-list--2">
							<li class="agreement-list__item">ინტერნეტ მაღაზიაში შეკვეთილი საქონლის მიწოდება საქართველოს ტერიტორიაზე განხორციელდება საკუთარი მიწოდების სერვისით საიტზე მითითებულ რეგიონებსა და ქალაქებში წინასწარ შეთანხმებული ასორტიმენტის და რაოდენობის მიხედვით.</li>
							<li class="agreement-list__item">შეკვეთილი საქონლის გაგზავნა და მიწოდება ხდება 24 საათის განმავლობაში.</li>
							<li class="agreement-list__item">კურიერი საქონელს აბარებს უშუალოდ შემკვეთს .</li>
							<li class="agreement-list__item">წარუმატებელი მიწოდების შემთხვევაში ხდება განმეორებითი მიწოდება</li>
							<li class="agreement-list__item">საქონლის განმეორებით მიწოდების შემთხვევაში თანხა უკან არ ბრუნდება</li>
							<li class="agreement-list__item">მიწოდების სერვისი მუსაობს 10:00-22:00 მდე</li>
							<li class="agreement-list__item">კურიერი წინასწარ ტელეფონით ან სმს-ით აწვდის დამკვეთს ინფორმაციას მიწოდებაზე .</li>
						</ol>
					</li>
					<li class="agreement-list">
						<p>შკვეთის საფასურის გადახდა</p>
						<ol class="agreement-list--2">
							<li class="agreement-list__item">გადახდის საშუალებები - Visa, Mastercard, LiqPay invoice by e-mail, Google Pay, Apple Pay.</li>
							<li class="agreement-list__item">გადახდისას მყიდველი უნაღდო ანგარიშსწორების სისტემების გამოყენებით რიცხავს თანხას კომპანია UA593052990000026000050274339 of SP "Fedorenko N.O." ანგარიშზე</li>
							<li class="agreement-list__item">ანგარიშსწორება ხდება მხოლოდ ეროვნულ ვალუტაში. საქონელი გადახდილია ასევე UAH– ში, უკრაინის ვალუტაში.</li>
						</ol>
					</li>
					<li class="agreement-list">
						<p>სხვა</p>
						<ol class="agreement-list--2">
							<li class="agreement-list__item">ინტერნეტ მაღაზიას აქვს უფლება ჩაუშვას სხვა და სხვა აქციები</li>
							<li class="agreement-list__item">პერსონალურ მონაცემთა შესახებ კანონის შესაბამისად შეკვეთის გაფორმებისას თქვენ აძლევთ ინტერნეტ მაღაზიას თანხმობას პერსონალური მონაცემების დამუშავებაზე რათა ინფორმაციის დაგზავნა და ბაზარზე პოპულარიზაცია მოახდინოს საქონლის . <br> <br> ამ შეთანხმების გაუქმება შესაძლებელია წერილობით მიმართვის გზით .ამ შემთხვევაში თქვენი პერსონალური მონაცემები წაიშლება და შეწყდება მათი დამუშავება წერილის მიღებიდან 7 დღის განმავლობაშ
							</li>
							<li class="agreement-list__item">გამყიდველი იღებს პასუხისმგებლობას დაიცვას მყიდველის კონფიდენციალურობა ასევე სხვა პირადი ინფორმაცია რომელიც გახდა მისთვის ცნობილი წინამდებარე ხელშეკრულების შესრულებისას გარდა შემთხვევებისა როცა
								<ul class="agreement-list__ul">
									<li class="agreement-list__ul-item">ინფორმაცია ყველასთვის ხელმისაწვდომია</li>
									<li class="agreement-list__ul-item">ღია მომხმარებლის მოთხოვნით ან მასთან თანხმობით</li>
									<li class="agreement-list__ul-item">საჭიროებს გამჟღავნებას კანონის საფუძველზე სასამართლოს ან უფლებამოსილი სახელმწიფო ორგანოების მხრიდან შესაბამისი მოთხოვნის საფუძველზე ;</li>
									<li class="agreement-list__ul-item">საჭიროებს გამჟღავნებას წინამდებარე ხელშეკრულებაში ჩადებული პირობების თანახმად . <br> <br> გამყიდველი იტოვებს უფლებას ცალმხრივად შეცვალოს პირობები საიტზე <a href="https://joymail.biz/">joymail.biz</a>, ცვლილებების ასახვის გზით</li>
								</ul>
							</li>
							<li class="agreement-list__item">შეთანხმება არის ხელშეკრულება მყიდველსა და გამყიდველს შორის რომელიც შეიცავს ინტერნეტ მაღაზია Joymail-ში საქონლის შეძენის წესებს .</li>
							<li class="agreement-list__item">ეს ხელშეკრულება ითვლება გაფორმებულად მას შემდეგ რაც გამყიდველი მყიდველს აწვდის გადახდის დამადასტურებელ საბუთს ან მომენტიდან როცა გამყიდველი იღებს შეტყობინებას საქონლის შეძენის თაობაზე.</li>
						</ol>
					</li>
				</ol>
            </div>'

];
