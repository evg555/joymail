<?php

return [
    'txt' => '<div class="agreement-main">
<br><br>In order to expand its presence in the market, JoyMail is looking for partners to open branches in cities around the world.<br><br>

Briefly what we offer: <br>

1. Site version for your region. <br>
2. Corporate identity of the company (design layouts of business cards, stickers, etc.). <br>
2. Launching a turnkey business within a week. <br>
3. Fast payback, minimal investment. <br>
4. Training and operational support for the work of the representative office. <br>
5. Timely technical support of the online store. <br>
6. The freedom to choose the types of goods for placement in the online store in your region. <br>
7. Transparent tariffs and flexible terms of cooperation. <br> <br>
 
To cooperate with the courier delivery service "JoyMail" you need: <br>

1. Staff 2 people: administrator and courier (to start). <br>
2. You don\'t need an office to work, you can work from home. <br>
3. Office equipment (computer, printer). <br>
4. Transport for the courier. <br>
5. Permit for entrepreneurial activity. <br>
6. Small start-up capital for the purchase of consumables. <br> <br>

We do not have a Franchise or any deposits. <br>
For more detailed information on costs and to launch a branch, write to email: support@joymail.biz<br><br></div>
'

];
