<?php

return [
    'title' => 'Thank you for request!',
    'message' => 'We will contact you <br> to clarify order details',
    'back' => 'To home page'
];
