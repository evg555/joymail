﻿<?php

return [
    'step' => [
        'title' => 'Step',
        'from' => 'from',
        'prev' => 'Previous step',
        'next' => 'Next step',
        'pay' => 'Proceed to checkout'
    ],
    'country' => [
        'title' => 'Select country of delivery'
    ],
    'city' => [
        'title' => 'Select city of delivery'
    ],
    'card' => [
        'title' => 'Select type of JoyKa'
    ],
    'postcard' => [
        'title' => 'Select postcard',
        'popup' => [
            'title' => 'Select postcard',
            'apply' => 'Apply'
        ]
    ],
    'textarea' => '&#128070; Write your wish here &#128070; (Maximum 300 symbols)',
    'photo' => [
        'click' => 'Upload photo or postcard'
    ],
    'price' => 'Price',
    'offers' => [
        'title' => 'Select additional goods'
    ],
    'detail' => [
        'title' => 'Information about order',
        'address' => 'Delivery address',
        'message' => 'Your message',
        'photo' => 'Your photo',
        'offers' => [
            'title' => 'Additional goods',
            'name' => 'Name',
            'amount' => 'Quantity',
            'total' => 'Total'
        ]
    ],
    'promocode' => [
        'title' => 'Promocode',
        'check' => 'Check',
        'discount' => 'Discount',
        'total' => 'Total price',
    ],
    'data' => [
        'title' => 'Sending',
        'sender' => 'Sender data',
        'reciever' => 'Recipient data',
        'phone' => 'Phone',
        'name' => 'Name',
        'email' => 'Email'
    ],
    'payment' => [
        'title' => 'Your order is accepted',
        'order' => 'Order ID',
        'system' => 'Select a payment method',
        'pay' => 'Pay'
    ],
];
