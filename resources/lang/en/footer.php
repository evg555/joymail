<?php



return [

    'slogan1' => 'To make people happy',

    'slogan2' => 'This is our job!',

    'country' => 'Select a country for delivery',

    'menu' => [

        'agreement' => 'Agreement',

        'contacts' => 'Contacts',

        'partners' => 'Partners'

    ]

];

