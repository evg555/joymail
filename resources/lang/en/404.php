<?php

return [
    'title' => 'Page not found',
    'message' => 'Come back to home page <br>while we clean up the mess',
    'back' => 'To home page'
];
