<?php

return [
    'txt' => '<div class="agreement-main">

                <p class="agreement-descr"><br><br>
                This user agreement, hereinafter referred to as the "Agreement", is concluded between the SP "Fedorenko N.O." as online store having an address on the Internet
                    <a href="https://joymail.biz/">joymail.biz</a> hereinafter referred to as the "Online Store" or "Site", and the user of the online store services, hereinafter referred to as the "Buyer", and determines the conditions for the purchase of goods through the Site. The website of the online store
                    <a href="https://joymail.biz/">joymail.biz</a> is the property of JoyMail LLC.
                </p>
                <ol class="agreement-lists">
                    <li class="agreement-list">
                        <p>The main Provisions</p>
                        <ol class="agreement-list--2">
                            <li class="agreement-list__item">This Agreement is concluded between the Buyer and the online stores at the time of placing the order. The buyer confirms his agreement with the conditions established by this Agreement by ticking the box "I agree with the rules of the store" when placing an order.</li>
                            <li class="agreement-list__item">The buyer can be any natural or legal person capable of accepting and paying for the goods ordered by him in the manner and on the conditions established by this Agreement, in Ukraine</li>
                            <li class="agreement-list__item">The online store reserves the right to amend this Agreement.</li>
                            <li class="agreement-list__item">This Agreement shall be considered as published on the Site and shall be applied and construed in accordance with the legislation of Ukraine</li>
                        </ol>
                    </li>
                    <li class="agreement-list">
                        <p>Product Information</p>
                        <ol class="agreement-list--2">
                            <li class="agreement-list__item">The goods are presented on the Site through photo samples owned by the online store.</li>
                            <li class="agreement-list__item">Each sample photo is accompanied by text information :, price and description of the goods</li>
                            <li class="agreement-list__item">The online store reserves the right to expand and reduce the product offer on the Site, to regulate access to the purchase of any goods, as well as to suspend or terminate the sale of any goods at its sole discretion.</li>
                            <li class="agreement-list__item">The price of goods indicated on the Website may be changed unilaterally by the online store.</li>
                        </ol>
                    </li>
                    <li class="agreement-list">
                        <p>Delivery of goods</p>
                        <ol class="agreement-list--2">
                            <li class="agreement-list__item">Delivery of goods ordered in the online store, in the agreed quantity and assortment, is carried out by our own delivery service to the cities of Ukraine indicated on the site.</li>
                            <li class="agreement-list__item">Sending and delivery of the ordered goods is made within 24 hours.</li>
                            <li class="agreement-list__item">Delivery of goods is made by courier personally in hand.</li>
                            <li class="agreement-list__item">Delivery to one addressee is made twice in the case of the first unsuccessful attempt.</li>
                            <li class="agreement-list__item">If the goods are not delivered twice, the money will not be returned</li>
                            <li class="agreement-list__item">Delivery working hours from 10.00 to 22.00</li>
                            <li class="agreement-list__item">The courier pre-notifies the customer of the delivery by SMS or call</li>
                        </ol>
                    </li>
                    <li class="agreement-list">
                        <p>Payment for goods</p>
                        <ol class="agreement-list--2">
                            <li class="agreement-list__item">
                                Ways of payment: Visa, Mastercard, LiqPay invoice by e-mail, Google Pay, Apple Pay. You can choose these types in the "Payment" section.
                            </li>
                            <li class="agreement-list__item">
                                When calculating, the Buyer pays for the goods, using specified types of payment, to the bank account  number UA593052990000026000050274339 of SP "Fedorenko N.O.".
                            </li>
                            <li class="agreement-list__item">
                                Goods are paid only in the UAH, currency of Ukraine.
                            </li>
                        </ol>
                    </li>
                      <li class="agreement-list">
                        <p>Purchase returns</p>
                        <ol class="agreement-list--2">
                            <li class="agreement-list__item">In case of non-delivery due to the fault of the company, the entire amount will be returned in full within 14-30 calendar days.</li>
                            <li class="agreement-list__item">If the Order cannot be delivered twice due to the fault of the Recipient, the money will not be returned.</li>
                            <li class="agreement-list__item">The company is not responsible for incorrectly filled in data fields of the sender or recipient, and the money for the paid Order will not be returned.</li>
                     </ol>
                    </li>

                    <li class="agreement-list">
                        <p>Other</p>
                        <ol class="agreement-list--2">
                            <li class="agreement-list__item">The online store has the right to conduct special promotions.</li>
                            <li class="agreement-list__item">In accordance with the Law "On Personal Data", by placing an order, you consent to the online store to process your personal data in order to send information and promote products on the market, without any time limit. This consent may be revoked by you by sending a written notice. In this case, your personal data will be destroyed and their processing terminated within 7 business days from the date we receive the notification</li>
                            <li class="agreement-list__item">The Seller undertakes to maintain confidentiality regarding the Buyer\'s personal data, as well as other information about the Buyer, which became known to the Seller in connection with the execution of this Agreement, unless such information:
                                <ul class="agreement-list__ul">
                                    <li class="agreement-list__ul-item">is publicly available;</li>
                                    <li class="agreement-list__ul-item">disclosed at the request or with the permission of the Buyer;</li>
                                    <li class="agreement-list__ul-item">requires disclosure on the grounds provided by law, or upon receipt of relevant requests from the court or authorized state bodies;</li>
                                    <li class="agreement-list__ul-item">disclosed for other reasons provided for by the agreement. The seller has the right to unilaterally amend the Agreement by posting the changes on the Website at <a href="https://joymail.biz/">joymail.biz</a>, unless otherwise provided by the new version of the Agreement.</li>
                                </ul>
                            </li>
                            <li class="agreement-list__item">The agreement is a legally binding contract between the Buyer and the Seller contains the rules for making purchases in the joymail.biz online store.</li>
                            <li class="agreement-list__item">This contract is considered concluded from the moment the seller gives the buyer a cash or sales receipt, or another document confirming the payment of the goods, or from the moment the seller receives a message of intent to purchase the goods.</li>
                        </ol>
                    </li>
                </ol>
            </div>'

];
