@extends('layouts.app')

@section('content')
    <div class="thanks">
        <div class="thanks_inner">
            <div class="thanks_body">
                <div class="thanks_info">
                    <div class="thanks_info-title">@lang('thanks.title')</div>
                    <div class="thanks_info-text">@lang('thanks.message')</div>
                    <a href="/" class="thanks_info-btn btn">@lang('thanks.back')</a>
                </div>
                <div class="thanks_img">
                    <img src="img/thanks_img.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
@endsection
