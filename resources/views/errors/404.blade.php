@extends('layouts.app')

@section('content')
    @php \App\Helpers\Site::setCountryAndLocale(); @endphp

    <div class="thanks thanks_error">
        <div class="thanks_inner">
            <div class="thanks_body">
                <div class="thanks_info">
                    <div class="thanks_info-title">@lang('404.title')</div>
                    <div class="thanks_info-text">@lang('404.message')</div>
                    <a href="/" class="thanks_info-btn btn">@lang('404.back')</a>
                </div>
            </div>
        </div>
    </div>
@endsection
