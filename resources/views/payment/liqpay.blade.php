<form method="POST" action="{{$url}}" accept-charset="utf-8">
    <input type="hidden" name="data" value="{{$data}}">
    <input type="hidden" name="signature" value="{{$signature}}">

    <button class="quiz_step-pay">@lang('payment.href.title')</button>
</form>
