<?
Meta::loadPackage(['jquery']);
?>

<div class="form-group form-element-text ">
    <button class="btn btn-primary js-create-link">Create payment link</button>
</div>

<div class="form-group form-element-text ">
    <a target="_blank" class="payment-link" href=""></a>
</div>

<script>
    $(function () {
        $('.js-create-link').on('click', function(e) {
            e.preventDefault();
            $(this).off();

            let targetForm = $('form.card');
            let DATA = {};

            DATA.orderId = $('input[name=id]').val();
            DATA.systemId = 1;

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: "/ajax/getPaymentButton",
                type: "POST",
                dataType: "json",
                data: DATA,
                success: function(answ) {
                    if (answ.status == 'success') {
                        $('.payment-link').html(answ.view);
                    } else {
                        Swal.fire({
                            title: 'Payment link is not created',
                            text: answ.msg,
                            icon: 'error',
                            confirmButtonText: 'OK'
                        });
                    }
                },
                error: function(xhr) {
                    if (xhr.responseJSON.errors) {
                        let errors = xhr.responseJSON.errors;
                        let errorMsg = '';

                        for (let key in errors) {
                            errorMsg += errors[key].join("\n") + "\n";
                        }

                        Swal.fire({
                            title: 'Payment link is not created',
                            text: errorMsg,
                            icon: 'error',
                            confirmButtonText: 'OK'
                        });
                    }
                }
            });

            return false;
        });
    });
</script>
