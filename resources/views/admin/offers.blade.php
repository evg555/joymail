@php
  $model->load('offers');
@endphp

@foreach ($model->offers as $offer)
    <div class="form-group form-element-text ">
        <label for="offers" class="control-label">{{$offer->name}}</label>
        <input v-pre  class="form-control" type="text" id="offers" name="offers" value="{{$offer->amount}}" readonly>
    </div>
@endforeach
