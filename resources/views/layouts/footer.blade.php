        @php
            //TODO: Вынести в БД
            $socials = [
                'instagram' => 'https://www.instagram.com/joymail.biz/',
                'facebook' => 'https://www.facebook.com/groups/2389460657958736',
                'twitter' => 'https://t.me/joymail_llc',
                'youtube' => 'https://www.youtube.com/channel/UCvzXZmwFkhq6JT3c0acS4ZA?view_as=subscriber'
            ];
        @endphp

        <footer class="footer">
            <div class="container">
                <div class="footer_wrapper">
                    <div class="footer_logo">
                        <div class="footer_logo-img">
                            <img src="img/logo.svg" alt="">
                        </div>
                        <div class="footer_logo-title">
                            @lang('footer.slogan1') — @lang('footer.slogan2')
                        </div>
                    </div>
                    <ul class="footer_menu">
                        <li><a href="/agreement" class="footer_menu-link">@lang('footer.menu.agreement')</a></li>
                        <li><a href="/contacts" class="footer_menu-link">@lang('footer.menu.contacts')</a></li>
                        <li><a href="/partners" class="footer_menu-link">@lang('footer.menu.partners')</a></li>
                    </ul>
                    <div class="footer_pay">
                        <img src="{{asset('/img/footer_pay.svg')}}" alt="Visa, Mastercard, Google Pay, Apple Pay by Liqpay" title="Visa, Mastercard, Google Pay, Apple Pay">
                        
                    </div>
                </div>
                <div class="footer_inner">
                    <div class="footer_messanges">
                        {{$i = 1}}
                        @foreach ($socials as $name => $url)
                            <a href="{{$url}}" target="_blank" class="footer_messanger">
                                <img src="{{asset('/img/footer_messange' . $i . '.svg')}}" alt="{{$name}}">
                            </a>

                            {{$i++}}
                        @endforeach
                    </div>
                     <a href="https://www.instagram.com/_aleksej2142/?hl=ru" target="_blank" class="footer_des">
                    Design by A.Kiselev
                </a>
                </div>
            </div>
        </footer>


        <div id="video_popup" class="video_popup" style="display: none;">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/U-7NvhwWjz0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
         <div id="video_popup2" class="video_popup" style="display: none;">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/y8sLJrWu_OQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
        <div id="video_popup3" class="video_popup" style="display: none;">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/marhE6GytSI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>

    </body>
</html>
