<!DOCTYPE html>
<html lang="{{request()->cookie('lang')}}">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>@lang('header.title')</title>

    <meta name="description" content="Доставка цветов и подарков с открыткой фото сюрприз в конверте в Батуми и Тбилиси. Грузия и Украина.">
    <meta name="keywords" content="доставка, открытка, подарки, цветы,  курьер, письмо, индивидуальные заказы, Батуми, тбилиси, украина, Грузия">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="shortcut icon" href="https://joymail.biz/favicon.ico" type="image/x-icon">

    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />
    <link rel="stylesheet" href="css/jquery.fancybox.min.css">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Neucha&family=Ubuntu:wght@300;400;500;700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">

    <script src="{{mix('js/app.js')}}"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

    <!-- <script src="//code.jivosite.com/widget/gGDm3s4JCE" async></script> -->
</head>
<body>


    @if(env('APP_ENV') == 'production')
        @include('layouts.partials.analytics')
    @endif

    <header class="header">
        @include('main.partials.nav-mobile')

        <div class="container">
            <div class="header_wrapper">
                @include('main.partials.logo')
                @include('main.partials.top-menu')
                @include('main.partials.langs')
            </div>
        </div>
    </header>
