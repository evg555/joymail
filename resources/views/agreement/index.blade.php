@extends('layouts.app')

@section('content')
    <div class="thanks">
        <div class="thanks_inner">
            <div class="thanks_body"> 
            @lang('agreement.txt')
              </div>
        </div>
    </div>          
@endsection
