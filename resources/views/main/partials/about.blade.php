<section id="we_block" class="we_block">
    <div class="container">
        <div class="we_block-title">@lang('main.about.title')</div>
        <div class="we_block-wrapper">
            @for ($i = 1; $i < 9; $i++)
                <div class="we_block-item">
                    <div class="we_block-info">
                        <div class="we_block-item_img">
                            <img src="{{asset('/img/we_block-item' . $i . '.png')}}" alt="">
                        </div>
                        <div class="we_block-item_title">@lang('main.about.choose_title_' . $i)</div>
                        <div class="we_block-item_text">@lang('main.about.choose_text_' . $i)</div>
                    </div>
                </div>
            @endfor
        </div>
    </div>
</section>
