<div class="content-row fifth-row">
    <div class="row__title row__title--fifth">@lang('send.form_title')</div>

    <div class="fifth-row-cols">
        <div class="col col--first">
            <div class="input-wrap">
                <label for="sender-email">@lang('send.input_9')</label>
                <input id="sender-email" type="email" name="email_sender" value="{{old('email_sender')}}" required>
            </div>
        </div>

        <div class="col col--second">
            <div class="input-wrap">
                <label for="senders-phone">@lang('send.input_5')</label>
                <input id="senders-phone" type="text" name="tel_sender" value="{{old('tel_sender')}}" required>
            </div>

            <div class="input-wrap">
                <label for="receiver-phone">@lang('send.input_7')</label>
                <input id="receiver-phone" type="text" name="tel_reciever" value="{{old('tel_reciever')}}" required>
            </div>
        </div>

        <div class="col col--third">
            <div class="input-wrap">
                <label for="senders-name">@lang('send.input_4')</label>
                <input id="senders-name" type="text" name="name_sender" value="{{old('name_sender')}}" required>
            </div>

            <div class="input-wrap">
                <label for="receiver-name">@lang('send.input_6')</label>
                <input id="receiver-name" type="text" name="name_reciever" value="{{old('name_reciever')}}" required>
            </div>

            <div class="input-wrap">
                <label for="promocode-phone">@lang('send.input_3')</label>

                <div class="wrap">
                    <input id="promocode-phone" type="text" name="promocode" value="{{old('promocode')}}">
                    <span class="promocode__icon"></span>
                    <a class="btn btn__promo js-promocode-apply">@lang('send.check')</a>
                    <p class="warning promocode-alert">@lang('send.promocode_alert')</p>
                    <p class="success promocode-success">@lang('send.promocode_success')</p>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" name="promocode_price" value="">
    <input type="hidden" name="promocode_type" value="">
    <input type="hidden" name="action" value="data">
</div>
