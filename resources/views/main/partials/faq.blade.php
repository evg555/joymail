<section id="faq" class="faq">
    <div class="faq_img">
        <img src="{{asset('/img/faq_img.jpg')}}" alt="">
    </div>
    <div class="container">
        <div class="faq_title">FAQ</div>

        <div class="faq_wrapper">
            @for ($i = 1; $i < 7; $i++)
                <div class="faq_item">
                    <div class="faq_item-title">
                        @lang('main.faq.faq_title_' . $i)
                        <span></span>
                    </div>
                    <div class="faq_content">@lang('main.faq.faq_text_' . $i)</div>
                </div>
            @endfor
        </div>
    </div>
</section>
