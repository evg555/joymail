<section id="motives" class="motives">
    <div class="motives_title">@lang('main.reasons.title')</div>
    <div class="motives_slider">
        @for ($i = 1; $i < 9; $i++)
            <div class="motives_item">
                <div class="motives_item-info">
                    <div class="motives_item-img">
                        <img src="{{asset('/img/motives_item' . $i . '.jpg')}}" alt="">
                    </div>
                    <div class="motives_item-title">@lang('main.reasons.reason_text_' . $i)</div>
                    <div class="motives_item-text">@lang('main.reasons.reason_title_' . $i)</div>
                </div>
            </div>
        @endfor
    </div>
</section>
