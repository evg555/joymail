<div class="header_lang">
    <select class="card_right-select" style="display: none">
        @foreach ($langs as $lang)
            <option data-img="{{asset('img/flag_' . $lang->name . '.svg')}}" value="{{strtoupper($lang->name)}}"
            @if (App::getLocale() == $lang->name)
                selected
            @endif
            >
                {{strtoupper($lang->name)}}
            </option>

        @endforeach
    </select>
</div>


