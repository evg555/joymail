<ul class="menu">
    <li><a href="/#how" class="menu_link">@lang('main.work.title')</a></li>
    <li><a href="/#we_block" class="menu_link">@lang('main.about.title')</a></li>
    <li><a href="/#motives" class="menu_link">@lang('main.reasons.title')</a></li>
    <li><a href="/#faq" class="menu_link">FAQ</a></li>
    <li><a href="/#reviews" class="menu_link">@lang('main.reviews.title')</a></li>
{{--    <li><a href="/#individual-order" class="menu_link">Индивидуальный заказ</a></li>--}}
</ul>
