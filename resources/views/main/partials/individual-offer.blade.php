<section id="individual-order" class="form">
    <div class="form_woman">
        <img src="img/form_woman.png" alt="">
    </div>

    <div class="container">
        <div class="form_wrapper">
            <form action="/" method="post" class="form_inner">
                <div class="form_inner-info">
                    <div class="form_title">@lang('main.individual.form')</div>
                    <div class="form_inp-wrapper">
                        <span>@lang('main.individual.name')</span>
                        <input type="text" name="name" class="form_inp" required>
                    </div>

                    <div class="form_connection">
                        <div class="form_connection-title">@lang('main.individual.connection')</div>
                        <div class="form_cheks">
                            <label class="form_chek-label">
                                <input type="radio" name="type" class="form_chek-inp" value="viber" checked>
                                <span class="form_chek">Viber</span>
                            </label>
                            <label class="form_chek-label">
                                <input type="radio" name="type" class="form_chek-inp" value="whatsapp">
                                <span class="form_chek">WhatsApp</span>
                            </label>
                            <label class="form_chek-label">
                                <input type="radio" name="type" class="form_chek-inp" value="telegram">
                                <span class="form_chek">Telegram</span>
                            </label>
                        </div>
                    </div>

                    <div class="form_inp-wrapper">
                        <span>@lang('main.individual.phone')</span>
                        <input type="text" name="phone" class="form_inp" required>
                    </div>

                    <div class="individual-error alert-danger d-none"></div>

                    <button class="form_btn btn">@lang('main.individual.send')<span></span></button>

                    <div class="form_ok">
                        <span><img src="img/form_ok.svg" alt=""></span>
                        <div class="form_ok-text">@lang('main.individual.agreement')
                            <a href="/agreement">@lang('main.individual.href')</a>
                        </div>
                    </div>
                </div>
            </form>

            <div class="form_text">
                <div class="form_text-inner">
                    <div class="form_text-img">
                        <img src="img/form_text-aft.svg" alt="">
                    </div>
                    @lang('main.individual.additional')
                </div>
            </div>
        </div>
    </div>
</section>
