@extends('main.partials.quiz.step')

@section('step')

@php $countryId = $basket->country_id ?? request()->cookie('country_id') @endphp

<div class="quiz_progress">
    <div class="quiz_progress-title">
        <span>@lang('send.step.title')</span>  1 @lang('send.step.from') 5
    </div>
    <div class="quiz_progress-bar">
        <span style="width: 20%;"></span>
    </div>
</div>

<div class="quiz_step-title">@lang('send.country.title')</div>

<div class="quiz_step-cheks">
    @foreach ($countries as $country)
        <label class="quiz_step-item">
            <div class="quiz_step-img">
                <img src="{{$country->image}}" alt="">
            </div>

            <input type="radio"
                    name="country_id"
                    class="quiz_step-inp"
                    value="{{$country->country_id}}"
                    @if ($countryId == $country->country_id)
                        checked
                    @endif
            >

            <span class="quiz_step-span"></span>

            <div class="quiz_step-cheks_title">{{$country->name}}</div>
        </label>
    @endforeach

    <input type="hidden" name="action" value="country">
    <input type="hidden" name="state" value="cities">
</div>

@endsection
