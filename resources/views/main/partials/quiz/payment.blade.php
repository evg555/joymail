@include('main.partials.quiz.header')

<div class="quiz_buy">
    <div class="quiz_buy-title">@lang('send.payment.title')</div>
    <div class="quiz_buy-info">
        <div class="quiz_buy-id">
            @lang('send.payment.order'): <span>{{$order->id}}</span>
        </div>
        <div class="quiz_buy-price">
            @lang('send.price'):  <span>{{$order->price}} {{$order->currency}}</span>
        </div>
    </div>

    <div class="quiz_buy-subtitle">@lang('send.payment.system')</div>

    <div class="quiz_step-ways">
        @foreach($systems as $system)
            <label class="quiz_step-way">
                <input type="radio"
                       name="payment-system"
                       data-system="{{$system->id}}"
                       id="payment-system-{{$system->id}}"
                       value="{{$system->code}}"
                       class="quiz_step-inp js-payment-system">
                <span class="quiz_step-span"></span>
                <div class="quiz_step-cheks_title">
                    {{$system->name}} @if ($system->code == 'liqpay'){{$priceInUah}}@endif
                </div>
            </label>
        @endforeach
    </div>

    <div class="quiz-error alert alert-danger d-none"></div>

    <input type="hidden" name="order_id" value="{{$order->id}}">

    <div class="step-buy_btn">
        <a class="quiz_step-pay">@lang('send.payment.pay')</a>
    </div>
</div>

<script src="{{asset('/js/test.js')}}"></script>

@include('main.partials.quiz.footer')
