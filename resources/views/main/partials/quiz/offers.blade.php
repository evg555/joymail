@extends('main.partials.quiz.step')

@section('step')

@php
    foreach ($basket->offers as $basketOffer) {
        $basketInOffers[$basketOffer->offer_id] = $basketOffer->amount;
    }

    $price = 0;
@endphp

<div class="quiz_progress">
    <div class="quiz_progress-title">
        <span>@lang('send.step.title')</span> 3 @lang('send.step.from') 5
    </div>
    <div class="quiz_progress-bar">
        <span style="width: 60%;"></span>
    </div>
</div>

<div class="quiz_step-title">@lang('send.offers.title')</div>

<div class="quiz_step-dop">
    @foreach ($offers as $offer)
        @php
            $amount = 0;

            if (!empty($basketInOffers) && array_key_exists($offer->id, $basketInOffers)) {
                $amount = $basketInOffers[$offer->id];
            }

        $price += $offer->price * $amount;
        @endphp

        <div class="step-dop_item calculator-item">
{{--            <div data-popup="#dop_item_{{$offer->id}}" class="quiz_type-link popup_btn">--}}
{{--                ?--}}
{{--            </div>--}}

            <div class="step-dop-img">
                <img src="{{$offer->picture}}" alt="{{$offer->code}}">
            </div>
            <div class="step-dop_title">{{$offer->langs->first()->name}}</div>
            <div class="step-dop_price">
                <span class="calculator-base-price">{{$offer->price}}</span> {{ucfirst($basket->currency)}}
            </div>
            <div class="step-dop_count">
                <span class="minus calculator-decrease">-</span>
                <span type="text" class="calculator-onchange-sum">{{$amount}}</span>
                <span class="plus calculator-increase">+</span>
            </div>

            <div class="quiz_type-supp" id="dop_item_{{$offer->id}}" style="display: none;">
                <div class="quiz_type-wrapper">
                    <div class="quiz_type-img">
                        <img src="{{$offer->picture}}" alt="">
                    </div>
                    @php //TODO: Раскоментить, когда появятся текста для товаров @endphp
{{--                    <div class="quiz_type-info">--}}
{{--                        <div class="quiz_type-tiitle">{{$offer->title}}</div>--}}
{{--                        <div class="quiz_type-text">--}}
{{--                            — советский палеонтолог, писатель-фантаст и общественный мыслитель. Доктор биологических наук (1941), с 1929 по 1959 год научный сотрудник Палеонтологического института, с 1937 года заведующий лабораторией низших позвоночных. Лауреат Сталинской премии второй степени (1952, за монографию «Тафономия и геологическая летопись»).звоночных. Лауреат Сталинской премии второй степени.--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>
            </div>

            <input type="hidden" class="offer-count" name="offers[{{$offer->id}}]" value="0">
            <input type="hidden" class="amount-limit" value="{{$offer->count}}">
            <input type="hidden" class="calculator-onchange-full  amount-full" value="0">
        </div>
    @endforeach
</div>

<div class="quiz_step-price">
    @lang('send.price'): <span class="calculator-result">{{$price}}</span> {{ucfirst($basket->currency)}}
</div>

<input type="hidden" class="calculator-input" name="price" value="{{$price}}">
<input type="hidden" name="action" value="offers">
<input type="hidden" name="state" value="cards">

@endsection
