@extends('main.partials.quiz.step')

@section('step')

@php $cityId = $basket->city_id ?? $cities->first()->id @endphp

<div class="quiz_progress">
    <div class="quiz_progress-title">
        <span>@lang('send.step.title')</span>  2 @lang('send.step.from') 5
    </div>
    <div class="quiz_progress-bar">
        <span style="width: 40%;"></span>
    </div>
</div>

<div class="quiz_step-title">@lang('send.city.title')</div>

<div class="quiz_step-cities">
    @foreach ($cities as $city)
        <label class="quiz_step-city">
            <input type="radio"
                   name="city_id"
                   class="quiz_step-inp"
                   value="{{$city->id}}"
                   @if (!$city->active) disabled @endif
                   @if ($cityId == $city->id) checked @endif>

            <span class="quiz_step-span"></span>

            <div class="quiz_step-cheks_title">{{$city->name}}</div>
        </label>
    @endforeach

    <input type="hidden" name="action" value="city">
    <input type="hidden" name="currency" value="{{$currency->code}}">
    <input type="hidden" name="state" value="offers">
</div>

@endsection
