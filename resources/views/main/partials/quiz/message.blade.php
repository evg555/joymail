@extends('main.partials.quiz.step')

@section('step')

<div class="quiz_progress">
    <div class="quiz_progress-title">
        <span>@lang('send.step.title')</span> 5 @lang('send.step.from') 5
    </div>
    <div class="quiz_progress-bar">
        <span style="width: 100%;"></span>
    </div>
</div>

<div class="quiz_step-title">{{$card->title}}</div>

<div class="quiz_step-desc">
    <textarea name="message"
              cols="30"
              rows="10"
              class="quiz_step-area"
              maxlength="300"
              placeholder="@lang('send.textarea')"
              required>
        {{$basket->message}}
    </textarea>

    <div class="quiz_step-files">
        <label class="quiz_step-file box-file">
            <input type="file" name="photo" id="photo" class="quiz_step-file_inp">
            <span class="quiz_step-file_span">@lang('send.photo.click')</span>
        </label>

        @if (!empty($postcards->toArray()))
            <div data-popup="#postcard"  class="quiz_step-file box-postcard popup_btn">
                <span>@lang('send.postcard.title')</span>
            </div>

            <div class="quiz_type-supp" id="postcard" style="display: none;">
                <div class="quiz_postcard-wrapper">
                    <div class="postcard_title">@lang('send.postcard.popup.title')</div>

                    <div class="postcard_items">
                        @foreach ($postcards as $postcard)
                            <label class="quiz_step-item">
                                @if ($postcard->description)
                                    <div data-popup="#quiz_type{{$postcard->id}}" class="quiz_type-link popup_btn">?</div>
                                @endif

                                <div class="quiz_step-img">
                                    <img src="{{asset(($postcard->image))}}" alt="{{$postcard->code}}">
                                </div>

                                <input type="radio" name="postcard_id" class="quiz_step-inp" value="{{$postcard->id}}">

                                <span class="quiz_step-span"></span>

                                <div class="quiz_step-cheks_title">{{$postcard->title}}</div>
                                <div class="quiz_step-cheks_price">{{$postcard->price}} {{$basket->currency}}</div>

                                <div class="quiz_type-supp" id="quiz_type{{$postcard->id}}" style="display: none;">
                                    <div class="quiz_type-wrapper">
                                        <div class="quiz_type-img">
                                            <img src="{{asset(($postcard->image))}}" alt="{{$postcard->code}}">
                                        </div>

                                        @if ($postcard->description)
                                            <div class="quiz_type-info">
                                                <div class="quiz_type-tiitle">{{$postcard->title}}</div>
                                                <div class="quiz_type-text">{{$postcard->description}}</div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </label>
                        @endforeach
                    </div>

                    <div class="postcard_btn js-postcard-apply">@lang('send.postcard.popup.apply')</div>
                </div>
            </div>
        @endif
    </div>
</div>

<div class="quiz_step-symbols">*@lang('send.textarea')</div>

<div class="quiz_step-price">
    @lang('send.price'): {{$basket->price}} {{ucfirst($basket->currency)}}
</div>

<input type="hidden" name="action" value="message">
<input type="hidden" name="state" value="detail">

@endsection
