@extends('main.partials.quiz.step')

@section('step')

<div class="quiz_progress">
    <div class="quiz_progress-title">
        <span>@lang('send.step.title')</span> 4 @lang('send.step.from') 5
    </div>
    <div class="quiz_progress-bar">
        <span style="width: 80%;"></span>
    </div>
</div>

<div class="quiz_step-title">@lang('send.card.title')</div>

<div class="quiz_step-cheks">
    @foreach ($cards as $card)
        @php
            if ($basket->card_id == $card->id) {
                $price = $card->price;
            }
        @endphp

        <label class="quiz_step-item">
{{--            <div data-popup="#quiz_type_{{$card->code}}" class="quiz_type-link popup_btn">--}}
{{--                ?--}}
{{--            </div>--}}

            <div class="quiz_step-img">
                <img src="{{asset($card->image)}}" alt="">
            </div>

            <input type="radio"
                   name="card_id"
                   class="quiz_step-inp"
                   data-price="{{$card->price}}"
                   value="{{$card->id}}"
                   @if ($basket->card_id == $card->id) checked @endif>

            <span class="quiz_step-span"></span>

            <div class="quiz_step-cheks_title">
                {{$card->title}}
            </div>

            <div class="quiz_step-cheks_price">
                {{$card->price}} {{ucfirst($basket->currency)}}
            </div>

            <div class="quiz_type-supp" id="quiz_type_{{$card->code}}" style="display: none;">
                <div class="quiz_type-wrapper">
                    <div class="quiz_type-img">
                        <img src="{{asset($card->image)}}" alt="">
                    </div>

                    <div class="quiz_type-info">
                        <div class="quiz_type-tiitle">
                            {{$card->title}}
                        </div>
                        <div class="quiz_type-text">
                            — советский палеонтолог, писатель-фантаст и общественный мыслитель. Доктор биологических наук (1941), с 1929 по 1959 год научный сотрудник Палеонтологического института, с 1937 года заведующий лабораторией низших позвоночных. Лауреат Сталинской премии второй степени (1952, за монографию «Тафономия и геологическая летопись»).звоночных. Лауреат Сталинской премии второй степени.
                        </div>
                    </div>
                </div>
            </div>
        </label>
    @endforeach

    <input type="hidden" name="price" value="{{$price ?? 0}}">
    <input type="hidden" name="action" value="card">
    <input type="hidden" name="state" value="message">
</div>

@endsection
