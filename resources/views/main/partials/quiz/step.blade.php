@include('main.partials.quiz.header')
    <div class="quiz_steps">
        <div class="quiz_step">
            @yield('step')

            <div class="quiz-error alert alert-danger d-none"></div>

            <div class="quiz_step-btns">
                @if ($template !== 'countries')
                    <input type="submit" name="prev" class="quiz_step-prev js-prev-step" value="@lang('send.step.prev')">
                @endif

                <input type="submit" name="next" class="quiz_step-next js-next-step" value="@lang('send.step.next')">
            </div>
        </div>

@include('main.partials.quiz.footer')
