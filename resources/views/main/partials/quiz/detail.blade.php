<div class="container">
    <div class="quiz_step-title">@lang('send.detail.title')</div>
    <div class="quiz_cart-adres">
        @if ($country && $city)
            @lang('send.detail.address'):  <span>{{$country->name}}, {{$city->name}}</span>
        @endif
    </div>
    <div class="quiz_cart-media">
        <div class="quiz_cart-postcard">
            <img src="{{$card->image}}" alt="">
            <span>{{$card->title}}</span>
        </div>
        <div class="quiz_cart-area">
            <textarea name="" id="" cols="30" rows="10" class="quiz_step-area" disabled>{{$basket->message}}</textarea>
            <span>@lang('send.detail.message')</span>
        </div>

        @if ($basket->photo)
            <div class="quiz_cart-img">
                <img src="{{$basket->photo ?? ''}}" alt="">
                <span>@lang('send.detail.photo')</span>
            </div>
        @endif
    </div>

    @if (!empty($basket->offers->all()))
        <div class="quiz_cart-dop">
            <div class="quiz_cart-subtitle">@lang('send.detail.offers.title')</div>
            <div class="quiz_cart-dop_top">
                <div class="quiz_cart-dop-title">@lang('send.detail.offers.name')</div>
                <div class="quiz_cart-dop-title">@lang('send.detail.offers.amount')</div>
                <div class="quiz_cart-dop-title">@lang('send.price')</div>
                <div class="quiz_cart-dop-title">@lang('send.detail.offers.total')</div>
            </div>

            <div class="quiz_cart-dop_body">
                <div class="quiz_cart-dop_items">
                    @foreach ($basket->offers as $basketOffer)
                        @php
                            $offer = \App\Models\Offer::where('id', $basketOffer->offer_id)
                                ->with(['langs' => function($query) {
                                    return $query->where('lang', App::getLocale());
                                }])
                                ->first();
                        @endphp

                        <div class="quiz_cart-dop_item calculator-item">
                            <div class="quiz_cart-dop_img">
{{--                                <div data-popup="#dop_item_{{$offer->id}}" class="quiz_type-link popup_btn">--}}
{{--                                    ?--}}
{{--                                </div>--}}
                                <img src="{{$offer->picture}}" alt="">
                            </div>

                            <div class="quiz_cart-dop_info">
                                <div class="quiz_cart-dop_name">{{$offer->langs->first()->name}}</div>

                                <div class="quiz_cart-dop_count">
                                    <span class="minus calculator-decrease">-</span>
                                    <span type="text" class="calculator-onchange-sum">{{$basketOffer->amount}}</span>
                                    <span class="plus calculator-increase">+</span>
                                </div>

                                <div class="quiz_cart-dop_price calculator-base-price">
                                    {{$offer->price}} {{ucfirst($basket->currency)}}
                                </div>

                                <div class="quiz_cart-dop_total">
                                    <span class="calculator-onchange-total">{{$offer->price * $basketOffer->amount}}</span> {{ucfirst($basket->currency)}}
                                </div>

                                <div class="quiz_cart-dop_delete"></div>
                            </div>

                            <input type="hidden" class="offer-count" name="offers[{{$offer->id}}]" value="0">
                            <input type="hidden" class="amount-limit" value="{{$offer->count}}">
                            <input type="hidden" class="calculator-onchange-full  amount-full" value="0">
                        </div>
                    @endforeach
                </div>

    {{--            <div class="quiz_cart-dop_item add_dop">--}}
    {{--                <div class="quiz_cart-dop_add">--}}

    {{--                </div>--}}
    {{--                <div class="quiz_cart-dop_name">--}}
    {{--                    Добавить товар--}}
    {{--                </div>--}}
    {{--            </div>--}}
            </div>
        </div>
    @endif
    <div class="quiz_cart-promo">
        <div  class="cart_promo-form">
            <div class="form_inp-wrapper">
                <span>@lang('send.promocode.title')</span>
                <input type="text" name="promo-title" class="form_inp">
            </div>
            <button class="cart_promo-btn js-promocode-apply">@lang('send.promocode.check')</button>
        </div>
        <div class="cart_promo-info">
            <div class="cart_promo-price">
                @lang('send.price'): <span class="calculator-result">{{$basket->price}}</span> {{ucfirst($basket->currency)}}
            </div>
            <div class="cart_promo-sale">
                @lang('send.promocode.discount'): <span class="calculator-promo">0</span> {{ucfirst($basket->currency)}}
            </div>
            <div class="cart_promo-total">
                @lang('send.promocode.total'): <span class="calculator-total">{{$basket->price}}</span> {{ucfirst($basket->currency)}}
            </div>
        </div>
    </div>

    <input type="hidden" class="calculator-input" name="price" value="{{$basket->price}}">
    <input type="hidden" name="promocode" value="">
    <input type="hidden" name="promo" value="0">
    <input type="hidden" name="action" value="detail">

    <div class="quiz_step-dispatch">
        <div class="quiz_step-title">@lang('send.data.title')</div>
        <div class="step-dispatch_item">
            <div class="step-dispatch_title">@lang('send.data.sender')</div>
            <div class="step-dispatch_inps">
                <div class="form_inp-wrapper">
                    <span>@lang('send.data.phone')</span>
                    <input type="text" name="tel_sender" class="form_inp" required>
                </div>
                <div class="form_inp-wrapper">
                    <span>@lang('send.data.name')</span>
                    <input type="text" name="name_sender" class="form_inp" required>
                </div>
                <div class="form_inp-wrapper">
                    <span>@lang('send.data.email')</span>
                    <input type="text" name="email_sender" class="form_inp" required>
                </div>
            </div>
        </div>
        <div class="step-dispatch_item">
            <div class="step-dispatch_title">@lang('send.data.reciever')</div>
            <div class="step-dispatch_inps">
                <div class="form_inp-wrapper">
                    <span>@lang('send.data.phone')</span>
                    <input type="text" name="tel_reciever" class="form_inp" required>
                </div>
                <div class="form_inp-wrapper">
                    <span>@lang('send.data.name')</span>
                    <input type="text" name="name_reciever" class="form_inp" required>
                </div>
            </div>
        </div>

        <div class="quiz-error alert alert-danger d-none"></div>

        <div class="step-dispatch_btns">
            <input type="submit" name="prev" class="quiz_step-quiz js-prev-step" value="@lang('send.step.prev')">
            <input type="submit" name="next" class="quiz_step-buy js-next-step" value="@lang('send.step.pay')">
            <input type="hidden" name="state" value="payment">
        </div>
    </div>
</div>
