<section class="gallery">
    <div class="container">
        <div class="gallery_title">@lang('main.gallery.title')</div>

        <div class="gallery_img">
            <img src="img/gallery_wrapper.png" alt="">
        </div>
        <div class="gallery_img1">
            <img src="img/gallery_img1.png" alt="">
        </div>
        <div class="gallery_img2">
            <img src="img/gallery_img2.png" alt="">
        </div>
        <div class="gallery_wrapper">
            <div class="gallery_item">

                <div class="gallery_content ">
                    <img src="img/gallery_item11.jpg" alt="">
                </div>
                <div class="gallery_content active">
                    <img src="img/gallery_item12.jpg" alt="">
                </div>
                <div class="gallery_content">
                    <img src="img/gallery_item13.jpg" alt="">
                </div>
                <ul class="gallery_tabs">
                    <li class="gallery_tab ">
                        <img src="img/gallery_item11.jpg" alt="">
                    </li>
                    <li class="gallery_tab active">
                        <img src="img/gallery_item12.jpg" alt="">
                    </li>
                    <li class="gallery_tab ">
                        <img src="img/gallery_item13.jpg" alt="">
                    </li>
                </ul>
                <div class="gallery_item-title">@lang('main.gallery.item_1')</div>
            </div>
            <div class="gallery_item">

                <div class="gallery_content ">
                    <img src="img/gallery_item21.jpg" alt="">
                </div>
                <div class="gallery_content active">
                    <img src="img/gallery_item22.jpg" alt="">
                </div>
                <div class="gallery_content">
                    <img src="img/gallery_item23.jpg" alt="">
                </div>
                <ul class="gallery_tabs">
                    <li class="gallery_tab ">
                        <img src="img/gallery_item21.jpg" alt="">
                    </li>
                    <li class="gallery_tab active">
                        <img src="img/gallery_item22.jpg" alt="">
                    </li>
                    <li class="gallery_tab ">
                        <img src="img/gallery_item23.jpg" alt="">
                    </li>
                </ul>
                <div class="gallery_item-title">@lang('main.gallery.item_2')</div>
            </div>
            <div class="gallery_item">

                <div class="gallery_content ">
                    <img src="img/gallery_item31.jpg" alt="">
                </div>
                <div class="gallery_content active">
                    <img src="img/gallery_item32.jpg" alt="">
                </div>
                <div class="gallery_content">
                    <img src="img/gallery_item33.jpg" alt="">
                </div>
                <ul class="gallery_tabs">
                    <li class="gallery_tab ">
                        <img src="img/gallery_item31.jpg" alt="">
                    </li>
                    <li class="gallery_tab active">
                        <img src="img/gallery_item32.jpg" alt="">
                    </li>
                    <li class="gallery_tab ">
                        <img src="img/gallery_item33.jpg" alt="">
                    </li>
                </ul>
                <div class="gallery_item-title">@lang('main.gallery.item_3')</div>
            </div>
             <div class="gallery_item">

                <div class="gallery_content ">
                    <img src="img/gallery_item41.jpg" alt="">
                </div>
                <div class="gallery_content active">
                    <img src="img/gallery_item42.jpg" alt="">
                </div>
                <div class="gallery_content">
                    <img src="img/gallery_item43.jpg" alt="">
                </div>
                <ul class="gallery_tabs">
                    <li class="gallery_tab ">
                        <img src="img/gallery_item41.jpg" alt="">
                    </li>
                    <li class="gallery_tab active">
                        <img src="img/gallery_item42.jpg" alt="">
                    </li>
                    <li class="gallery_tab ">
                        <img src="img/gallery_item43.jpg" alt="">
                    </li>
                </ul>
                <div class="gallery_item-title">@lang('main.gallery.item_4')</div>
            </div>

        </div>

        <div class="gallery_inner">
            <div  data-popup="#video_popup2" class="gallery_video popup_btn">
                <div class="gallery_video-body">
                    <img src="img/video.jpg" alt="">
                    <span></span>
                </div>
                <div class="gallery_video-title">@lang('main.gallery.item_5')</div>
            </div>
            <div  data-popup="#video_popup3" class="gallery_video popup_btn">
                <div class="gallery_video-body">
                    <img src="img/video.jpg" alt="">
                    <span></span>
                </div>
                <div class="gallery_video-title">@lang('main.gallery.item_6')</div>
            </div>
        </div>

        <a href="{{route('checkout')}}" class="gallery_btn btn">@lang('main.main.send') <span></span></a>
    </div>
</section>
