<div class="content-row third-row">
    <h2 class="row__title row__title--third">@lang('send.photo')</h2>

    <div class="third-row-upload">
        <img src="{{asset('/img/icons/upload.svg')}}" width="65" height="72" alt="upload image">
        <input id="photo" class="upload__button" type="file" name="photo">
        <div class="upload__text">@lang('send.photo_click')</div>
    </div>

    <input type="hidden" name="action" value="photo">
</div>
