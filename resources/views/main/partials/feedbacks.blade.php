<section id="reviews" class="reviews">
    <div class="reviews_title">@lang('main.reviews.title')</div>
    <div class="reviews_wrapper">
        @foreach ($feedbacks as $feedback)
            <div class="reviews_item">
                <div class="reviews_item-wrapper">
                    <div class="reviews_item-img">
                        <img src="{{$feedback->picture}}" alt="Review{{$feedback->id}}">
                    </div>
                    <div class="reviews_item-info">
                        <div class="reviews_item-title"></div>
                        <div class="reviews_item-text">{{$feedback->body}}</div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</section>
