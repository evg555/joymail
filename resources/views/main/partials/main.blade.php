<section class="main">
    <div class="container">
        <div class="main_title">@lang('main.main.title')</div>
        <div class="main_title mobile">@lang('main.main.mobile_title')</div>

        <div class="main_items">
            <div class="main_item">@lang('main.main.delivery')</div>
            <div class="main_item">@lang('main.main.time')</div>
            <div class="main_item">@lang('main.main.whom')</div>
        </div>

        <div class="main_wrapper">
            <a href="{{route('checkout')}}" class="main_btn btn">@lang('main.main.send')<span></span></a>
            <div class="main_video">
                <a data-popup="#video_popup" class="main_video-btn popup_btn"></a>
                <div class="main_video-info">
                    <div class="main_video-title">@lang('main.main.video.title')</div>
                    <div class="main_video-text">@lang('main.main.video.time')</div>
                </div>
            </div>
        </div>
    </div>
</section>
