<section id="how" class="how">
    <div class="container">
        <div class="how_title">@lang('main.work.title')</div>
        <div class="how_items">
            @for($i = 1; $i < 4; $i++)
                <div class="how_item">
                    <div class="how_item-bef"><span></span></div>
                    <div class="how_item-img">
                        <img src="{{asset('/img/how_item'. $i .'.png')}}" alt="">
                    </div>
                    <div class="how_item-info">
                        <div class="how_item-title">@lang('main.work.work_title_' . $i)</div>
                        <div class="how_item-text">@lang('main.work.work_text_' . $i)</div>
                        <div class="how_item-number">{{$i}}</div>
                    </div>
                </div>
            @endfor
        </div>

        <div class="how_wrapper">
            <div class="how_wrapper-img">
                <picture>
                    <source srcset="{{asset('/img/how_wrapper-img2.png')}}" media="(max-width: 500px)">
                    <source srcset="{{asset('/img/how_wrapper-img.png')}}">
                    <img src="{{asset('/img/how_wrapper-img.png')}}" alt="">
                </picture>
            </div>

            <div class="how_wrapper-info">
                <div class="how_wrapper-title">@lang('main.work.work_title_4')</div>
                <div class="how_wrapper-text">@lang('main.work.work_text_4')</div>
                <div class="how_item-number">4</div>
                <a href="{{route('checkout')}}" class="how_wrapper-btn btn">
                    @lang('main.main.send')
                    <span></span>
                </a>
            </div>
        </div>
    </div>
</section>
