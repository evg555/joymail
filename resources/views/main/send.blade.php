@extends('layouts.app')

@section('content')
{{--        <div class="preloader">--}}
{{--            <img src="{{asset('/img/preloader.gif')}}" alt="">--}}
{{--        </div>--}}

<form  class="quiz" action="{{route('checkout')}}" id="data-form" method="post" enctype="multipart/form-data">
    <div class="quiz_partial">
        @include('main.partials.quiz.' . $template)
    </div>
</form>

@endsection
