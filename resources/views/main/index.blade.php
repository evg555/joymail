@extends('layouts.app')

@section('content')
    @include('main.partials.main')
    @include('main.partials.work')
    @include('main.partials.reasons')
    @include('main.partials.about')
    @include('main.partials.gallery')
    @include('main.partials.individual-offer')
    @include('main.partials.faq')
    @include('main.partials.feedbacks')
@endsection
