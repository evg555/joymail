Уважаемый (ая) {{$data['name_sender']}}.<br>
<br>
<b>
Ваш заказ #{{$data['id']}} полностью оплачен и принят к исполнению.<br>
Общая сумма: {{$data['price']}} {{$data['currency']}}</b>
<br>
Пожалуйста, не отвечайте на это письмо. Если Вам нужна консультация или помощь, обращайтесь к нам:<br>
<a href="mailto:support@joymail.biz">support@joymail.biz</a><br>
<a href="tel:+995557000656">+995557000656</a><br>
<a href="https://joymail.biz">https://joymail.biz</a>
<br><br>
Спасибо.<br>
С наилучшими пожеланиями, <br> Команда Joymail ))<br><br>

-------------------------------------------------------------<br>

Dear {{$data['name_sender']}}.<br><br>

<b>
Your order #{{$data['id']}} has been PAID and accepted for processing.<br>
Total paid: {{$data['price']}} {{$data['currency']}}</b>
<br>
Please do not reply for this e-mail. For help or assistance, contact us: <br>
<a href="mailto:support@joymail.biz">support@joymail.biz</a><br>
<a href="tel:+995557000656">+995557000656</a><br>
<a href="https://joymail.biz">https://joymail.biz</a>
<br><br>
Thank you.<br>
Best Regards, <br> Team of Joymail LLC<br>

<img src="https://joymail.biz/logo.png">
<br>
