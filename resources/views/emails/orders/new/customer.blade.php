<br>
Уважаемый (ая)  {{$data['name_sender']}}.<br>

Спасибо за Ваш заказ #{{$data['id']}}!<br>

<b>Общая сумма: {{$data['price']}} {{$data['currency']}}
<br> Ваш заказ ожидает подтвержденя оплаты. <br> 
В случае успешной оплаты, Вам придёт письмо-подтверждение.</b><br><br>
Детали заказа:<br>
Сообщение: {{$data['message']}}<br>
Город: {{$data['city']}}<br>
@if (!empty($data['promocode']))
    Promocode: {{$data['promocode']}}
@endif
<br>
Имя отправителя: {{$data['name_sender']}}<br>
Тел. отправителя: {{$data['tel_sender']}}<br>
Имя получателя: {{$data['name_reciever']}}<br>
Тел. получателя: {{$data['tel_reciever']}}<br>
Тип открытки: {{$data['letter_type']}}<br>

@if (isset($data['offers']))
    @foreach ($data['offers'] as $offer)
        {{$offer['name']}}: {{$offer['amount']}} pcs.
    @endforeach
@endif

<br><br>
Ваша фотография находится здесь:

    @if (!empty($data['photo']))
        <a href="{{$data['site_url']}}/{{$data['photo']}}">Открыть</a>
    @endif
<br>
------------------------------------------------------------
<br>
Пожалуйста, не отвечайте на это письмо. Если Вам нужна консультация или помощь, обращайтесь к нам:<br>
<a href="mailto:support@joymail.biz">support@joymail.biz</a><br>
<a href="tel:+995557000656">+995557000656</a><br>
<a href="https://joymail.biz">https://joymail.biz</a>
<br>
Спасибо.<br>
С наилучшими пожеланиями, <br> Команда Joymail ))<br>

-------------------------------------------------------------


<br><br>Dear {{$data['name_sender']}}.<br>

Thank you for your order #{{$data['id']}}.<br>

<b>Total amount: {{$data['price']}} {{$data['currency']}}
<br> Your order is awaiting for payment.<br> Please wait for the confirmation e-mail.</b>
<br><br>

Details:<br>
Message: {{$data['message']}}<br>
City: {{$data['city']}}<br>
@if (!empty($data['promocode']))
    Promocode: {{$data['promocode']}}
@endif
<br>
Sender name: {{$data['name_sender']}}<br>
Sender phone: {{$data['tel_sender']}}<br>
Receiver name: {{$data['name_reciever']}}<br>
Receiver Phone: {{$data['tel_reciever']}}<br>
Additional items:<br>
Type of JoyKa: {{$data['letter_type']}}<br>

@if (isset($data['offers']))
    @foreach ($data['offers'] as $offer)
        {{$offer['name']}}: {{$offer['amount']}} pcs.
    @endforeach
@endif
<br>
<br>
Your uploaded picture is here:

    @if (!empty($data['photo']))
        <a href="{{$data['site_url']}}/{{$data['photo']}}">Open link</a>
    @endif
<br>
-------------------------------------------------------------
<br>
Please do not reply for this e-mail. For help or assistance, contact us: <br>
<a href="mailto:support@joymail.biz">support@joymail.biz</a><br>
<a href="tel:+995557000656">+995557000656</a><br>
<a href="https://joymail.biz">https://joymail.biz</a>
<br>
Thank you.
<p>Best Regards, <br> Team of Joymail LLC ))</p>

<img src="{{$data['site_url']}}/logo.png">

