$(function () {
    let form = $('#data-form'),
        card = 'input[name=card_id]',
        quiz = $('.quiz_partial'),
        promocodeBtn = '.js-promocode-apply',
        promocodeInput = 'input[name="promo-title"]',
        promocode = 'input[name="promocode"]',
        promo = 'input[name="promo"]',
        postcardBtn = '.js-postcard-apply';

    $('.quiz_step-desc .quiz_step-area').keyup(function () {
        $('.quiz_cart-area .quiz_step-area').val($('.quiz_step-desc .quiz_step-area').val())
    })

    quiz.on('change', card, function() {
        $('input[name=price]').val($(this).data('price'));
    });

    quiz.on('change', '#photo', function(e) {
        let alert = $('.quiz-error');

        alert.addClass('d-none');

        if (this.files[0]) {
            let file = this.files[0];
            let allowedMimeTypes = [
                'image/jpeg',
                'image/jpg',
                'image/png'
            ];
            let fr = new FileReader();

            if (file.size > 10000000) {
                this.value = '';

                alert.html('Image size must be less than 10Mb');
                alert.removeClass('d-none');

                return false;
            }

            if (!allowedMimeTypes.includes(file.type)) {
                this.value = '';

                alert.html('Allowed images types are jpg, jpeg, png');
                alert.removeClass('d-none');

                return false;
            }

            $('.box-postcard').removeAttr( 'style' ).find('span').show();
            $('input[name=postcard_id]').prop('checked', false);

            fr.addEventListener("load", function () {
                $('.quiz_step-file_span').hide();
                $('.box-file').css('background-size', 'contain')
                    .css('background-image', 'url(' + fr.result + ')')
                    .css('background-position', 'center center')
                    .css('background-color', 'inherit')
                    .css('background-repeat', 'no-repeat');
            }, false);

            fr.readAsDataURL(file);
        }
    });

    quiz.on('click', '.js-next-step', function(e) {
        e.preventDefault();

        let alert = $('.quiz-error');
        let DATA = new FormData(form.get(0));

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: "/ajax/nextStep",
            type: "POST",
            dataType: "json",
            data: DATA,
            processData: false,
            contentType: false,
            beforeSend: function () {
                alert.addClass('d-none');
            },
            success: function(answ) {
                if (answ.status == 'success') {
                    if (answ.redirect) {
                        location.href = '/thanks';
                    }

                    form.find('.quiz_partial').html(answ.view);
                }
            },
            error: function(xhr) {
                if (xhr.responseJSON.errors) {
                    let errors = xhr.responseJSON.errors;
                    let errorMsg = '';

                    for (let key in errors) {
                        errorMsg += errors[key].join("<br>") + "<br>";
                    }

                    alert.html(errorMsg);
                    alert.removeClass('d-none');
                }
            }
        });
    });

    quiz.on('click', '.js-prev-step', function(e) {
        e.preventDefault();

        let alert = $('.quiz-error');
        let DATA = new FormData(form.get(0));

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: "/ajax/prevStep",
            type: "POST",
            dataType: "json",
            data: DATA,
            processData: false,
            contentType: false,
            beforeSend: function () {
                alert.addClass('d-none');
            },
            success: function(answ) {
                if (answ.status == 'success') {
                    form.find('.quiz_partial').html(answ.view);
                } else {
                    //alert.html(answ.msg);
                    //alert.removeClass('d-none');
                }
            }
        });
    });

    quiz.on('input', promocodeInput, function(e) {
        $(this).css('color', 'black');
    });

    quiz.on('click', promocodeBtn, function(e) {
        e.preventDefault();

        if ($(this).hasClass('applied')) {
            return;
        }

        var DATA = {};
        DATA.code = $(promocodeInput).val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: "/ajax/discount/",
            type: "POST",
            dataType: "json",
            data: DATA,
            beforeSend: function() {
                $(promocodeInput).prop( "disabled", true );
            },
            success: function(answ) {
                $(promocodeInput).prop( "disabled", false);

                if (answ.status == 'success') {
                    let currentPrice = $('.calculator-input').val();
                    let newPrice;

                    if (answ.type == '%') {
                        answ.amount = parseInt((answ.amount / 100) * currentPrice);
                    }

                    newPrice = currentPrice - answ.amount;

                    if (newPrice < 0) {
                        newPrice = 0;
                    }

                    $('.calculator-promo').text(answ.amount);
                    $('.calculator-total').text(newPrice);
                    $(promo).val(answ.amount);
                    $(promocode).val($(promocodeInput).val());


                    $(promocodeInput).prop( "disabled", true );
                    $(promocodeBtn).addClass('applied');
                    $(promocodeBtn).hide();
                } else {
                    $(promocodeInput).css('color', 'red');
                }
            }
        });
    });

    $(document).on('click', postcardBtn, function() {
        let checkedInput = $('input[type=radio]:checked');
        let src = checkedInput.parents('label').find('img').attr('src');
        let value = checkedInput.val();

        $('.box-postcard').css('background-size', 'contain')
            .css('background-image', 'url(' + src + ')')
            .css('background-position', 'center center')
            .css('background-color', 'inherit')
            .css('background-repeat', 'no-repeat')
            .find('span').hide();

        $('.box-file').removeAttr( 'style' );
        $('.quiz_step-file_span').show();
        $('#photo').val('');

        $.fancybox.close();
    });

    /* @calculator.js*/
    calculator.init();
});
