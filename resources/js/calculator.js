(function(){
    calculator = {
        result: '.calculator-result',
        input: '.calculator-input',
        increase: '.calculator-increase',
        decrease: '.calculator-decrease',
        onchange_sum: '.calculator-onchange-sum',
        onchange_full: '.calculator-onchange-full',
        onchange_total: '.calculator-onchange-total',
        onchange_total_with_discount: '.calculator-total',
        promo: '.calculator-promo',
        base_price: '.calculator-base-price',
        limit: '.amount-limit',
        item: '.calculator-item',
        count: '.offer-count',
        parent: '.quiz_partial',
        discount: 0,
        total_with_discount: 0,
        total: 0,

        init: function () {
            this.registerEvents();
        },
        add: function(sum, value) {
            let total = sum * value;
            let input = $(this.input).val();

            this.total = parseFloat(input) + total;

            if (this.total < 0) {
                this.total = 0;
            }

            this.total_with_discount = this.total;

            if (this.discount > 0) {
                this.total_with_discount = this.total - this.discount;
            }

            if (this.total_with_discount < 0) {
                this.total_with_discount = 0;
            }

            this.render();
        },
        render: function() {
            $(this.result).text(this.total);
            $(this.input).val(this.total);
            $(this.onchange_total_with_discount).text(this.total_with_discount);
        },
        registerEvents: function() {
            $(this.parent).on('click', this.increase, function() {
                let item = $(this).parents(calculator.item),
                    basePrice = parseFloat(item.find(calculator.base_price).text()),
                    full = item.find(calculator.onchange_full),
                    total = item.find(calculator.onchange_total),
                    limit = parseInt(item.find(calculator.limit).val()),
                    amount = parseInt(item.find(calculator.onchange_sum).text()),
                    result = item.find(calculator.count),
                    promo = parseInt($(calculator.promo).text()),
                    fullFlag = 0;

                if (amount < limit) {
                    item.find(calculator.onchange_sum).text(++amount);
                    total.text(amount * basePrice);
                    result.val(amount);
                    calculator.discount = promo;
                    calculator.add(basePrice, 1);
                } else {
                    fullFlag = 1;
                }

                full.val(fullFlag);
            });

            $(this.parent).on('click', this.decrease, function() {
                let item = $(this).parents(calculator.item),
                    basePrice = parseFloat(item.find(calculator.base_price).text()),
                    total = item.find(calculator.onchange_total),
                    full = item.find(calculator.onchange_full),
                    amount = parseInt(item.find(calculator.onchange_sum).text()),
                    promo = parseInt($(calculator.promo).text()),
                    result = item.find(calculator.count),
                    fullFlag = 0;

                if (amount > 0) {
                    item.find(calculator.onchange_sum).text(--amount);
                    total.text(amount * basePrice);
                    result.val(amount);
                    calculator.discount = promo;
                    calculator.add(basePrice, -1);
                } else {
                    fullFlag = -1;
                }

                full.val(fullFlag);
            });
        }
    };
})();
