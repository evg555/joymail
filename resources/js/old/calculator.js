(function(){
    calculator = {
        result: '.calculator-result',
        input: '.calculator-input',
        increase: '.calculator-increase',
        decrease: '.calculator-decrease',
        onchange_sum: '.calculator-onchange-sum',
        onchange_total: '.calculator-onchange-total',
        onchange_full: '.calculator-onchange-full',
        card: '.calculator-card',
        card_sum: '.calculator-card-sum',
        base_price: '.calculator-base-price',
        item: '.calculator-item',
        card_price: 0,
        file_price: 50,
        total: 3,

        init: function () {
            //Регистрируем события
            this.registerEvents();

            //Обнуляем
            this.card_price = $(this.card).first().data('value');
            localStorage.setItem('calculate_card_value', this.card_price);
            localStorage.setItem('calculate_total', 0);

            this.total = this.card_price;
            this.add(1, this.total);
            this.render();
        },
        add: function(sum, value) {
            total = sum * value;
            this.total = parseFloat(localStorage.getItem('calculate_total')) + total;

            if (this.total < 0) {
                this.total = 0;
            }

            localStorage.setItem('calculate_total', this.total);
            this.render();
        },
        render: function() {
            $(this.card_sum).text(this.card_price);
            $(this.result).text(this.total);
            $(this.input).val(this.total);
        },
        registerEvents: function() {
            $(this.increase).on('click', function() {
                let item = $(this).parents(calculator.item),
                    basePrice = item.find(calculator.base_price).text(),
                    value = parseInt(item.find(calculator.onchange_sum).text()),
                    full = parseInt(item.find(calculator.onchange_full).val());

                if (full !== 1) {
                    calculator.add(basePrice, 1);
                    item.find(calculator.onchange_total).text(value * basePrice);
                }
            });

            $(this.decrease).on('click', function() {
                let item = $(this).parents(calculator.item),
                    basePrice = item.find(calculator.base_price).text(),
                    value = parseInt(item.find(calculator.onchange_sum).text()),
                    full = parseInt(item.find(calculator.onchange_full).val());

                if (full !== -1) {
                    calculator.add(basePrice, -1);
                    item.find(calculator.onchange_total).text(value * basePrice);
                }
            });

            $(this.card).on('click', function(){
                let currentValue = parseInt($(this).data('value'));
                let oldCurrentPrice = localStorage.getItem('calculate_card_value');

                if (currentValue !== oldCurrentPrice) {
                    let diff = currentValue - oldCurrentPrice;
                    localStorage.setItem('calculate_card_value', currentValue);
                    calculator.add(1, diff);
                }
            });
        }
    };
})();
