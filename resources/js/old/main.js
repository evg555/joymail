import Swal from 'sweetalert2';

$(function () {
    let card = '.first-row__select > .item-card',
        uploadBtn = '.upload__button',
        uploadImg = $('.third-row-upload'),
        plus = '.mdi-plus',
        minus = '.mdi-minus',
        quiz = $('.quiz');

    quiz.on('click', minus, function() {
        let amountSpan = $(this).parents('.total-num').find('span'),
            amount = parseInt(amountSpan.text()),
            full = $(this).parents('.swiper-slide').find('.amount-full'),
            result = $(this).parents('.swiper-slide').find('.offer-count'),
            resultTotal = $('.calculator-result'),
            resultPrice = $('.calculator-input'),
            price = parseFloat($(this).parents('.swiper-slide').find('.calculator-base-price').text()),
            totalSpan = $(this).parents('.swiper-slide').find('.total-price span'),
            total;

        if (amount > 0) {
            amountSpan.text(--amount);
            total = price * amount;
            result.val(amount);
            full.val(0);
            totalSpan.text(total);
            resultTotal.text(parseFloat(resultTotal.text()) - price);
            resultPrice.val(parseFloat(resultPrice.val()) - price);
        } else {
            full.val(-1);
        }
    });

    quiz.on('click', plus, function() {
        let amountSpan = $(this).parents('.total-num').find('span'),
            amount = parseInt(amountSpan.text()),
            limit = parseInt($(this).parents('.swiper-slide').find('.amount-limit').val()),
            full = $(this).parents('.swiper-slide').find('.amount-full'),
            result = $(this).parents('.swiper-slide').find('.offer-count'),
            resultTotal = $('.calculator-result'),
            resultPrice = $('.calculator-input'),
            price = parseFloat($(this).parents('.swiper-slide').find('.calculator-base-price').text()),
            totalSpan = $(this).parents('.swiper-slide').find('.total-price span'),
            total;

        if (amount < limit) {
            amountSpan.text(++amount);
            total = price * amount;
            result.val(amount);
            full.val(0);
            totalSpan.text(total);
            resultTotal.text(parseFloat(resultTotal.text()) + price);
            resultPrice.val(parseFloat(resultPrice.val()) + price);
        } else {
            full.val(1);
        }
    });

    quiz.on('click', card, function() {
        $(card).removeClass('card-active');
        $(this).addClass('card-active');

        let price = $(this).data('value');
        let letterType = $(this).data('item');

        $('input[name=total]').val(price);
        $('input[name=letter_type]').val(letterType);
    });

    quiz.on('change', uploadBtn, function(e) {
        if (this.files[0]) {
            let file = this.files[0];
            let allowedMimeTypes = [
                'image/jpeg',
                'image/jpg',
                'image/png'
            ];
            let fr = new FileReader();

            if (file.size > 10000000) {
                this.value = '';

                Swal.fire({
                    icon: 'error',
                    text: 'Image size must be less than 10Mb',
                    confirmButtonText: 'OK'
                });

                return false;
            }

            if (!allowedMimeTypes.includes(file.type)) {
                this.value = '';

                Swal.fire({
                    icon: 'error',
                    text: 'Allowed images types are jpg, jpeg, png',
                    confirmButtonText: 'OK'
                });

                return false;
            }

            fr.addEventListener("load", function () {
                uploadImg.find('img').hide();
                uploadImg.find('.upload__text').hide();

                uploadImg.css('background-size', 'cover');
                uploadImg.css('background-image', 'url(' + fr.result + ')');
                uploadImg.css('background-position', 'center center');
            }, false);

            fr.readAsDataURL(file);
        }
    });

    //Glued button in mobile version
    $(document).on('scroll', function() {
        if (pageYOffset >= 485 && window.screen.width <= 768) {
            $('.content__btn').addClass('btn-fix');
            $('.btn-row').hide();
        } else {
            $('.btn-row').show();
            $('.content__btn').removeClass('btn-fix');
        }
    });

    //Choice country
    $('.js-set-country').on('change', function(e) {
        let countryId = $(this).val();

        $.cookie('country_id', countryId, {expires: 30, path: '/'});

        location.reload();
    });

    //Choice lang
    $('.js-set-lang').on('change', function(e) {
        let lang = $(this).val();

        $.cookie('lang', lang, {expires: 30, path: '/'});

        location.reload();
    });

    /* @calculator.js*/
    //calculator.init();
});
