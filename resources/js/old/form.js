import Swal from 'sweetalert2';

$(function () {
    var form = $('#data-form');
    var formIndividualOrder = $('#data-form-individual');
    var promocodeBtn = '.js-promocode-apply';
    var promocodeInput= 'input[name="promocode"]';
    var promocodeIcon = $('.promocode__icon');
    var quiz = $('.quiz');

    form.trigger('reset');
    formIndividualOrder.trigger('reset');

    form.on('submit', function(e) {
        e.preventDefault();

        let targetForm = $(this);
        let DATA = new FormData(targetForm.get(0));
        let alert = $(this).find('.alert');

        // let promo = targetForm.find('input[name=promocode]');
        //
        // if (promo.prop('disabled')) {
        //     DATA.append('promocode', promo.val());
        // } else {
        //     DATA.delete('promocode');
        // }
        //
        // $('.create-item').each(function(index, value) {
        //     if ($(this).hasClass('card-active')) {
        //         DATA.append('letter_type', $(this).data('item'));
        //     }
        // });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: "/ajax/saveForm",
            type: "POST",
            dataType: "json",
            data: DATA,
            processData: false,
            contentType: false,
            beforeSend: function () {
                alert.hide();
            },
            success: function(answ) {
                if (answ.status == 'success') {
                    form.find('.quiz').html(answ.view);
                } else {
                    alert.html(answ.msg);
                    alert.show();
                }
            }
        });



        // $.ajax({
        //     url: "/send",
        //     type: "POST",
        //     dataType: "json",
        //     data: DATA,
        //     processData: false,
        //     contentType: false,
        //     beforeSend: function() {
        //         ym(64404439,'reachGoal','pay');
        //         $('.preloader').show();
        //     },
        //     success: function(answ) {
        //         if (answ.status == 'success') {
        //             location.href = answ.url;
        //         } else {
        //             $('.preloader').hide();
        //             Swal.fire({
        //                 title: 'Payment error',
        //                 text: answ.msg,
        //                 icon: 'error',
        //                 confirmButtonText: 'OK'
        //             });
        //         }
        //     },
        //     error: function(xhr) {
        //         $('.preloader').hide();
        //         if (xhr.responseJSON.errors) {
        //             let errors = xhr.responseJSON.errors;
        //             let errorMsg = '';
        //
        //             for (let key in errors) {
        //                 errorMsg += errors[key].join("\n") + "\n";
        //             }
        //
        //             Swal.fire({
        //                 title: 'Form is not sent',
        //                 text: errorMsg,
        //                 icon: 'error',
        //                 confirmButtonText: 'OK'
        //             });
        //         }
        //     }
        // });
    });

    formIndividualOrder.on('submit', function(e) {
        e.preventDefault();

        let targetForm = $(this);
        let DATA = new FormData(targetForm.get(0));

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: "/ajax/individual",
            type: "POST",
            dataType: "json",
            data: DATA,
            processData: false,
            contentType: false,
            beforeSend: function() {
                ym(64404439,'reachGoal','send_individual_order');
                $('#individual-order').modal('hide');
                $('.preloader').show();
            },
            success: function(answ) {
                $('.preloader').hide();

                if (answ.status == 'success') {
                    formIndividualOrder.trigger('reset');
                }

                Swal.fire({
                    title: 'Order result',
                    text: answ.msg,
                    icon: answ.status,
                    confirmButtonText: 'OK'
                });
            },
            error: function(xhr) {
                $('.preloader').hide();

                if (xhr.responseJSON.errors) {
                    let errors = xhr.responseJSON.errors;
                    let errorMsg = '';

                    for (let key in errors) {
                        errorMsg += errors[key].join("\n") + "\n";
                    }

                    Swal.fire({
                        title: 'Form is not sent',
                        text: errorMsg,
                        icon: 'error',
                        confirmButtonText: 'OK'
                    });
                }
            }
        });
    });

    quiz.on('input', promocodeInput, function(e) {
        $(this).css('color', 'black');
        $('.promocode-alert').hide();
        promocodeIcon.empty();
    });

    quiz.on('click', promocodeBtn, function(e) {
        e.preventDefault();

        if ($(this).hasClass('applied')) {
            return;
        }

        var DATA = {};
        DATA.code = $(promocodeInput).val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: "/ajax/discount/",
            type: "POST",
            dataType: "json",
            data: DATA,
            beforeSend: function() {
                $(promocodeBtn).addClass('btn__disabled');
                promocodeIcon.html('<i class="fa fa-clock-o grey" aria-hidden="true"></i>');
            },
            success: function(answ) {
                $(promocodeBtn).removeClass('btn__disabled');

                if (answ.status == 'success') {
                    var amount;
                    //var total = parseInt(localStorage.getItem('calculate_total'));

                    promocodeIcon.html('<i class="fa fa-check green" aria-hidden="true"></i>');
                    $(promocodeInput).prop( "disabled", true );
                    $(promocodeBtn).addClass('applied');
                    $(promocodeBtn).hide();
                    $('.promocode-success').show();

                    $('input[name=promocode_price]').val(answ.amount);
                    $('input[name=promocode_type]').val(answ.type);

                    // if (answ.type == '%') {
                    //     amount = -parseInt(total * answ.amount / 100);
                    // }
                    //
                    // if (answ.type == 'fix') {
                    //     amount = -answ.amount;
                    // }
                    //
                    // calculator.add(amount, 1);
                } else {
                    promocodeIcon.html('<i class="fa fa-exclamation-circle red" aria-hidden="true"></i>');
                    $('.promocode-alert').show();
                }
            }
        });
    });
});

