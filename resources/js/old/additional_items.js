import Swiper from './swiper-bundle.min';

$(function () {
    const additionalSwiper = new Swiper('.fourth-row--additional', {
        speed: 200,
        slidesPerView: 'auto',
        loop: false,
        spaceBetween: 10,
        freeMode: true,
        // pagination: {
        //   el: '.swiper-pagination',
        //   type: 'bullets',
        //   clickable: true
        // }
    });
});
