import Swiper from './swiper-bundle.min';

$(function () {
  const reviewsSwiper = new Swiper('.reviews', {
      speed: 200,
      slidesPerView: 1,
      loopedSlides: 1,
      loop: true,
      spaceBetween: 39,
      slidesOffsetBefore: 165,
      pagination: {
        el: '.swiper-pagination',
        type: 'bullets',
        clickable: true
      },
      breakpoints: {
          320: {
            slidesPerView: 1,
            slidesOffsetBefore: 0
          },
          560: {
            slidesPerView: 2,
            slidesOffsetBefore: 200
          },
          1024: {
            slidesPerView: 3,
            slidesOffsetBefore: 190
          },
          1366: {
            slidesPerView: 4,
            slidesOffsetBefore: 190
          },
          1920: {
            slidesPerView: 6,
            slidesOffsetBefore: 165
          }
      }
  });
});
