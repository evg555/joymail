import Inputmask from "inputmask/lib/inputmask";

$(function () {
    let individual = $('form.form_inner');

    $('.motives_slider').slick({
        slidesToShow: 3,
        dots: true,
        speed: 0,
        customPaging: function (slider, i) {
            var current = i + 1;
            current = current < 10 ? +current : current;

            var total = slider.slideCount;
            total = total < 10 ? +total : total;

            var currentTotal = 0
            if (current < 10) {
                currentTotal = '0' + current
            } else {
                currentTotal = current
            }

            var totalTotal = 0
            if (total < 10) {
                totalTotal = '0' + total
            } else {
                totalTotal = total
            }

            return (
                '<button type="button" role="button" tabindex="0" class="slick-dots-button">\
                  <span class="slick-dots-current">' + currentTotal + '</span>\
                  <span class="slick-dots-separator"> / </span>\
                  <span class="slick-dots-total">' + totalTotal + '</span>\
                  </button>'
            );

        },
        prevArrow: '<button type="button" class="slick-arrow slick-prev"><img src="img/slider_left.svg" alt=""></button>',
        nextArrow: '<button type="button" class="slick-arrow slick-next"><img src="img/slider_right.svg" alt=""></button>',
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 701,
                settings: {
                    slidesToShow: 1,
                }
            },
            {
                breakpoint: 501,
                settings: {
                    slidesToShow: 1,
                    arrows: false,
                    speed: 300,
                }
            },
        ]
    });

    $('.gallery_wrapper').slick({
        slidesToShow: 3,
        customPaging: function (slider, i) {
            var current = i + 1;
            current = current < 10 ? +current : current;

            var total = slider.slideCount;
            total = total < 10 ? +total : total;

            var currentTotal = 0
            if (current < 10) {
                currentTotal = '0' + current
            } else {
                currentTotal = current
            }

            var totalTotal = 0
            if (total < 10) {
                totalTotal = '0' + total
            } else {
                totalTotal = total
            }

            return (
                '<button type="button" role="button" tabindex="0" class="slick-dots-button">\
                  <span class="slick-dots-current">' + currentTotal + '</span>\
                <span class="slick-dots-separator"> / </span>\
                <span class="slick-dots-total">' + totalTotal + '</span>\
                </button>'
            );

        },
        prevArrow: '<button type="button" class="slick-arrow slick-prev"><img src="img/slider_left.svg" alt=""></button>',
        nextArrow: '<button type="button" class="slick-arrow slick-next"><img src="img/slider_right.svg" alt=""></button>',
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 701,
                settings: {
                    slidesToShow: 1,

                }
            },
            {
                breakpoint: 501,
                settings: {
                    slidesToShow: 1,
                    arrows: false,
                    speed: 300,
                    dots: true,
                }
            },
        ]
    })

    $('.reviews_wrapper').slick({
        dots: true,
        prevArrow: '<button type="button" class="slick-arrow slick-prev"><img src="img/slider_left.svg" alt=""></button>',
        nextArrow: '<button type="button" class="slick-arrow slick-next"><img src="img/slider_right.svg" alt=""></button>',
        customPaging: function (slider, i) {
            var current = i + 1;
            current = current < 10 ? +current : current;

            var total = slider.slideCount;
            total = total < 10 ? +total : total;


            var currentTotal = 0
            if (current < 10) {
                currentTotal = '0' + current
            } else {
                currentTotal = current
            }

            var totalTotal = 0
            if (total < 10) {
                totalTotal = '0' + total
            } else {
                totalTotal = total
            }

            return (
                '<button type="button" role="button" tabindex="0" class="slick-dots-button">\
                <span class="slick-dots-current">' + currentTotal + '</span>\
                <span class="slick-dots-separator"> / </span>\
                <span class="slick-dots-total">' + totalTotal + '</span>\
                </button>'
            );

        },
        responsive: [
            {
                breakpoint: 501,
                settings: {
                    slidesToShow: 1,
                    arrows: false,
                    speed: 300,
                    dots: true,
                }
            },
        ]
    })

    $('ul.gallery_tabs').on('click', 'li:not(.active)', function () {
        $(this).addClass('active')
            .siblings()
            .removeClass('active')
            .closest('div.gallery_item')
            .find('div.gallery_content')
            .removeClass('active')
            .eq($(this)
            .index())
            .addClass('active');
    })

    $('.faq_item-title').on('click', function () {
        $('.faq_item-title').not($(this)).removeClass('active');
        $('.faq_item .faq_content').not($(this).next()).slideUp(500);

        $(this).toggleClass('active');
        $(this).next().slideToggle(600);
    });

    $(document).on('click', '.popup_btn', function (e) {
        e.preventDefault();

        let idPopup = $(this).attr('data-popup');

        $.fancybox.close();
        $.fancybox.open({
            src: idPopup,
            type: 'inline',
            touch: false,
            autoFocus: false,
        });
    });

    $('.menu_burger, .menu_link').click(function () {
        $('.menu_burger, .header').toggleClass('active')
    })

    $('.we_block-wrapper').slick({
        slidesToShow: 3,
        dots: true,
        customPaging: function (slider, i) {
            var current = i + 1;
            current = current < 10 ? +current : current;

            var total = slider.slideCount;
            total = total < 10 ? +total : total;

            var currentTotal = 0
            if (current < 10) {
                currentTotal = '0' + current
            } else {
                currentTotal = current
            }

            var totalTotal = 0
            if (total < 10) {
                totalTotal = '0' + total
            } else {
                totalTotal = total
            }

            return (
                '<button type="button" role="button" tabindex="0" class="slick-dots-button">\
                <span class="slick-dots-current">' + currentTotal + '</span>\
                <span class="slick-dots-separator"> / </span>\
                <span class="slick-dots-total">' + totalTotal + '</span>\
                </button>'
            );

        },
        prevArrow: '<button type="button" class="slick-arrow slick-prev"><img src="img/slider_left.svg" alt=""></button>',
        nextArrow: '<button type="button" class="slick-arrow slick-next"><img src="img/slider_right.svg" alt=""></button>',
        responsive: [
            {
                breakpoint: 4000,
                settings: "unslick",
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 700,
                settings: {
                    slidesToShow: 1,
                }
            },
            {
                breakpoint: 501,
                settings: {
                    slidesToShow: 1,
                    arrows: false,
                    speed: 300,
                }
            },
        ]
    });

    $('.gallery_inner').slick({
        dots: true,
        arrows: false,
        customPaging: function (slider, i) {
            var current = i + 1;
            current = current < 10 ? +current : current;

            var total = slider.slideCount;
            total = total < 10 ? +total : total;

            var currentTotal = 0
            if (current < 10) {
                currentTotal = '0' + current
            } else {
                currentTotal = current
            }

            var totalTotal = 0
            if (total < 10) {
                totalTotal = '0' + total
            } else {
                totalTotal = total
            }

            return (
                '<button type="button" role="button" tabindex="0" class="slick-dots-button">\
          <span class="slick-dots-current">' + currentTotal + '</span>\
    <span class="slick-dots-separator"> / </span>\
    <span class="slick-dots-total">' + totalTotal + '</span>\
    </button>'
            );

        },
        prevArrow: '<button type="button" class="slick-arrow slick-prev">       <img src="img/slider_left.svg" alt=""></button>',
        nextArrow: '<button type="button" class="slick-arrow slick-next">       <img src="img/slider_right.svg" alt=""></button>',
        responsive: [
            {
                breakpoint: 4000,
                settings: "unslick",
            },
            {
                breakpoint: 701,
                settings: {
                    slidesToShow: 1,
                }
            },
        ]
    });

    $(window).on('resize', function () {
        if ($(window).width() < '992') {
            $('.we_block-wrapper').slick('resize');
            $('.gallery_inner').slick('resize');
        }
    });

    //Change langs
    $('.card_right-select').each(function () {
        var $this = $(this),
            numberOfOptions = $(this).children('option').length,
            styledImg = $(this).children('option').attr('data-img');

        $this.addClass('select-hidden');
        $this.wrap('<div class="select"></div>');
        $this.after('<div class="select-styled"></div>');

        var styledSelect = $this.next('div.select-styled');
        let selectedOption = $this.children('option[selected]');

        styledSelect.html(selectedOption.text() + ' ' + `<img src='${selectedOption.attr('data-img')}' alt=''>`);

        var list = $('<ul />', {
            'class': 'select-options'
        }).insertAfter(styledSelect);

        for (var i = 0; i < numberOfOptions; i++) {
            $('<li />', {
                html: $this.children('option').eq(i).text() + `<img src='${$this.children('option').eq(i).attr('data-img')}' alt=''>`,
                rel: $this.children('option').eq(i).val()
            }).appendTo(list);
        }

        let lang = $.cookie('lang').toUpperCase();

        if (lang) {
            $this.val(lang);
        }

        var listItems = list.children('li');

        styledSelect.on('click', function (e) {
            e.stopPropagation();

            $('div.select-styled.active').not(this).each(function () {
                $(this).removeClass('active').next('ul.select-options').hide();
            });

            $(this).toggleClass('active').next('ul.select-options').toggle();
        });

        listItems.on('click', function (e) {
            e.stopPropagation();

            let lang = $(this).attr('rel').toLowerCase();

            styledSelect.html($(this).html()).removeClass('active');
            $this.val($(this).attr('rel'));
            list.hide();

            $.cookie('lang', lang, {expires: 30, path: '/'});

            location.reload();
        });

        $(document).on('click', function () {
            styledSelect.removeClass('active');
            list.hide();
        });
    });

    individual.on('submit', function(e) {
        e.preventDefault();

        let alert = $('.individual-error');
        let form = $(this);
        let DATA = new FormData(form.get(0));

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: "/ajax/individual",
            type: "POST",
            dataType: "json",
            data: DATA,
            processData: false,
            contentType: false,
            beforeSend: function () {
                alert.addClass('d-none');
            },
            success: function(answ) {
                if (answ.status == 'success') {
                    location.href = '/thanks';
                }
            },
            error: function(xhr) {
                if (xhr.responseJSON.errors) {
                    let errors = xhr.responseJSON.errors;
                    let errorMsg = '';

                    for (let key in errors) {
                        errorMsg += errors[key].join("<br>") + "<br>";
                    }

                    alert.html(errorMsg);
                    alert.removeClass('d-none');
                }
            }
        });
    });

    Inputmask({'mask': '+[999999999999999]'}).mask($('input[name=phone]'));
});
