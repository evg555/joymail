docker_up:
	docker compose up -d
composer_install:
	docker run --rm -it --name php-fpm-cli -v ${PWD}:/src php-joymail composer install
migrate:
	docker run --rm -it --name php-fpm-cli -v ${PWD}:/src php-joymail php artisan migrate
generate_key:
	docker run --rm -it --name php-fpm-cli -v ${PWD}:/src php-joymail php artisan key:generate
test:
	docker run --rm -it --name php-fpm-cli -v ${PWD}:/src php-joymail ./codecept run unit
