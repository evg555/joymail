$(function () {
    let radioPayment = $('.js-payment-system');

    $('form.quiz').each(function() {
        $(this).replaceWith("<div class=\"quiz\">"+$(this).html()+"</div>")
    });

    radioPayment.on('change', function () {
        if (this.checked != true) {
            return;
        }

        let DATA = {};
        let alert = $('.quiz-error');
        let button = $('.step-buy_btn');

        DATA.orderId = $('input[name=order_id]').val();
        DATA.systemId = $(this).attr('data-system');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: "/ajax/getPaymentButton",
            type: "POST",
            dataType: "json",
            data: DATA,
            beforeSend: function () {

                alert.addClass('d-none');
            },
            success: function (answ) {
                if (answ.status == 'success') {
                    button.html(answ.view);
                } else {
                    alert.html(answ.msg);
                    alert.removeClass('d-none');
                }
            }
        });
    });
});
